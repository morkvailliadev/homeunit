<?php
include "header.php";

if(isset($contact)) {
	$name=trim(addslashes($name));
	$email=trim(addslashes($email));
	$sub=trim(addslashes($sub));
	$mobile=trim(addslashes($mobile));
	$message=trim(addslashes($message));
	$ip=$_SERVER['REMOTE_ADDR'];
	if($name!="" && $email!="" && $sub!="" && $mobile!="" && $message!="") {
		$set="name='$name'";
		$set.=",email='$email'";
		$set.=",sub='$sub'";
		$set.=",mobile='$mobile'";
		$set.=",message='$message'";
		$set.=",ip_addr='$ip'";
		$db->insertrec("insert into contact set $set");
		$subject="Contact Enquiry from $sitetitle";
		$message="<b><i>You have received a enquiry from $sitetitle </i></b><br/><br/> <b>Name:</b> $name<br/><b>Email:</b> $email<br/><b>Subject:</b> $sub<br/><b>Mobile:</b> $mobile<br/><b>Message:</b> $message";
		$com_obj->email($email, $siteemail, $subject, $message);
		$resp="<div class='alert alert-success'>You enquiry has been sent!</div>";
	}
	else {
		$resp="<div class='alert alert-danger'>Requierd fields are missing!</div>";
	}
}
?>

<div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12 row market-place-head-bg mt20">
        <span class="blackhead">Contact  </span><span class="greenhead">  Us</span>       
    </div><!--col-md-12 col-sm-12 col-xs-12 market-place-head-bg mt20-->
    <div class="col-md-8 col-sm-12 col-xs-12 row">
       <?php echo $resp=isset($resp)?$resp:''; ?>
       <div class="col-md-12 col-sm-12 col-xs-12 row mt15  brdr">
          <form class="form-horizontal" action="" method="post">
		      <h3>Quick Contact</h3>
			  <div class="form-group col-md-6 col-lg-6">
			     <!--<label class="col-sm-3">Name</label>-->
				 <div class="col-sm-12">
				    <input type="text" name="name" class="form-control" required placeholder="Full Name">
				 </div>
			  </div>
			  
			  <div class="form-group col-md-6 col-lg-6">
			     <!--<label class="col-sm-3">Email Address</label>-->
				 <div class="col-sm-12">
				    <input type="text" name="email" class="form-control" required placeholder="Email ID">
				 </div>
			  </div>
			  
			  <div class="form-group col-md-6 col-lg-6">
			     <!--<label class="col-sm-3">Subject</label>-->
				 <div class="col-sm-12">
				    <input type="text" name="sub" class="form-control" required placeholder="Subject">
				 </div>
			  </div>
			  
			  <div class="form-group col-md-6 col-lg-6">
			     <!--<label class="col-sm-3">Phone Number</label>-->
				 <div class="col-sm-12">
				    <input type="text" name="mobile" class="form-control" required placeholder="Mobile Number">
				 </div>
			  </div>
			  
			  <div class="form-group col-md-12">
			     <!--<label class="col-sm-3">Message</label>-->
				 <div class="col-sm-12">
				    <textarea class="form-control" name="message" rows="5" required placeholder="Message"></textarea>
				 </div>
			  </div>
			  
			  
			  <div class="form-group text-center col-md-12">
			     <button class="btn btn-search" type="submit" name="contact">Submit</button>
			  </div>
		  </form>
       </div><!--col-md-12-->
    </div><!--col-md-9 col-sm-12 col-xs-12 row-->
	
	<div class="col-sm-4 mt15">
	   <div class="col-sm-12 brdr">
	     <div class="row col-sm-12">
		    <h3>Reach Us</h3>
			<p><?php echo $contactus;?></p>
			
			<h3>Social Connect</h3>
			<div class=" center-block mt10 pdb25">
				<?php $getsocialicon=$db->singlerec("select * from social_links where id='1'"); @extract($getsocialicon);?>            
                <a href="<?php echo $facebook;?>" target="_blank"><i id="social-fb" class="social fb"></i><img src="<?php echo $siteurl;?>/assets/images/social icon 1.png"></a>
	            <a href="<?php echo $twitter;?>" target="_blank"><i id="social-tw" class="social tw"></i><img src="<?php echo $siteurl;?>/assets/images/social icon 2.png"></a>
	            <a href="<?php echo $gplus;?>" target="_blank"><i id="social-gp" class="social gg"></i><img src="<?php echo $siteurl;?>/assets/images/social icon 3.png"></a>
	            <a href="<?php echo $skype;?>" target="_blank"><i id="social-em" class="social skype"></i><img src="<?php echo $siteurl;?>/assets/images/social icon 4.png"></a>
         </div>
			
		 </div>
	   </div>
	</div>
	
	
	
	<div class="clearfix"></div>
	
	<div class="col-md-12 col-sm-12 col-xs-12 row mt15 ">
        <div class="embed-responsive embed-responsive-4by3" style="padding-bottom: 30%;">
		  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d995105.4852222495!2d79.51063783320352!3d13.027341380365929!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a5265ea4f7d3361%3A0x6e61a70b6863d433!2sChennai%2C+Tamil+Nadu!5e0!3m2!1sen!2sin!4v1495527075160" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>         
    </div>
    
    
</div>
</div><!--container-->

<?php include "footer.php"; ?>