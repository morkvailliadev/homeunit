<?php

?>
<footer class="">
    <div class="footer-top">
        <div class="container-wrapper">
            <div class="links-container">
                <div class="links-wrapper">
                    <h3>Homunit</h3>
                    <ul>
                        <li><a href="">For Sale</a></li>
                        <li><a href="">To Rent</a></li>
                        <li><a href="">Commerical</a></li>
                        <li><a href="">Luxury</a></li>
                        <li><a href="">New Homes</a></li>
                        <li><a href="">Wanted</a></li>
                        <li><a href="">Agents</a></li>
                        <li><a href="">Market Place</a></li>
                    </ul>
                </div>
                <div class="links-wrapper">
                    <h3>Houses for Rent</h3>
                    <ul>
                        <li><a href="">For Sale</a></li>
                        <li><a href="">To Rent</a></li>
                        <li><a href="">Commerical</a></li>
                        <li><a href="">Luxury</a></li>
                        <li><a href="">New Homes</a></li>
                        <li><a href="">Wanted</a></li>
                        <li><a href="">Agents</a></li>
                        <li><a href="">Market Place</a></li>
                    </ul>
                </div>
                <div class="links-wrapper">
                    <h3>Houses for Sale</h3>
                    <ul>
                        <li><a href="">For Sale</a></li>
                        <li><a href="">To Rent</a></li>
                        <li><a href="">Commerical</a></li>
                        <li><a href="">Luxury</a></li>
                        <li><a href="">New Homes</a></li>
                        <li><a href="">Wanted</a></li>
                        <li><a href="">Agents</a></li>
                        <li><a href="">Market Place</a></li>
                    </ul>
                </div>
                <div class="links-wrapper">
                    <h3>Luxury Properties</h3>
                    <ul>
                        <li><a href="">For Sale</a></li>
                        <li><a href="">To Rent</a></li>
                        <li><a href="">Commerical</a></li>
                        <li><a href="">Luxury</a></li>
                        <li><a href="">New Homes</a></li>
                        <li><a href="">Wanted</a></li>
                        <li><a href="">Agents</a></li>
                        <li><a href="">Market Place</a></li>
                    </ul>
                </div>
            </div>
            <div class="signup-block">
                <h3>Luxury Properties</h3>
                <div class="signup-input-block">
                    <form action="">
                        <input type="text" placeholder="Please enter your email address">
                        <button type="submit">Signup</button>
                    </form>
                </div>
                <div class="social-connect">
                    <h3>Social Connect</h3>
                    <ul>
                        <li><a href=""><span class="facebook fa fa-facebook"></span></a></li>
                        <li><a href=""><span class="twitter fa fa-twitter"></span></a></li>
                        <li><a href=""><span class="google fa fa-google-plus"></span></a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container-wrapper">
            <span class="copyright">Copyright © 2017 Homunit.lk. All rights reserved.</span>
            <div class="links">
                <a href="">Terms and Conditions</a>
                <a href="">Privacy Policy</a>
            </div>
        </div>
    </div>
</footer>
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>
</html>

<?php include "jsfunction.php"; ?>
