<?php
//include "admin/AMframe/config.php";
$PSCurncy="<span class='pdr7' style='color:#333;text-transform: capitalize;'>Rs</span>";
//if($cms_on != $cms_approve_st){echo "<script>location.href='$cms_approve';</script>";}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo $sitetitle; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $siteurl; ?>/assets/css/stylesheet.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="icon" href="<?php echo $siteurl; ?>/index.ico">
</head>
<body>
<!-- Modal Login -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header avd-serbg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="<?php echo $siteurl; ?>/assets/images/close-icon.png"></span></button>
                <h4 class="modal-title mode-tit text-center banner-font-1" style="color:#FFF;" id="myModalLabel">Login</h4>
            </div>
            <div class="modal-body">
                <form id="lform" action="<?php echo $siteurl; ?>/session" method="post">
                    <?php
                        if(isset($_REQUEST['lerror'])) {
                            echo '<center><span class="err">Incorrect Email Address/Password!</span></center>';
                        }
                        if(isset($_REQUEST['inactive'])) {
                            echo '<center><span class="err">Your account currently is awaiting for activation! Please check your mail and follow the link to activate your account.</span></center>';
                        }
                        if(isset($_REQUEST['lmt'])) {
                            echo '<center><span class="err">Fill both the fields!</span></center>';
                        }
                    ?>
                    <div class="form-group">
                        <label class="login-font">Email Address</label>
                        <input type="email" class="form-control" name="email" maxlength="30" value="<?php echo @$email; ?>" required>
                    </div>

                    <div class="form-group">
                        <label class="login-font">Password</label>
                        <input type="password" class="form-control" name="password" maxlength="20" required>
                    </div>
                    <div class="form-group">
                        <small class="pull-left">
                            <a href="#" class="login-link" data-toggle="modal" data-target="#forgetpass" data-dismiss="modal" aria-label="Close">Forgot Password?</a>
                        </small>
                        <small class="pull-right">
                            Don't Have an Account? <a href="#" class="login-link" data-toggle="modal" data-target="#register" data-dismiss="modal" aria-label="Close">Register Now</a>
                        </small>
                    </div><br>
                    <div class="form-group text-center mt30">
                        <input type="submit" class="btn bc3 btn-login" name="login" value="Log In">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Login -->

<!-- Modal Register -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header avd-serbg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="<?php echo $siteurl; ?>/assets/images/close-icon.png"></span></button>
                <h4 class="modal-title mode-tit text-center banner-font-1" style="color:#FFF;" id="myModalLabel">Register</h4>
            </div>
            <div class="modal-body">
                <form id="rform" action="<?php echo $siteurl; ?>/signup" method="post">
                    <div class="form-group">
                        <label class="login-font">Full Name</label>
                        <input type="text" class="form-control" name="fullname" maxlength="20" value="<?php echo @$fullname; ?>">
                    </div>

                    <div class="form-group">
                        <label class="login-font">Email Address
                            <?php
                            if(isset($_REQUEST['rmerr'])) {
                                echo '&bull; <span class="err">Email address already used!</span>';
                            }
                            ?>
                        </label>
                        <input type="email" class="form-control" name="email" maxlength="30" value="<?php echo @$email; ?>">
                    </div>

                    <div class="form-group">
                        <label class="login-font">Mobile Number</label>
                        <input type="text" class="form-control" name="mobile" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="<?php @$mobile; ?>">
                    </div>

                    <div class="form-group">
                        <label class="login-font">Password</label>
                        <input type="password" class="form-control" name="password" maxlength="20" value="<?php @$pass; ?>">
                    </div>


                    <div class="form-group">
                        <label class="login-font">Confirm Password
                            <?php
                            if(isset($_REQUEST['rperr'])) {
                                echo '&bull; <span class="err">Password does not match!</span>';
                            } ?></label>
                        <input type="password" class="form-control" name="cpassword" maxlength="20" value="<?php @$cpass; ?>">
                    </div>

                    <div class="form-group">
                        <small class="pull-right">Already have an account? <a href="#" class="login-link" data-toggle="modal" data-target="#login" data-dismiss="modal" aria-label="Close">Login Here</a></small><br>
                    </div>
                    <div class="form-group text-center mt30">
                        <input type="submit" class="btn bc3 btn-login" name="signup" value="Register">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Register -->

<header class="">
    <div class="TopNavBar">
        <div class="container-wrapper">
            <select class=" select-country" id="sel1">
                <option>Select your city</option>
                <option>Select your city</option>
            </select>
            <div class="auth-section">
                <a href="" data-toggle="modal" data-target="#login">Login</a>
                <a href="" data-toggle="modal" data-target="#register">Register</a>
                <a href="/contact-us">Contact</a>
            </div>
        </div>
    </div>
    <div class="MainNavigation">
        <div class="container-wrapper">
            <div class="menu-items">
                <a href="#">For Sale</a>
                <a href="#">To Rent</a>
                <a href="#">Commercial</a>
                <a href="#">Luxury</a>
                <a href="#">New Homes</a>
                <a href="#">Wanted</a>
                <a href="#">Agents</a>
                <a href="#">Market Place</a>
            </div>
        </div>
    </div>

</header>