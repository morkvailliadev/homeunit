<?php
//error_reporting(-1);
ini_set('display_errors', 1);
include "header.php";
include "mapapi.php";
if ($cms_on != $cms_approve_st) {
    echo "<script>location.href='$cms_approve';</script>";
}
?>
    <div class="container property-header">
        <div class="inner-container">
            <div class="main-logo"></div>
            <div class="list-property">List Property</div>
        </div>
        <div class="second-menu">
            <a href="#">For Sale</a>
            <a href="#">To Rent</a>
            <a href="#">Commercial</a>
            <a href="#">Luxury</a>
            <a href="#">New Homes</a>
        </div>
        <div class="header-filter">
            <h3>Lets experts help you find your perfect property</h3>
            <form action="">
                <div class="inputs-block">
                    <div class="input-wrapper">
                        <input type="text" placeholder="City / Location">
                        <span class="fa fa-search search-icon"></span>
                    </div>
                    <div class="input-wrapper">
                        <select name="" id="">
                            <option value="" hidden>Bedrooms</option>
                            <option value="">2</option>
                        </select>
                    </div>
                    <div class="input-wrapper">
                        <select name="" id="">
                            <option value="" hidden>Budget</option>
                            <option value="">2</option>
                        </select>
                    </div>
                    <div class="input-wrapper search">
                        <button type="submit">Search</button>
                    </div>
                </div>
            </form>
            <form action="">
                <div class="inputs-block bottom-inputs">
                    <div class="input-wrapper">
                        <span class="quick-search">Quick Search <span></span><span></span></span>
                    </div>
                    <div class="input-wrapper">
                        <select name="" id="">
                            <option value="" hidden>Property Catogory</option>
                            <option value="">2</option>
                        </select>
                    </div>
                    <div class="input-wrapper">
                        <select name="" id="">
                            <option value="" hidden>Property for</option>
                            <option value="">2</option>
                        </select>
                    </div>
                    <div class="input-wrapper">
                        <button type="submit"><span></span></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="logos-block">
        <div class="container-wrapper">
            <div class="logos-wrapper">
                <div class="image-wrapper">
                    <img src="img/logo.jpg" alt="">
                </div>
                <div class="image-wrapper">
                    <img src="img/logo1.png" alt="">
                </div>
                <div class="image-wrapper">
                    <img src="img/logo3.png" alt="">
                </div>
                <div class="image-wrapper">
                    <img src="img/logo.jpg" alt="">
                </div>
                <div class="image-wrapper">
                    <img src="img/logo1.png" alt="">
                </div>
            </div>
            <div class="logos-select">
                <div class="input-wrapper">
                    <select name="" id="">
                        <option value="" hidden>Browse properties in Sri Lanka by City</option>
                        <option value="">2</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="content-block">
        <div class="container-wrapper">
            <h2>Newly listed properties for Sale in Sri Lanka</h2>
            <div class="content-wrapper">
                <div class="content-element">

                </div>
                <div class="content-element">

                </div>
                <div class="content-element">

                </div>
                <div class="content-element">

                </div>
                <div class="content-element">

                </div>
                <div class="content-element">

                </div>
            </div>
        </div>
        <div class="container-wrapper">
            <h2>Newly listed properties for Rent in Sri Lanka</h2>
            <div class="content-wrapper">
                <div class="content-element">

                </div>
                <div class="content-element">

                </div>
                <div class="content-element">

                </div>
                <div class="content-element">

                </div>
                <div class="content-element">

                </div>
                <div class="content-element">

                </div>
            </div>
        </div>
    </div>
    <div class="select-block">
        <div class="container-wrapper">
            <div class="logos-select">
                <div class="input-wrapper">
                    <select name="" id="">
                        <option value="" hidden>Browse properties in Sri Lanka by City</option>
                        <option value="">2</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
<?php include "footer.php";
