<script>
$(document).ready(function () {

    $('#p_ad').validate({
        rules: {
            mobile: {
                required: true
            },
			title: {
                required: true
            },
			loc: {
                required: true
            },
			address: {
                required: true
            },
			eprice: {
                required: true
            },
			bprice: {
                required: true
            },
			descrip: {
                required: true
            },
			carea: {
                required: true
            },
			parea: {
                required: true
            },
			cperson: {
                required: true
            },
			cno: {
                required: true
            },
			tcall: {
                required: true
            }
        }
    });

});</script>

<script>
$(document).ready(function () {

    $('#maform').validate({
        rules: {
            fullname: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
			mobile: {
                required: true
            },
			website: {
				url: true
            }
        }
    });

});</script>

<script>
$(document).ready(function () {

    $('#cnt_form').validate({
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
			mobile: {
                required: true
            },
			sub: {
				required: true
            },
			message: {
				required: true
            }
        }
    });

});</script>

<script>
$(document).ready(function () {

    $('#rform').validate({
        rules: {
            fullname: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
			mobile: {
                required: true
            },
			password: {
                required: true,
				minlength: 6
            },
			cpassword: {
				required: true,
				minlength: 6
            }
        }
    });

});</script>

<script>
$(document).ready(function () {

    $('#lform').validate({
        rules: {
            email: {
                required: true,
                email: true
            },
			password: {
                required: true
            }
        }
    });

});</script>

<script>
$(document).ready(function () {

    $('#cpform').validate({
        rules: {
            cpass: {
                required: true
            },
            npass: {
                required: true
            },
			rpass: {
                required: true
            }
        }
    });

});</script>

<script>
$(document).ready(function () {

    $('#updsoc').validate({
        rules: {
            fb: {
                required: true,
				url: true
            },
            twt: {
                required: true,
				url: true
            }
        }
    });

});</script>

<?php if(isset($_REQUEST['rperr']) || isset($_REQUEST['rmerr'])) {?> <script type="text/javascript">$(window).load(function(){
  $('#register').modal('show');
  $('#forgetpass').css({ display: "none" });
  $('#login').removeAttr( 'style' ); }); </script> <?php } ?>
	
<?php if(isset($_REQUEST['lerror']) || isset($_REQUEST['nl']) || isset($_REQUEST['lmt'])) {?> <script type="text/javascript">$(window).load(function(){
  $('#login').modal('show');
  $('#forgetpass').css({ display: "none" });
  $('#register').removeAttr( 'style' ); }); </script><?php } ?>

