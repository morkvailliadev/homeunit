<div class="row">
<div class=" col-md-3 col-sm-12 col-xs-12 mt20">
 <div class="col-md-12 col-sm-12 col-xs-12 profile-brdr">
		  <div class="profile-sidebar row">
			  <div class="profile-userpic">
				  <img src="<?php echo $siteurl; ?>/images/user/<?php echo $row["prof_image"]; ?>" class="img-responsive" alt=""><br><center><a style="text-decoration: none;" href="<?php echo $siteurl; ?>/change-picture">Change Picture</a></center>
			  </div><!--profile-userpic-->
			  <div class="profile-usertitle">
				  <div class="profile-usertitle-name">
					 <?php echo strtoupper($row["fullname"]); ?>
				  </div><!--profile-usertitle-name--><?php echo 'Member Since: '.date("d-M-Y", $row['created_at']); ?>
			  </div><!--profile-usertitle-->
			  <div class="profile-usermenu">
				  <ul class="nav">
					  <li <?php if(basename($_SERVER['SCRIPT_NAME'])=='dashboard.php') echo 'class="active"'; ?>><a href="<?php echo $siteurl; ?>/dashboard">Dashboard </a></li>
					  <li <?php if(basename($_SERVER['SCRIPT_NAME'])=='manage-your-list.php') echo 'class="active"'; ?>><a href="<?php echo $siteurl; ?>/manage-your-list">Manage Your List</a></li>
					  <li <?php if(basename($_SERVER['SCRIPT_NAME'])=='plan-detail.php') echo 'class="active"'; ?>><a href="<?php echo $siteurl; ?>/plan-detail">Membership Plan</a></li>
					  <li <?php if(basename($_SERVER['SCRIPT_NAME'])=='post-ad.php') echo 'class="active"'; ?>><a href="<?php echo $siteurl; ?>/post-ad">Add Property</a></li>
					  <li <?php if(basename($_SERVER['SCRIPT_NAME'])=='my-account.php') echo 'class="active"'; ?>><a href="<?php echo $siteurl; ?>/my-account">My Account</a></li>
					  <li><a href="<?php echo $siteurl; ?>/logout">Logout</a></li>
				  </ul>
			  </div><!--profile-usermenu-->
		  </div><!--profile-sidebar-->
	  </div><!--col-md-12 col-sm-12 col-xs-12 profile-brdr-->
</div><!--col-md-3 col-sm-12 col-xs-12-->