<?
include "header.php";
include "pagination.php";
$perpage=12;
$limit=limitation($perpage);
include "srch_algorithm.php";
?>

<div class="container mt30">
    
 
    <div class="col-md-9 col-sm-12 col-xs-12 mt20">
     <p><span class="blackhead"> TITANIUM </span><span class="greenhead"> AGENCIES</span></p>
   </div>  
   
  <ul id="flexiselDemo3">
   <?php $agentrec=$db->get_all("select prof_image from register where role='Agent' limit 0,10");
	foreach($agentrec as $agntrec){
		$profimage=$agntrec['prof_image'];
		echo "<li><img src='$siteurl/images/user/$profimage' /></li>";
	}
	?>
  </ul>    


    
	
	<div class="col-md-9 col-sm-12 col-xs-12 row">
    
    <div class="col-md-12 pdb15 " style="border: 1px solid #ccc;">
      
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
            <?php $sldisp=""; $getrec=$db->get_all("select name,comment,image from slider where active_status='1' and sidertype='0'");
			for($sl=0;$sl<count($getrec);$sl++){
				$name=$getrec[$sl]['name'];
				$comment=$getrec[$sl]['comment'];
				$imgsl=$getrec[$sl]['image'];
				if($sl==0){$slact="active";}else{$slact="";}
				$sldisp .= "<div class='item $slact'>
					<img src='admin/uploads/slider/$imgsl' >
					<div class='carousel-caption'>
						<span class='l head1 font_r_en'>$name</span><br />
						<span class='head2'>$comment</span>
					</div>
				</div>";
			}
			echo $sldisp; 
			?>
			<!--<div class="item active">
                <img src="images/prop/prop1_113b.jpg" >
                <div class="carousel-caption">
                  <span class="l head1 font_r_en">Executive Lodges</span><br />
                  <span class="head2">Executive Lodges, Lahore</span>
                </div>
              </div>
			  
              <div class="item">
                <img src="images/prop/prop1_114d.jpg" >
                <div class="carousel-caption">
                   <span class="l head1 font_r_en">Executive Lodges</span><br />
                   <span class="head2">Executive Lodges, Lahore</span>
                </div>
              </div>-->
			  
            </div>
          
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
    
    </div>
    
    
    <div class="col-md-12 col-xs-12 col-sm-12 brdr-new-1 mt20">
	   <div class="row brdr-new-1-bg">
			<p class="pdl20"><span class="blackhead"> FEATURED </span><span class="greenhead"> DEVELOPERS</span></p>
		</div>     <!--col-md-12-->                 
		<?php $futurerec=$db->get_all("select b.pid,b.image,a.con_person,a.con_num from listings as a inner join listing_images as b on a.id=b.pid where a.featured='1' group by b.pid");
		foreach($futurerec as $future){
			$con_person=$future['con_person'];
			$con_num=$future['con_num'];
			$ftrimg=$future['image'];
			$pid=$future['pid'];
		?>
		<div class="col-md-2 col-sm-3  col-xs-6 mt20">
		   <a href="#" rel="popover" class="pop" data-placement="top"  data-trigger="focus" data-popover-content="#list-popover<?php echo $pid;?>" data-original-title="" title="">
			  <img class="avatar2 img-responsive" src="<?php echo $siteurl;?>/images/prop/230_144/<?php echo $ftrimg;?>" alt="agent">
			</a>
			<div id="list-popover<?php echo $pid;?>" class="hide">
				<strong> Town Developer</strong><br />
				<small><i class="fa fa-map-marker"></i> <?php echo $con_person;?></small> <br />
				<small><i class="fa fa-mobile"></i> <?php echo $con_num;?></small> <br />
			</div><!--list-popover-->
		</div><!--col-2-->
		<?}?>
    </div>
    
			
   <div class="col-md-12 col-sm-12 col-xs-12 brdr-new-1 mt20">
              <!---------Slider List--------->

          <div class="row brdr-new-1-bg">
          <div class="corner-ribbon top-left sticky red shadow">New</div>
                      <div class="col-md-6 col-sm-6 col-xs-6">
                          <p><span class="blackhead"> New </span><span class="greenhead"> Projects</span></p>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-6">
                          <!-- Controls -->
                          <div class="controls pull-right">
                              <a class="left fa fa-chevron-left btn control-btn carshop-btn arrow-brdr" href="#carousel-example"
                                  data-slide="prev"></a><a class="right fa fa-chevron-right btn control-btn carshop-btn arrow-brdr" href="#carousel-example"
                                      data-slide="next"></a>
                          </div>
                          
                      </div>
                </div><!--row-->
                  
                  <div id="carousel-example" class="carousel slide " data-ride="carousel">
                      <!-- Wrapper for slides -->
                      <div class="carousel-inner">
						<?
						$count = 1;
						$res=mysql_query("select * from listings where post_sts=1 and featured=0 order by mplan desc limit 0, 8");
						while($prop=mysql_fetch_assoc($res)) {
							$im=$db->singlerec("select * from listing_images where pid='".$prop['id']."' limit 1");
						if($count==1){
							$active="active";
						}
						else {
							$active="";
						}
						if ($count%4 == 1) {  
							echo "<div class='item ".$active."'>";
							echo "<div class='row'>";
						}
						?>             
			  <div class="col-md-3 col-sm-6 col-xs-12 ">
				  <div class="carshop-item brdr-2">
					  <div class="photo">
						  <a href="listing/<?php echo $prop['randuniq']; ?>/<?php echo $prop['slug']; ?>"><img src="images/prop/<?php echo $im['image']; ?>"  alt="*" /></a>
					  </div><!--photo-->
						 <div class="info row">                                    
							  <div class="col-md-12">
									 <p class="new-properties-head pdl10"><?php echo $prop['prop_title']; ?></p>
									 <p class="new-properties-place pdl10 "><?php echo $prop['location']; ?></p>
									 <p class=" greenhead pdl10"><?php echo $PSCurncy . $prop['exp_price']; ?></p>
									 <div class=" col-md-12">
										 <div class="row">
											 <div class=" col-xs-4 brdr-2 new-properties-details">
												  <img src="assets/images/square.png" /><?php echo $prop['covered_area']; ?>
											 </div><!--col-md-4-->
											 <div class=" col-xs-4 brdr-2 new-properties-details">
												  <img src="assets/images/bed.png" /><span class="pdl10"><?php echo $prop['bedroom']; ?></span>
											 </div><!--col-md-4-->
											 <div class=" col-xs-4 brdr-2 new-properties-details">
												<img src="assets/images/bath.png" /><span class="pdl10"><?php echo $prop['bathroom']; ?></span>
											 </div><!--col-md-4-->
										 </div><!--row-->
									 </div><!--brdr-2-->
							   </div><!--price col-md-12-->                                      
							  </div><!--info row-->
						  </div><!--carshop-item-->
					 </div><!--col-md-3-->
						<?php                      
						if ($count%4 == 0) {
							echo '</div></div>';
						}
						$count++;
						}
						if ($count%4 != 1) echo '</div></div>';
						?>  
			</div>
	  </div> 


<!-------End Slider List--------->
          </div><!--col-md-12 col-sm-12 col-xs-12 brdr-->
        
        
        
        
        
				
</div><!--col-md-9 col-sm-12 col-xs-12 row-->    
<?php include "listing_right.php"; ?>
</div><!--container-->

<?php include "footer.php"; ?>