<?php include "header.php"; ?>

<div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20">
        <span class="blackhead pdl10">Contact Us</span>
    </div><!--col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20-->
    
    <div class="row">
         
         <div class="col-md-12 col-sm-12 col-xs-12 mt20">
             <div class="col-md-12 col-sm-12 col-xs-12 profile-brdr-2">
                 <div class="pdt10">
                 
                 
                 </div><!--class="pdt10"-->
                    <!-- <img src="assets/images/about-us-banner.png" class="img-responsive"> -->
					
					
					<div class="text-center blackhead" style="font-size:20px; font-weight:bold;">Contact Us</div>
					
					<div class="col-md-6">
                                    <form action="contact" method="post">
                                      <div class="form-group">
                                        <label class="heading-black-small-2">Name</label>
                                        <input type="text" name="name" class="form-control" required>
                                      </div>
                                      
                                      <div class="form-group">
                                        <label class="heading-black-small-2">Email ID</label>
                                        <input type="email" name="email" class="form-control" required>
                                      </div>
                                      
                                      <div class="form-group">
                                        <label class="heading-black-small-2">Mobile Number</label>
                                        <input type="number" name="mobile" class="form-control" required>
                                      </div>
                                      
                                      <div class="form-group">
                                        <label class="heading-black-small-2">Message</label>
                                        <textarea class="form-control" name="message" rows="5" name="textarea" required></textarea>
                                      </div>
                                      
                                      <div class="form-group pdt15">
                                          <input name="cont_us" class="btn btn-search" type="submit" value="Submit">
                                      </div>
                                     
                             
                                    </form>
                               </div>
					<div class="col-md-6">
                                  
                                    <ul class="contact-list">
                                         <li class=" heading-black  pdt20">Contact Us</li>
                                         <li ><i class="fa fa-phone page-links" aria-hidden="true"></i> &nbsp; (India) +91-979 003 3633</li>
                                         <li ><i class="fa fa-phone page-links" aria-hidden="true"></i> &nbsp; (USA) +1-325 200 4515</li>
                                         <li ><i class="fa fa-phone page-links" aria-hidden="true"></i> &nbsp; (UK) +44-203 290 5530</li>
                                         <li ><i class="fa fa-whatsapp page-links" aria-hidden="true"></i>  &nbsp; +91-979 003 3633 </li>
                                         <li ><i class="fa fa-envelope-o page-links" aria-hidden="true"></i> &nbsp; vsjayan@gmail.com </li>
                                         <li ><i class="fa fa-skype page-links" aria-hidden="true"></i>  &nbsp; hemabalan2004 </li>                   
                                     </ul>
                                    
                                    
                               </div>
                 
             </div><!--col-md-12 col-sm-12 col-xs-12 profile-brdr-->
         </div><!--col-md-9 col-sm-12 col-xs-12-->
    </div><!--row-->
</div> <!--container-->

<?php include "footer.php"; ?>