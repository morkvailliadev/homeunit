<?
if(!$_SESSION["usr"]) {
	echo "<script>location.href='$siteurl?nl';</script>";
	header("Location: $siteurl?nl"); exit;
}
$ref = "email='".$_SESSION['usr']."'";
$row=$db->singlerec("select * from register where $ref");
if($_SESSION['usr']!=$row['email']) {
	session_destroy();
	echo "<script>location.href='$siteurl';</script>";
	header("Location: $siteurl"); exit;
}

if(isset($prof_upd)) {
    $name=trim(addslashes($fullname));
	$name=strtolower($name);
    $mobile=trim(addslashes($mobile));
    $site=trim(addslashes($website));
    $dtl=trim(addslashes($details));
	$set = "fullname='$name',";
	$set .= "mobile='$mobile',";
	$set .= "website='$site',";
	$set .= "details='$dtl'";
	$db->insertrec("update register set $set where email='".$_SESSION['usr']."'");
	echo "<script>swal('Great!', 'Your profile has been updated!', 'success')</script>";
}

if(isset($chpass)) {
	$cpass=trim(addslashes($cpass));
	$npass=trim(addslashes($npass));
	$rpass=trim(addslashes($rpass));
	$epass=md5($npass);
    
	$checkcpass=$db->check2column("register", "password", $cpass, "email", $_SESSION['usr']);
	if($checkcpass==0) {
		echo "<script>swal('Oops..', 'Current password is incorrect!', 'error')</script>";
	}
	else {
			if($npass!=$rpass) {
					echo "<script>swal('Oops..', 'Your password does not match!', 'error')</script>";
			}
			else {
				   $set = "password='$npass',";
				   $set .= "encr_password='$epass'";
				   $db->insertrec("update register set $set where email='".$_SESSION['usr']."'");
				   echo "<script>swal('Great!', 'Your password has been updated!', 'success')</script>";
			}
	}
}

if(isset($updsoc)) {
    $fburl=trim(addslashes($fb));
	$twturl=trim(addslashes($twt));
	$skpid=trim(addslashes($skp));	 
	$set = "facebook='$fburl',";
	$set .= "twitter='$twturl',";
	$set .= "skype='$skpid'";
	$db->insertrec("update register set $set where email='".$_SESSION['usr']."'");
	echo "<script>swal('Great!', 'Your social profile has been updated!', 'success')</script>";
}

if(isset($nletter)) {
     if($rcv==1&&$agreed==1) {
	        $name=stripslashes($row['fullname']);
			$mail=stripslashes($row['email']);
			$ip=$_SERVER['REMOTE_ADDR'];
			$time=time();
			$check=$db->check1column("newsletter", "email", $mail);
			if($check==0) { 
				$set="name='$name',";
				$set.="email='$mail',";
				$set.="ip='$ip',";
				$set.="date='$time'";
				$db->insertrec("insert into newsletter set $set");
				echo "<script>swal('Great!', 'You subscribed to the newsletter!', 'success')</script>";
			}
			else {
				echo "<script>swal('Oops..', 'You already subscribed to the newsletter!', 'error')</script>";
			}
	    }
		else if($rcv==0&&$agreed==0 || $rcv==1&&$agreed==0 || $rcv==0&&$agreed==1) {
				echo "<script>swal('Oops..', 'You`re not agreed to get newsletters!', 'error');</script>";
	    }
}

if(isset($prf_img)) {
	$file=$_FILES['file']['tmp_name'];
	$temp = explode(".", $_FILES["file"]["name"]);
	$ext = end($temp);
	list($width,$height,$type,$attr) = getimagesize($file);
	$mime = image_type_to_mime_type($type);
	if(($mime != "image/jpeg") && ($mime != "image/jpeg") && ($mime != "image/png")) {
			echo "<script>swal('Oops..', 'Unrecognised image type. Upload .jpg or .png types!', 'error')</script>";
	}
	else if($width<150 || $height<150) {
		echo "<script>swal('Oops..', 'Image must be atleast 150*150 pixels!', 'error');</script>";
	}
	else {
			$newname = 'user_xe'.$row['id'];
			$org='images/user/org/'.$newname.'.'.$ext;
			$prf='images/user/'.$newname.'.'.$ext;
			move_uploaded_file($file, $org);
			$resizeObj = new resize($org);		
			$resizeObj -> resizeImage(150, 150, 'exact');
			$resizeObj -> saveImage($prf, 100);	
			@unlink($org);
			$imgurl = $newname.'.'.$ext;
			$set = "prof_image='$imgurl'";
			$db->insertrec("update register set $set where email='".$_SESSION["usr"]."'");
			echo "<script>location.href='my-account';</script>";
			header("Location: my-account"); exit;
	}
}
?>