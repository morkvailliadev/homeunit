<div class="container-fluid footerbox mt20">
 <div class="container mt20">
  <div class="row col-md-12">

       
     <div class="col-md-3 col-sm-6 col-xs-12 pdb20">
          <p class="footerboxheader">Property Category</p>
		<?php
		$PS_Categ=$db->get_all("select filtername,cat_name from category where cat_status='1'");
		foreach($PS_Categ as $PSCateg):
		$catt=$PSCateg['cat_name'];
		$fcat=$PSCateg['filtername'];
		if(isset($cat)==$fcat) $ch="selected";
		else $ch="";
		echo "<p><a href='property-list.php?cat=$fcat' class='footerboxlink'>$catt</a></p>";
		endforeach;
		?>      
    </div><!--col-md-3-->
  

    <div class="col-md-3 col-sm-6 col-xs-12 pdb20">
        <p class="footerboxheader">Listing by Location</p>
		<?php $avcity=$db->get_all("select DISTINCT location from listings where post_sts='1' order by location asc");
		  for($ct=0;$ct<count($avcity);$ct++){
			$ctnam=$avcity[$ct]['location'];
			if($ct==0)
				echo "<p class='mt20'><a href='property-list?srchloc=$ctnam' class='footerboxlink'>$ctnam</a></p>";
			else 
				echo "<p><a href='property-list?srchloc=$ctnam' class='footerboxlink'>$ctnam</a></p>";  
		  }
		  ?>         
    </div><!--col-md-3-->
    
    <div class="col-md-3 col-sm-6 col-xs-12 pdb20">
          <p class="footerboxheader">Featured Properties</p>
		  <?php
		  $prop=$db->get_all("select prop_title,slug,randuniq from listings where post_sts=1 and featured=1 order by mplan desc limit 0, 5");
		  for($fr=0;$fr<count($prop);$fr++){
			$slug=$prop[$fr]['slug'];
			$randuniq=$prop[$fr]['randuniq'];
			$frtrprop=ucwords($prop[$fr]['prop_title']);
			if($fr==0)
				echo "<p class='mt20'><a href='$siteurl/listing/$randuniq/$slug' class='footerboxlink'>$frtrprop</a></p>";
			else 
				echo "<p><a href='$siteurl/listing/$randuniq/$slug' class='footerboxlink'>$frtrprop</a></p>";
		  }
		  ?>
    </div><!--col-md-3-->
  <style>/*.mt20 p{color:#FFF;}*/</style>  
    
    <div class="col-md-3 col-sm-6 col-xs-12 pdb20">
          <p class="footerboxheader">Contact Us</p>
          <p class="mt20" style="color:#FFF;"><?php echo $contactus;?></p>
          <p class="footerboxheader">Social Connect</p>
          <div class=" center-block pdb25">           
			<?php $getsocialicon=$db->singlerec("select * from social_links where id='1'"); @extract($getsocialicon);?>
                <a href="<?php echo $facebook;?>" target="_blank"><i id="social-fb" class="social fb"></i><img src="<?php echo $siteurl;?>/assets/images/social icon 1.png"></a>
	            <a href="<?php echo $twitter;?>" target="_blank"><i id="social-tw" class="social tw"></i><img src="<?php echo $siteurl;?>/assets/images/social icon 2.png"></a>
	            <a href="<?php echo $gplus;?>" target="_blank"><i id="social-gp" class="social gg"></i><img src="<?php echo $siteurl;?>/assets/images/social icon 3.png"></a>
	            <a href="<?php echo $skype;?>" target="_blank"><i id="social-em" class="social skype"></i><img src="<?php echo $siteurl;?>/assets/images/social icon 4.png"></a>
          </div>
          
    </div><!--col-md-3-->
    
    
  </div><!--row col-md-12-->
 </div><!--container-->
</div><!--container-fluid footerbox-->
<!--End Footer Box-->


<!--Footer-->
<div class="container-fluid footer">
<div class="container">
  <div class="row col-md-12">
    
      <div class="col-md-6 col-xs-12 col-sm-12 text-left">
     <?php echo $copyrights;?>
      </div><!--col-md-6-->
      <div class="col-md-4 col-xs-12 col-sm-12 col-md-offset-2 text-right">
       <div class="col-md-6 col-md-offset-2 col-sm-12 col-xs-12">
         <a href="#" class="footerlink">Terms and Conditions</a>
       </div><!--col-md-3-->
       <div class="col-md-4 col-sm-12 col-xs-12">
          <a href="#" class="footerlink">Privacy Policy</a>
       </div><!--col-md-3-->
      </div><!--col-md-6-->
    
  </div><!--row-->
</div><!--container-->
</div><!--container-fluid-->
<!--End Footer-->

 <script>
	 $('[rel="popover"]').popover({
        container: 'body',
        html: true,
        content: function () {
            var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
            return clone;
        }
    }).click(function(e) {
        e.preventDefault();
    });
	</script>
	
	
	<script type="application/javascript">
		$(".pop").popover({ trigger: "manual" , html: true, animation:false})
    .on("mouseenter", function () {
        var _this = this;
        $(this).popover("show");
        $(".popover").on("mouseleave", function () {
            $(_this).popover('hide');
        });
    }).on("mouseleave", function () {
        var _this = this;
        setTimeout(function () {
            if (!$(".popover:hover").length) {
                $(_this).popover("hide");
            }
        }, 300);
});

    </script>

</body></html>

<?php include "jsfunction.php"; ?>