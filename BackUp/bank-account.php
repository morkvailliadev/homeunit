<?php
include 'header.php';
include "pagination.php";
$perpage=5;
$limit=limitation($perpage);
include 'profile_header.php';
?>

<div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20">
        <span class="blackhead pdl10">Dashboard</span>
    </div><!--col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20-->
         
		 <?php include "profile_left.php"; ?>
         <div class="col-md-9 col-sm-12 col-xs-12 mt20">
             <div class="col-md-12 col-sm-12 col-xs-12 profile-brdr-2">
                 <div class="pdt10">
				 
                 <div class="row col-md-10 col-sm-6 col-xs-12 property-dash-head">Bank Account Details<br><hr></div>
                 <!--<div class="row col-md-2 col-sm-6 col-xs-12 mb10 pull-right"><a href="post-ad"><input type="button" class="btn btn-view-detail" value="Add Property" /></a><br><br></div>-->
                 </div><!--class="pdt10"-->
                 
                 <div class="row">
				    <div class="col-sm-6 bank-account">
					    <div class="panel">
							<?php $bank=$db->singlerec("select * from bank_detail where id='1'"); ?>
						      <div class="panel-head">
								    <?php echo $bank['bank_name']; ?>
								</div>
								<div class="panel-body" >
									<table class="table b_account">
									   <tr>
									      <td>Account Number</td>
										  <td><?php echo $bank['acc_no']; ?></td>
									   </tr>
									   
									   <tr>
									      <td>Bank Name</td>
										  <td><?php echo $bank['bank_name']; ?></td>
									   </tr>
									   
									   <tr>
									      <td>IFSC Code</td>
										  <td><?php echo $bank['ifsc_code']; ?></td>
									   </tr>
									   
									   <tr>
									      <td>Account Name</td>
										  <td><?php echo $bank['acc_name']; ?></td>
									   </tr>
									   
									   <tr>
									      <td>Branch Name</td>
										  <td><?php echo $bank['branch_name']; ?></td>
									   </tr>
									</table>
								</div>
						</div>
					</div>
			     </div>
				
             </div><!--col-md-12 col-sm-12 col-xs-12 profile-brdr-->
         </div><!--col-md-9 col-sm-12 col-xs-12-->
    </div><!--row-->
	</div>
</div> <!--container-->

<?
include "footer.php";

if(isset($_REQUEST['verified'])) {
	echo "<script>swal('Congrats!', 'Your account has been activated! Now you can post ad, view list of properties etc.!', 'success')</script>";
}
?>