<?php
include "admin/AMframe/config.php";
$PSCurncy="<span class='pdr7' style='color:#333;text-transform: capitalize;'>Rs</span>";
if($cms_on != $cms_approve_st){echo "<script>location.href='$cms_approve';</script>";}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="Property Listing">
    <meta name="description" content="Property Listing">
    <meta name="keywords" content="property,listing">
	<title><?php echo $sitetitle; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo $siteurl; ?>/assets/css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $siteurl; ?>/assets/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $siteurl; ?>/assets/css/stylesheet.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $siteurl; ?>/assets/css/slider.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $siteurl; ?>/assets/css/bootstrap-select.min.css" />
	<link rel="stylesheet" href="<?php echo $siteurl; ?>/assets/css/sweetalert.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,800italic,800,700italic,700,600italic,600,300italic,400italic,300">
	<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    <link rel="icon" href="<?php echo $siteurl; ?>/index.ico">
	<script type="text/javascript" src="<?php echo $siteurl; ?>/assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $siteurl; ?>/assets/js/bootstrap.js"></script>
	<script src="https://js.stripe.com/v2/" type="text/javascript"></script>
    <script src="<?php echo $siteurl; ?>/assets/js/sweetalert.min.js"></script>
	<script src="<?php echo $siteurl; ?>/assets/js/jquery.validate.min.js"></script>
	<script src="<?php echo $siteurl; ?>/assets/js/image-prv.js"></script>
	
     <!--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" />	-->
     <script src="http://www.jqueryscript.net/demo/Clean-Responsive-jQuery-Carousel-Plugin-flexisel/js/jquery.flexisel.js"></script>
     
   
     
    

<script>
$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            $(this).toggleClass('open');       
        }
    );
});
</script>
<script>
$(window).load(function() {
    $("#flexiselDemo3").flexisel({
        visibleItems: 3,
        animationSpeed: 1000,
        autoPlay: true,
        autoPlaySpeed: 3000,            
        pauseOnHover: true,
        enableResponsiveBreakpoints: true,
        responsiveBreakpoints: { 
            portrait: { 
                changePoint:480,
                visibleItems: 1
            }, 
            landscape: { 
                changePoint:640,
                visibleItems: 2
            },
            tablet: { 
                changePoint:768,
                visibleItems: 3
            }
        }
    });

   
    
});
</script>
<link rel="stylesheet" href="<?php echo $siteurl; ?>/slick.css" />
<link rel="stylesheet" href="<?php echo $siteurl; ?>/slick-theme.css" />
<script src="slick.js"> </script>
<script type="text/javascript">
    $(document).on('ready', function() {
      $(".regular").slick({
        dots: true,
        infinite: true,
        slidesToShow:3,
        slidesToScroll: 3
      });
     
});
  </script>
<style>
.pdr7{ padding-right:7px} 
   
.slider {
        width: 50%;
        margin: 100px auto;
    }

    .slick-slide {
      margin: 0px 20px;
    }

    .slick-slide img {
      width: 100%;
    }

    .slick-prev:before,
    .slick-next:before {
        color: black;
    }

.overlay {   color:#fff;  position:absolute;  z-index:12;  left:0;  width:100%;  text-align:center;}
.carousel-indicators .active {    width: 22px;    height: 22px;    margin: 0; margin-left:15px; background-color: #5BB531;    border-color: #5BB531;}
.carousel-indicators li {
    display: inline-block;
    width: 20px;
    height: 20px;
    margin: 1px;
	margin-left:15px;
    text-indent: -999px;
    cursor: pointer;
    background-color: #FFF;
    border: 1px solid #fff;
    border-radius: 10px; 
}

a.add-prop
{background: #fda744;
color: #FFF !important;
padding: 3px 10px;
border-radius: 3px;}

a.add-prop:hover
{background: #fda744;
}
</style>
</head>

<body>
<!-- Modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header avd-serbg">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="<?php echo $siteurl; ?>/assets/images/close-icon.png"></span></button>
                    <h4 class="modal-title mode-tit text-center banner-font-1" style="color:#FFF;" id="myModalLabel">Login</h4>
                  </div>
      <div class="modal-body">
        <form id="lform" action="<?php echo $siteurl; ?>/session" method="post">
		<?php
		if(isset($_REQUEST['lerror'])) { 
				echo '<center><span class="err">Incorrect Email Address/Password!</span></center>';
		}
		if(isset($_REQUEST['inactive'])) { 
				echo '<center><span class="err">Your account currently is awaiting for activation! Please check your mail and follow the link to activate your account.</span></center>';
		}
		if(isset($_REQUEST['lmt'])) { 
				echo '<center><span class="err">Fill both the fields!</span></center>';
		}			
		?>
          <div class="form-group">
            <label class="login-font">Email Address</label>
            <input type="email" class="form-control" name="email" maxlength="30" value="<?php echo @$email; ?>" required>
          </div>
          
           <div class="form-group">
            <label class="login-font">Password</label>
            <input type="password" class="form-control" name="password" maxlength="20" required>
          </div>
          <div class="form-group">
            <small class="pull-left">
              <a href="#" class="login-link" data-toggle="modal" data-target="#forgetpass" data-dismiss="modal" aria-label="Close">Forgot Password?</a>
            </small>
            <small class="pull-right">
              Don't Have an Account? <a href="#" class="login-link" data-toggle="modal" data-target="#register" data-dismiss="modal" aria-label="Close">Register Now</a>
            </small>
          </div><br>
          <div class="form-group text-center mt30">
             <input type="submit" class="btn bc3 btn-login" name="login" value="Log In">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<?php
if(isset($forget)) {
	$mail=trim(addslashes($mail));
	$chemail=$db->check1column("register", "email", $mail);
	if($chemail==1) {
		$pass=rand(11111,99999);
		$epass=md5($pass);
		$db->insertrec("update register set password='$pass', encr_password='$epass' where email='$mail'");
		$subject="Password Reset Email";
		$message="<b><i>We have changed your password as per your request.<br/> Kindly use the below password to login to your account.</i></b><br/><br/><b>New Password</b>: $pass";
		$com_obj->email($siteemail, $mail, $subject, $message);
		echo '<script>$(window).load(function(){$("#forgetpass").modal("show"); });</script>';
		$resp="<div class='alert alert-success'>We have sent you a mail with new password. Kindly check your mail.</div>";
	}
	else {
		echo '<script>$(window).load(function(){$("#forgetpass").modal("show"); });</script>';
		$resp="<div class='alert alert-danger'>Enter valid email!</div>";
	}
}
?>
<div class="modal fade" id="forgetpass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header avd-serbg">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="<?php echo $siteurl; ?>/assets/images/close-icon.png"></span></button>
            <h4 class="modal-title mode-tit text-center banner-font-1" style="color:#FFF;" id="myModalLabel">Get Password</h4>
            </div>
      <div class="modal-body">
        <form action="" method="post">
			<?php echo isset($resp)?$resp:''; ?>
           <div class="form-group">
            <label class="login-font">Email Address</label>
            <input type="email" name="mail" required class="form-control">
          </div>
          <div class="form-group">
            <small class="pull-left">
              Already have an account? <a href="#" class="login-link" data-toggle="modal" data-target="#login" data-dismiss="modal" aria-label="Close">Login Here</a>
            </small>
            <small class="pull-right">
             Don't Have an Account? <a href="#" class="login-link" data-toggle="modal" data-target="#register" data-dismiss="modal" aria-label="Close">Register Now</a>
            </small>
			
			
          </div>
          <div class="form-group text-center">
             <button style="margin-top:30px;" type="submit" name="forget" class="btn bc3 btn-login">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header avd-serbg">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="<?php echo $siteurl; ?>/assets/images/close-icon.png"></span></button>
                    <h4 class="modal-title mode-tit text-center banner-font-1" style="color:#FFF;" id="myModalLabel">Register</h4>
                  </div>
      <div class="modal-body">
        <form id="rform" action="<?php echo $siteurl; ?>/signup" method="post">
          <div class="form-group">
            <label class="login-font">Full Name</label>
            <input type="text" class="form-control" name="fullname" maxlength="20" value="<?php echo @$fullname; ?>">
          </div>
          
          <div class="form-group">
            <label class="login-font">Email Address 
			<?php
			if(isset($_REQUEST['rmerr'])) { 
				echo '&bull; <span class="err">Email address already used!</span>';
			} 
			?>
			</label>
            <input type="email" class="form-control" name="email" maxlength="30" value="<?php echo @$email; ?>">
          </div>
          
          <div class="form-group">
            <label class="login-font">Mobile Number</label>
            <input type="text" class="form-control" name="mobile" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="<?php @$mobile; ?>">
          </div>
          
           <div class="form-group">
            <label class="login-font">Password</label>
            <input type="password" class="form-control" name="password" maxlength="20" value="<?php @$pass; ?>">
          </div>
          
          
          <div class="form-group">
            <label class="login-font">Confirm Password 
			<?php
			if(isset($_REQUEST['rperr'])) {
				echo '&bull; <span class="err">Password does not match!</span>';
			} ?></label>
            <input type="password" class="form-control" name="cpassword" maxlength="20" value="<?php @$cpass; ?>">
          </div>
          
          <div class="form-group">
            <small class="pull-right">Already have an account? <a href="#" class="login-link" data-toggle="modal" data-target="#login" data-dismiss="modal" aria-label="Close">Login Here</a></small><br>
          </div>
          <div class="form-group text-center mt30">
             <input type="submit" class="btn bc3 btn-login" name="signup" value="Register">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<!--HEAD-->
<div class="container-fluid header">
 <div class="container">
     <div class="col-md-2 col-sm-6 col-xs-12 row-fluid">
       <i class="fa fa-map-marker" aria-hidden="true"></i>
       <select class="herader-drop" onchange="location=this.options[this.selectedIndex].value;">
			<?php
			$loc=isset($_REQUEST['srchloc'])?$_REQUEST['srchloc']:'';
			$cityrec=$db->get_all("select location,locationid from listings group by locationid");
			$disp="<option value='$siteurl/index.php'>Select Your City</option>";
			for($i=0;$i<10;$i++){
				$locationid=$cityrec[$i]['locationid'];
				if($locationid){
					$location=ucwords($cityrec[$i]['location']);
					$citycount=$db->singlerec("select count(*) as tot from listings where locationid='$locationid' and post_sts=1");
					$stcount=$citycount['tot'];
					if($loc==$location) $st="selected";
					else $st="";
					$disp .="<option value='$siteurl/property-list?srchloc=$location' $st>$location</option>";
				}
			}
			echo $disp;
			?>
       </select>
     </div>
    <div class="col-md-2 col-sm-6 col-xs-12 row-fluid" style="font-size:14px;">
         <i class="fa fa-phone fa-3 phone"></i><?php echo $sitemobile; ?>
    </div><!--col-md-3-->
    <div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-2 text-right row-fluid">
       <ul class="headlist">
	        <li><a href="<?php echo $siteurl.'/post-ad'; ?>" class="add-prop" style="color:#FFF !important;"><i class="fa fa-plus" aria-hidden="true"></i> Add Property</a></li>
			<?php
			
			if(!@$_SESSION['usr']) {
                   echo '<li><a href="" data-toggle="modal" data-target="#login">Login</a></li>';
			       echo '<li><a href="" data-toggle="modal" data-target="#register">Register</a></li>';
			} else {
			        echo '<li><a href="'.$siteurl.'/my-account"><img src="'.$siteurl.'/assets/images/social1.png" alt="">&nbsp;My Account</a></li>';
			    }
			?>
            <li><a href="<?php echo $siteurl; ?>/">Home</a></li>
            <li class="hidden"><a href="#">About Us</a></li> 
            <li><a href="<?php echo $siteurl; ?>/contact-us">Contact</a></li>
            <?php
			if(isset($_SESSION['usr'])) {			
                echo '<li><a href="'.$siteurl.'/logout">Logout</a></li>';
			}
			?>
       </ul>       
    </div><!--col-md-4-->
  
 </div><!--container-->
</div><!--container-fluid-->
<!--END HEAD-->


<!--LOGO-->

 <div class="container">
  <div class="col-md-4">
      <!--<a href="<?php echo $siteurl; ?>/"><img src="<?php echo $siteurl; ?>/assets/images/logo.png" class="img-responsive" /></a>-->
	  <a href="<?php echo $siteurl; ?>/"><img src="<?php echo $siteurl; ?>/admin/uploads/general_setting/<?=$sitelogo;?>" class="img-responsive" /></a>
  </div> <!--col-md-12-->
  <div class="col-md-5">
     <div id="carousel-example-generic-ad" class="carousel slide" data-ride="carousel">
          <!-- Wrapper for slides -->
          <div class="carousel-inner mt5 mb5" role="listbox">
			<?php /* $homehd=$db->get_all("select img,name,googleadsurl from ads where googleadsurl !='' and adstype='1' and active_status='1'");
			if(count($homehd) !=0){
				for($hd=0;$hd<count($homehd);$hd++){
					$hdimg=$homehd[$hd]['img'];
					$hdurl=$homehd[$hd]['name'];
					$adsurl=$homehd[$hd]['googleadsurl'];
					if($hd==0)
						echo "<div class='item active'>$adsurl</div>";
					else
						echo "<div class='item'>$adsurl</div>";
				}
			}
			else{ */
				$homehd=$db->get_all("select img,name,googleadsurl from ads where img !='noimage.jpg' and adstype='1' and active_status='1'");
				for($hd=0;$hd<count($homehd);$hd++){
					$hdimg=$homehd[$hd]['img'];
					$hdurl=$homehd[$hd]['name'];
					$adsurl=$homehd[$hd]['googleadsurl'];				
					if($hd==0)
						echo "<div class='item active'><a href='$hdurl'><img src='admin/uploads/icon/$hdimg' class='img-responsive'></a></div>";
					else
						echo "<div class='item'><a href='$hdurl'><img src='admin/uploads/icon/$hdimg' class='img-responsive'></a></div>";
				}
			//}
			?>
          </div>
		  <?php if(count($homehd) !=0) { ?>
          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic-ad" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic-ad" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
		<?php } ?>
        </div>
     
  </div> <!--col-md-12-->
 </div><!--container-->

<!--END LOGO-->


<!--MAIN MENU-->

<nav class="navbar navbar-default">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse-1">
          <ul class="nav navbar-nav navbar-left">
            <li class="menu-li-list-2"><a  href="<?php echo $siteurl; ?>/">Home</a></li>
			<?php
			$PS_Categ=$db->get_all("select filtername,cat_name from category where cat_status='1'");
			foreach($PS_Categ as $PSCateg):
			$catt=$PSCateg['cat_name'];
			$fcat=$PSCateg['filtername'];
			if(isset($cat)==$fcat) $ch="selected";
			else $ch="";
			echo '<li class="menu-li-list"><a  href="'.$siteurl.'/property-list.php?cat='.$fcat.'">'.$catt.'</a></li>';
			endforeach;
			?>
			<li class="menu-li-list"><a  href="<?php echo $siteurl; ?>/agent-list">Agents</a></li>
            <li class="menu-li-list"><a  href="<?php echo $siteurl; ?>/new-projects.php">New Projects</a></li>
              
              
            <!-- <li class="menu-li-list"><a  href="<?php echo $siteurl; ?>/post-ad">Post Ad</a></li>
             <li class="menu-li-list"><a  href="<?php echo $siteurl; ?>/category/residential">Residential</a></li>
              <li class="menu-li-list"><a  href="<?php echo $siteurl; ?>/category/commercial">Commercial</a></li>
              <li class="menu-li-list"><a  href="<?php echo $siteurl; ?>/category/industrial">Industrial</a></li>
              <li class="menu-li-list"><a  href="<?php echo $siteurl; ?>/category/bspace">Business Space</a></li>
              <li class="menu-li-list"><a  href="<?php echo $siteurl; ?>/category/overseas">Overseas</a></li>
              <li class="menu-li-list"><a  href="<?php echo $siteurl; ?>/category/bforsale">Business For Sale</a></li>
              <li class="menu-li-list"><a  href="<?php echo $siteurl; ?>/category/market">Marketplace</a></li>-->

          </ul>
          
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->
<!---------------------------------------------END MAIN MENU-------------------------------------------------------->