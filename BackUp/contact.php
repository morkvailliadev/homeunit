<?
include "admin/AMframe/config.php";

if(isset($cont_agent)) {
	$name=trim(addslashes($name));
	$email=trim(addslashes($email));
	$mobile=trim(addslashes($mobile));
	$sub=trim(addslashes($sub));
	$message=trim(addslashes($message));
	if(isset($_SESSION['usr'])) { $fmail=$_SESSION['usr']; }
	else { $fmail=$email; }
	$tomail=$_SESSION['tomail'];
	$msg="The user <i><b>$name</b></i> is enquired about your property on property listing. If you are interested you can respond to this email.<br><br>Enquiry Details:<br><br>Name: <i><b>$name</b></i><br>Email: <i><b>$fmail</b></i><br>Subject: <i><b>$sub</b></i><br>Message: <i><b>$message</b></i><br>";
	$com_obj->email($fmail, $tomail, "Response for your property from property listing", $msg);
	unset($_SESSION['tomail']);
	echo "<script>location.href='$siteurl?cmailsent'</script>";
	header("Location: $siteurl?cmailsent"); exit;
}
if(isset($cont_us)) {
	$name=trim(addslashes($name));
	$email=trim(addslashes($email));
	$mobile=trim(addslashes($mobile));
	$message=trim(addslashes($message));
	$sub="Message from site contact form";
	$msg="The user <i><b>$name</b></i> tried to contact you through the contact form.<br><br>Name: <i><b>$name</b></i><br>email: <i><b>$email</b></i><br>Mobile: <i><b>$mobile</b></i><br>Message: <i><b>$message</b></i><br>";
	$com_obj->email($email, $siteemail, $sub, $msg);
	echo "<script>location.href='$siteurl?cmailsent'</script>";
	header("Location: $siteurl?cmailsent"); exit;
}
else {
	echo "<script>location.href='$siteurl'</script>";
	header("Location: $siteurl"); exit;
}
?>