<?php include"header.php"; ?>
<div class="boxed">
	<!--CONTENT CONTAINER-->
	<!--===================================================-->
	<div id="content-container">
		<?php //include "header_nav.php"; ?>
		<div class="pageheader">
			<h3><i class="fa fa-home"></i>Google ads </h3>
			<div class="breadcrumb-wrapper">
				<span class="label">You are here:</span>
				<ol class="breadcrumb">
					<li> <a href="welcome.php"> Home </a> </li>
					<li class="active"> Google ads </li>
				</ol>
			</div>
		</div>
<?
$upd = isset($upd)?$upd:'';
$id = isSet($id) ? $id : '' ;
$act = isSet($act) ? $act : '' ;
$page = isSet($page) ? $page : '' ;
$Message = isSet($Message) ? $Message : '' ;
$googleadsurl = isSet($googleadsurl) ? $googleadsurl : '' ;

if($submit) {
    $crcdt = time();
	$googleadsurl = trim(addslashes($googleadsurl));
	$set  = "googleadsurl = '$googleadsurl'";
	$set  .= ",adstype = '$adstype'";
	$set  .= ",ipaddr = '$ipaddress'";			
	if($upd == 1){
		$set  .= ",crcdt = '$crcdt'";    
		$set  .= ",active_status = '1'";
		$set  .= ",usercre = '$usrcre_name'";
		$idvalue = $db->insertid("insert into ads set $set");
		$act = "add";
	}
	else if($upd == 2){
		$set  .= ",chngdt = '$crcdt'";    
		$set  .= ",userchng = '$usrcre_name'";
		$db->insertrec("update ads set $set where id='$idvalue'");
		$act = "upd";
	}
	header("location:googleadsmgmt.php?act=$act");
	exit;
}

if($upd == 1){
	$TextChange = "Add";
}
else if($upd == 2){
	$TextChange = "Edit";
}
	
$GetRecord = $db->singlerec("select * from ads where id='$id'");
@extract($GetRecord);
$name = stripslashes($name);
?>
		<!--Page content-->
		<!--===================================================-->
		<div id="page-content">
			<div class="row">
			  <div class="eq-height">
				 <div class="col-sm-6 eq-box-sm">
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"><?php echo $TextChange;?> Google ads <?echo $Message;?></h3>
						</div>
						<form class="form-horizontal" method="post" action="googleads.php" enctype="multipart/form-data">
							<input type="hidden" name="idvalue" value="<?echo $id;?>" />
							<input type="hidden" name="upd" value="<?echo $upd;?>" />							
							<div class="panel-body">
								<table style="padding:25px;">
									<tr>
										<td>Type of ads</td>
										<td><select name="adstype" class="form-control"><?php echo $com_obj->dropdowninitial($PS_Ads,$adstype,"");?></select></td>
									</tr>
									<tr>
										<td>google address code <font color="red">*</font></td>
										<td valign="top"><textarea name="googleadsurl" class="form-control" cols="40" rows="5"><?php echo $googleadsurl; ?></textarea>
										</td>
									</tr>
									<tr><td></td><td><br/><?echo $googleadsurl;?></td></tr>
								</table>
							</div>
							<div class="panel-footer text-left">
								<div class="col-md-4  text-right"><input class="btn btn-info" type="submit" name="submit" value="Submit"></div>
								<a class="btn btn-info" href="googleadsmgmt.php">Cancel</a>
							</div>
						</form>
						<!--===================================================-->
						<!--End Horizontal Form-->
					</div>
				</div>
			  </div>
			</div>
		</div>
		<!--===================================================-->
		<!--End page content-->
	</div>
	<!--===================================================-->
	<!--END CONTENT CONTAINER-->
	<?php include "leftmenu.php"; ?>
</div>
<?php include "footer.php"; ?>