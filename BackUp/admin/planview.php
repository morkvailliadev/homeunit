<?php include "header.php"; ?>
            <!--===================================================-->
            <!--END NAVBAR-->
            <div class="boxed">
                <!--CONTENT CONTAINER-->
                <!--===================================================-->
                <div id="content-container">
                    <?php include "header_nav.php"; ?>
                    <div class="pageheader">
                        <h3><i class="fa fa-home"></i> Membership Plans </h3>
                        <div class="breadcrumb-wrapper">
                            <span class="label">You are here:</span>
                            <ol class="breadcrumb">
                                <li> <a href="welcome"> Home </a> </li>
                                <li class="active"> Membership Plans </li>
                            </ol>
                        </div>
                    </div>
<?

$id = isSet($id) ? $id : '' ;
$page = isSet($page) ? $page : '' ;
$name = isSet($name) ? $name : '' ;

$GetRecordView = $db->singlerec("select * from membership_plans where id='$id'");
@extract($GetRecordView);
$GetRefereBy = $db->get_all("select * from membership_plans where id='$id'");
@extract($GetRefereBy);

?>
                   <!--Page content-->
                    <!--===================================================-->
                    <div id="page-content">
                        <!-- Basic Data Tables -->
                        <!--===================================================-->
						<h3>View Plan Details</h3><a href="plan"  class="btn btn-success">Back</a></br>
                        <div class="panel">
                            <div class="panel-heading">
                            </div>
                            <div class="panel-body">
								<table>
                                    <thead>
                                        <tr>
											<td>Plan Name</td><td width="75">:</td><td><?php echo $name;?></td>
											</tr>
											<tr>
											<td>Cost</td><td width="75">:</td><td><?php echo "$$cost"; ?></td>
											</tr>
											<tr>
											<td>Can Post</td><td width="75">:</td><td><?php echo "$canpost properties"; ?></td>
											</tr>
											<tr>
											<td>Can Feature</td><td width="75">:</td><td><?php echo "$canfeature properties"; ?></td>
											</tr>
											<tr>
											<td>Validity</td><td width="75">:</td><td><?php echo "$validity days"; ?></td>
											</tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--===================================================-->
                    <!--End page content-->
                </div>
                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
			<?php include "leftmenu.php"; ?>
            </div>
<?

include "footer.php";
?>