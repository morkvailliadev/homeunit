<?
include "header.php";
$totusr=$db->singlerec("select count(*) from register");
$actusr=$db->singlerec("select count(*) from register where active=1");
$penusr=$db->singlerec("select count(*) from register where active=0");
$totpr=$db->singlerec("select count(*) from listings");
$actpr=$db->singlerec("select count(*) from listings where post_sts=1");
$penpr=$db->singlerec("select count(*) from listings where post_sts=0");
?>

	<div class="boxed">
		<!--CONTENT CONTAINER-->
		<!--===================================================-->
		<div id="content-container">
			<?php include "header_nav.php"; ?>
			<!--Page Title-->
			<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
			<div class="pageheader">
				<h3><i class="fa fa-home"></i> Dashboard </h3>
				<div class="breadcrumb-wrapper">
					<span class="label">You are here:</span>
					<ol class="breadcrumb">
						<li> <a href="#"> Home </a> </li>
						<li class="active"> Dashboard </li>
					</ol>
				</div>
			</div>
			<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
			<!--End page title-->
			<!--Page content-->
			<!--===================================================-->
			<div id="page-content">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-6 col-xm-12">
						<!--Registered User-->
						<div class="panel media pad-all">
							<div class="media-left">
								<span class="icon-wrap icon-wrap-sm icon-circle bg-info">
								<i class="fa fa-users fa-2x" aria-hidden="true"></i>
								</span>
							</div>
							<div class="media-body">
								<p class="text-2x mar-no text-thin text-right"><?php echo $totusr[0]; ?></p>
								<p class="h5 mar-no text-right">Total Users</p>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xm-12">
						<!--New Order-->
						<div class="panel media pad-all">
							<div class="media-left">
								<span class="icon-wrap icon-wrap-sm icon-circle bg-success">
								<i class="fa fa-user fa-2x" aria-hidden="true"></i>
								</span>
							</div>
							<div class="media-body">
								<p class="text-2x mar-no text-thin text-right"><?php echo $actusr[0]; ?></p>
								<p class="h5 mar-no text-right">Active Users</p>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xm-12">
						<!--Comments-->
						<div class="panel media pad-all">
							<div class="media-left">
								<span class="icon-wrap icon-wrap-sm icon-circle bg-danger">
								<i class="fa fa-user-plus fa-2x" aria-hidden="true"></i>
								</span>
							</div>
							<div class="media-body">
								<p class="text-2x mar-no text-thin text-right"><?php echo $penusr[0]; ?></p>
								<p class="h5 mar-no text-right">Pending users</p>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xm-12">
						<!--Sales-->
						<div class="panel media pad-all">
							<div class="media-left">
								<span class="icon-wrap icon-wrap-sm icon-circle bg-info">
								<i class="fa fa-home fa-2x" aria-hidden="true"></i>
								</span>
							</div>
							<div class="media-body">
								<p class="text-2x mar-no text-thin text-right"><?php echo $totpr[0]; ?></p>
								<p class="h5 mar-no text-right">Total properties</p>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xm-12">
						<!--Sales-->
						<div class="panel media pad-all">
							<div class="media-left">
								<span class="icon-wrap icon-wrap-sm icon-circle bg-success">
								<i class="fa fa-home fa-2x" aria-hidden="true"></i>
								</span>
							</div>
							<div class="media-body">
								<p class="text-2x mar-no text-thin text-right"><?php echo $actpr[0]; ?></p>
								<p class="h5 mar-no text-right">Active properties</p>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xm-12">
						<!--Sales-->
						<div class="panel media pad-all">
							<div class="media-left">
								<span class="icon-wrap icon-wrap-sm icon-circle bg-danger">
								<i class="fa fa-home fa-2x" aria-hidden="true"></i>
								</span>
							</div>
							<div class="media-body">
								<p class="text-2x mar-no text-thin text-right"><?php echo $penpr[0]; ?></p>
								<p class="h5 mar-no text-right">Pending properties</p>
							</div>
						</div>
					</div>
				</div>
				<!--<div class="row">
				   <div class="col-md-12"> 
                        <div class="panel">
						<div class="panel-headin">
                                <h3 class="panel-title"><?echo $Message;?></h3>
                            </div>
                            <div class="panel-body">

							    <table id="demo-dt-basic" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
											<th>S.No</th>
											<th>Image</th>
											<th>Title</th>
											<th>Email</th>
											<th>Location</th>
											<th>Address</th>
											<th>Price</th>
											<th>Category</th>
											<th>Property For</th>
											<th>Posted at</th>
											<th class='cntrhid'>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody><?echo $disp; ?></tbody>
                                </table>
                            </div>
                        </div>
						</div>
				</div>
				
				
				
				<div class="row">
				    <div class="col-md-6">

                        <div class="panel">
                            <div class="panel-headin">
                                <h3 class="panel-title"><?echo $Message;?></h3>
                            </div>
                            <div class="panel-body">
                                <table id="demo-dt-basic" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
											<th>S.No</th>
											<th>Image</th>
											<th>Name</th>										
											<th>Email Id</th>
											<th>Mobile</th>																	
											<th>Created at</th>
											
                                        </tr>
                                    </thead>
                                    <tbody><?echo $disp; ?></tbody>
                                </table>
                            </div>
                        </div>
					</div>
					
					
					
					<div class="col-md-6">

                        <div class="panel">
                            <div class="panel-headin">
                                <h3 class="panel-title"><?echo $Message;?></h3>
                            </div>
                            <div class="panel-body">
                                <table id="demo-dt-basic" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
											<th>S.No</th>
											<th>Image</th>
											<th>Name</th>										
											<th>Email Id</th>
											<th>Mobile</th>																	
											<th>Created at</th>
											
                                        </tr>
                                    </thead>
                                    <tbody><?echo $disp; ?></tbody>
                                </table>
                            </div>
                        </div>
					</div>
				</div>-->
				
				
				
				
				
				
				
				
				
				<div class="row">
						  <div class="col-md-12">
						    <h3>General Setting</h3>
							<div class="col-sm-3 col-xs-4 ">
							  <div class="well admin-hme">
							   <a href="registerupd?upd=1"><div class="circle-icon blue">
							    <i class="fa fa-3x fa-user" aria-hidden="true"></i>
							   </div>
							   Add User</a>
							   </div>
							</div>
							
							
							<div class="col-sm-3 col-xs-4 ">
							  <div class="well admin-hme">
							   <a href="register"><div class="circle-icon green">
							    <i class="fa fa-3x fa-users" aria-hidden="true"></i>
							   </div>
							   Manage User</a>
							   </div>
							</div>
							
							
							
							
							
							<div class="col-sm-3 col-xs-4 ">
							  <div class="well admin-hme">
							   <a href="general"><div class="circle-icon red">
							    <i class="fa fa-3x fa-cogs" aria-hidden="true"></i>
							   </div>
							    General Setting</a>
							   </div>
							</div>
							
							
							<div class="col-sm-3 col-xs-4 ">
							  <div class="well admin-hme">
							   <a href="cms"><div class="circle-icon purple">
							    <i class="fa fa-3x fa-file-text-o" aria-hidden="true"></i>
							   </div>
							    CMS Setting</a>
							   </div>
							</div>
							
							<div class="col-sm-3 col-xs-4 ">
							  <div class="well admin-hme">
							   <a href="testimonial"><div class="circle-icon orag">
							    <i class="fa fa-3x fa-comments-o" aria-hidden="true"></i>
							   </div>
							   Manage Testimonial</a>
							   </div>
							</div>
							
							
							<div class="col-sm-3 col-xs-4 ">
							  <div class="well admin-hme">
							   <a href="testimonialupd?upd=1"><div class="circle-icon brwn">
							    <i class="fa fa-3x fa-pencil" aria-hidden="true"></i>
							   </div>
							   Add Testimonial</a>
							   </div>
							</div>
							
							<div class="col-sm-3 col-xs-4 ">
							  <div class="well admin-hme">
							   <a href="news"><div class="circle-icon yellow">
							    <i class="fa fa-3x fa-newspaper-o" aria-hidden="true"></i>
							   </div>
							    Manage News</a>
							   </div>
							</div>
							
							<div class="col-sm-3 col-xs-4 ">
							  <div class="well admin-hme">
							   <a href="newsupd?upd=1"><div class="circle-icon ylet">
							    <i class="fa fa-3x fa-pencil-square-o" aria-hidden="true"></i>
							   </div>
							   Add News</a>
							   </div>
							</div>
						  </div>
						  
						  <div class="col-md-12">
						      <h3>Property Management</h3>
							  
							<div class="col-sm-3 col-xs-4 ">
							  <div class="well admin-hme">
							    <a href="propupd?upd=1"><div class="circle-icon blue">
							    <i class="fa fa-3x fa-home"></i>
							   </div>
							   Add Property</a>
							   </div>
							</div>
							
							<div class="col-sm-3 col-xs-4 ">
							  <div class="well admin-hme">
							    <a href="prop"><div class="circle-icon green">
							   <i class="fa fa-3x fa-building-o" aria-hidden="true"></i>
							   </div>
							   Manage Property</a>
							   </div>
							</div>

						  </div>
						  
						  
						  
						  <div class="col-md-12">
						      <h3>Others</h3>
							  
							<div class="col-sm-3 col-xs-4 ">
							  <div class="well admin-hme">
							    <a href="newsletterupd?upd=1"><div class="circle-icon red">
							    <i class="fa fa-3x fa-envelope-o"></i>
							   </div>
							   Add Newsletter</a>
							   </div>
							</div>
							
							<div class="col-sm-3 col-xs-4 ">
							  <div class="well admin-hme">
							    <a href="newsletter"><div class="circle-icon purple">
							   <i class="fa fa-3x fa-envelope-o" aria-hidden="true"></i>
							   </div>
							   Manage Newsletter</a>
							   </div>
							</div>
							
						
							
							<div class="col-sm-3 col-xs-4 ">
							  <div class="well admin-hme">
							    <a href="plan"><div class="circle-icon yellow">
							   <i class="fa fa-3x fa-shield" aria-hidden="true"></i>
							   </div>
							   Manage Membership Plan</a>
							   </div>
							</div>

						  </div>
						  
						  
						  
						  
						  
						  
						  
						  
						  
						</div>
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				<div class="row hidden">
					<div class="col-lg-8">
						<div class="panel">
							<div class="panel-heading">
								<h3 class="panel-title"> Order Statistics </h3>
							</div>
							<div class="panel-body">
								<div class="col-md-8">
									<!-- World Map -->
									<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
									<div id="world-map-markers" style="height: 300px"></div>
									<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
								</div>
								<div class="col-md-4">
									<!-- Progress bars Start -->
									<div class="clearfix"> 
										<span class="pull-left">In Queue</span> 
										<small class="pull-right">45%</small> 
									</div>
									<div class="pad-btm">
										<div class="progress progress-striped progress-sm">
											<div class="progress-bar progress-bar-info" style="width: 55%;"></div>
										</div>
									</div>
									<!-- Progress bars End -->                                
									<!-- Progress bars Start -->
									<div class="clearfix"> 
										<span class="pull-left">Shipped Products</span> 
										<small class="pull-right">350/500</small> 
									</div>
									<div class="pad-btm">
										<div class="progress progress-striped progress-sm">
											<div class="progress-bar progress-bar-primary" style="width: 55%;"></div>
										</div>
									</div>
									<!-- Progress bars End -->                                
									<!-- Progress bars Start -->
									<div class="clearfix"> 
										<span class="pull-left">Returned Products</span> 
										<small class="pull-right">50/500</small> 
									</div>
									<div class="pad-btm">
										<div class="progress progress-striped progress-sm">
											<div class="progress-bar progress-bar-warning" style="width: 55%;"></div>
										</div>
									</div>
									<!-- Progress bars End -->                                
									<!-- Progress bars Start -->
									<div class="clearfix"> 
										<span class="pull-left">Pending Deliveries</span> 
										<small class="pull-right">150/500</small> 
									</div>
									<div class="pad-btm">
										<div class="progress progress-striped progress-sm">
											<div class="progress-bar progress-bar-danger" style="width: 55%;"></div>
										</div>
									</div>
									<!-- Progress bars End -->                                
									<!-- Progress bars Start -->
									<div class="clearfix"> 
										<span class="pull-left">Project 2</span> 
										<small class="pull-right">32%</small> 
									</div>
									<div class="pad-btm">
										<div class="progress progress-striped progress-sm">
											<div class="progress-bar progress-bar-info" style="width: 55%;"></div>
										</div>
									</div>
									<!-- Progress bars End -->                                
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="panel">
							<div class="panel-heading">
								<h3 class="panel-title">Monthly Statistics <small> based on the User Activities </small></h3>
							</div>
							<div class="panel-body pad-no">
								<!--Default Accordion--> 
								<!--===================================================-->
								<div class="panel-group accordion mar-no" id="accordion1">
									<div class="panel defaultpanel">
										<!--Accordion title-->
										<div class="panel-heading">
											<h4 class="panel-title"> <a data-parent="#accordion1" data-toggle="collapse" href="#accordionOne"> <i class="fa fa-calendar"></i> This Month</a> 
											</h4>
										</div>
										<!--Accordion content-->
										<div class="panel-collapse collapse in" id="accordionOne">
											<div class="panel-body pad-no">
												<table class="table mar-no bg-light-gray">
													<tbody>
														<tr>
															<td class="text-center">1</td>
															<td>Google Chrome</td>
															<td class="center">4909</td>
															<td><i class="fa fa-caret-down text-danger"></i></td>
														</tr>
														<tr>
															<td class="text-center">2</td>
															<td>Google Chrome</td>
															<td class="center">4909</td>
															<td><i class="fa fa-caret-down text-danger"></i></td>
														</tr>
														<tr>
															<td class="text-center">3</td>
															<td>Google Chrome</td>
															<td class="center">4909</td>
															<td><i class="fa fa-caret-down text-danger"></i></td>
														</tr>
														<tr>
															<td class="text-center">4</td>
															<td>Google Chrome</td>
															<td class="center">4909</td>
															<td><i class="fa fa-caret-down text-danger"></i></td>
														</tr>
														<tr>
															<td class="text-center">5</td>
															<td>Google Chrome</td>
															<td class="center">4909</td>
															<td><i class="fa fa-caret-down text-danger"></i></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="panel defaultpanel">
										<!--Accordion title-->
										<div class="panel-heading">
											<h4 class="panel-title"> <a data-parent="#accordion1" data-toggle="collapse" href="#accordionTwo"> <i class="fa fa-calendar"></i> Last Month</a> </h4>
										</div>
										<!--Accordion content-->
										<div class="panel-collapse collapse" id="accordionTwo">
											<div class="panel-body pad-no">
												<table class="table mar-no bg-light-gray">
													<tbody>
														<tr>
															<td class="text-center">1</td>
															<td>Google Chrome</td>
															<td class="center">4909</td>
															<td><i class="fa fa-caret-down text-danger"></i></td>
														</tr>
														<tr>
															<td class="text-center">2</td>
															<td>Google Chrome</td>
															<td class="center">4909</td>
															<td><i class="fa fa-caret-down text-danger"></i></td>
														</tr>
														<tr>
															<td class="text-center">3</td>
															<td>Google Chrome</td>
															<td class="center">4909</td>
															<td><i class="fa fa-caret-down text-danger"></i></td>
														</tr>
														<tr>
															<td class="text-center">4</td>
															<td>Google Chrome</td>
															<td class="center">4909</td>
															<td><i class="fa fa-caret-down text-danger"></i></td>
														</tr>
														<tr>
															<td class="text-center">5</td>
															<td>Google Chrome</td>
															<td class="center">4909</td>
															<td><i class="fa fa-caret-down text-danger"></i></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="panel defaultpanel">
										<!--Accordion title-->
										<div class="panel-heading">
											<h4 class="panel-title"> <a data-parent="#accordion1" data-toggle="collapse" href="#accordionThree"> <i class="fa fa-calendar"></i> Last Quarter</a> </h4>
										</div>
										<!--Accordion content-->
										<div class="panel-collapse collapse" id="accordionThree">
											<div class="panel-body pad-no">
												<table class="table mar-no bg-light-gray">
													<tbody>
														<tr>
															<td class="text-center">1</td>
															<td>Google Chrome</td>
															<td class="center">4909</td>
															<td><i class="fa fa-caret-down text-danger"></i></td>
														</tr>
														<tr>
															<td class="text-center">2</td>
															<td>Google Chrome</td>
															<td class="center">4909</td>
															<td><i class="fa fa-caret-down text-danger"></i></td>
														</tr>
														<tr>
															<td class="text-center">3</td>
															<td>Google Chrome</td>
															<td class="center">4909</td>
															<td><i class="fa fa-caret-down text-danger"></i></td>
														</tr>
														<tr>
															<td class="text-center">4</td>
															<td>Google Chrome</td>
															<td class="center">4909</td>
															<td><i class="fa fa-caret-down text-danger"></i></td>
														</tr>
														<tr>
															<td class="text-center">5</td>
															<td>Google Chrome</td>
															<td class="center">4909</td>
															<td><i class="fa fa-caret-down text-danger"></i></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<!--===================================================--> 
								<!--End Default Accordion--> 
							</div>
						</div>
					</div>
				</div>
				<div class="row hidden">
					<div class="col-lg-4">
						<!--Panel with Header-->
						<!--===================================================-->
						<div class="panel">
							<div class="panel-body">
								<div class="carousel slide" id="c-slide" data-ride="carousel">
									<div class="carousel-inner">
										<div class="item active">
											<div class="text-bold"> Task in Progress </div>
											<div class="pad-ver-5"> Navigation Illustration </div>
											<!-- Progress bars Start -->                                
											<div class="pad-btm">
												<div class="progress progress-striped progress-sm">
													<div class="progress-bar progress-bar-info" style="width: 55%;"></div>
												</div>
											</div>
											<!-- Progress bars End -->
											<div class="eq-height">
												<div class="text-dark"> Status : </div>
												<div class="text-dark text-lg pull-left pad-ver-5"> Active </div>
												<div class="text-dark pull-right"> 
													<a href="#" class="btn btn-info btn-sm"> 
													<i class="fa fa-pause pad-rgt-5"></i> Pause 
													</a> 
													<a href="#" class="btn btn-info btn-sm"> 
													<i class="fa fa-check pad-rgt-5"></i> Complete 
													</a> 
												</div>
											</div>
											<hr>
											<ul class="list-inline">
												<li class="text-bold pull-left pad-ver-5"> Assigned to : </li>
												<li class="pull-right"><img src="img/av1.png" class="img-rounded img-sm" alt=""></li>
												<li class="pull-right"><img src="img/av1.png" class="img-rounded img-sm" alt=""></li>
												<li class="pull-right"><img src="img/av1.png" class="img-rounded img-sm" alt=""></li>
											</ul>
										</div>
										<div class="item">
											<div class="text-bold"> Task in Progress </div>
											<div class="pad-ver-5"> App Usability Testing </div>
											<!-- Progress bars Start -->                                
											<div class="pad-btm">
												<div class="progress progress-striped progress-sm">
													<div class="progress-bar progress-bar-info" style="width: 55%;"></div>
												</div>
											</div>
											<!-- Progress bars End -->
											<div class="eq-height">
												<div class="text-dark"> Status : </div>
												<div class="text-dark text-lg pull-left pad-ver-5"> Active </div>
												<div class="text-dark pull-right"> 
													<a href="#" class="btn btn-info btn-sm"> 
													<i class="fa fa-pause pad-rgt-5"></i> Pause 
													</a> 
													<a href="#" class="btn btn-info btn-sm"> 
													<i class="fa fa-check pad-rgt-5"></i> Complete 
													</a> 
												</div>
											</div>
											<hr>
											<ul class="list-inline">
												<li class="text-bold pull-left pad-ver-5"> Assigned to : </li>
												<li class="pull-right"><img src="img/av1.png" class="img-rounded img-sm" alt=""></li>
												<li class="pull-right"><img src="img/av1.png" class="img-rounded img-sm" alt=""></li>
												<li class="pull-right"><img src="img/av1.png" class="img-rounded img-sm" alt=""></li>
											</ul>
										</div>
										<div class="item">
											<div class="text-bold"> Task in Progress </div>
											<div class="pad-ver-5"> Web Portal Redesign </div>
											<!-- Progress bars Start -->                                
											<div class="pad-btm">
												<div class="progress progress-striped progress-sm">
													<div class="progress-bar progress-bar-info" style="width: 55%;"></div>
												</div>
											</div>
											<!-- Progress bars End -->
											<div class="eq-height">
												<div class="text-dark"> Status : </div>
												<div class="text-dark text-lg pull-left pad-ver-5"> Active </div>
												<div class="text-dark pull-right"> 
													<a href="#" class="btn btn-info btn-sm"> 
													<i class="fa fa-pause pad-rgt-5"></i> Pause 
													</a> 
													<a href="#" class="btn btn-info btn-sm"> 
													<i class="fa fa-check pad-rgt-5"></i> Complete 
													</a> 
												</div>
											</div>
											<hr>
											<ul class="list-inline">
												<li class="text-bold pull-left pad-ver-5"> Assigned to : </li>
												<li class="pull-right"><img src="img/av1.png" class="img-rounded img-sm" alt=""></li>
												<li class="pull-right"><img src="img/av1.png" class="img-rounded img-sm" alt=""></li>
												<li class="pull-right"><img src="img/av1.png" class="img-rounded img-sm" alt=""></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--===================================================-->
						<!--End Panel with Header-->
					</div>
					<div class="col-lg-4">
						<div class="panel">
							<div class="panel-heading">
								<h3 class="panel-title"> Sales Statistics </h3>
							</div>
							<div class="panel-body">
								<!--Flot Line Chart placeholder-->
								<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
								<div id="demo-sales-statistics" style="height:150px"></div>
								<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="panel">
							<div class="panel-heading">
								<h3 class="panel-title"> Earning Statistics </h3>
							</div>
							<div class="panel-body">
								<!--Flot Line Chart placeholder-->
								<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
								<div id="demo-earning-statistics" style="height:150px"></div>
								<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
							</div>
						</div>
					</div>
				</div>
				
				<div class="row hidden">
					<div class="col-lg-3">
						<div class="panel">
							<div class="panel-heading">
								<h3 class="panel-title"><span class="count-todos"></span> Items Left</h3>
							</div>
							<div class="panel-body pad-no">
								<div class="todolist not-done">
									<ul id="sortable" class="list-unstyled">
										<li class="ui-state-default">
											<div class="checkbox">
												<label class="form-checkbox form-icon active">
												<input type="checkbox"> Option 1
												</label>
											</div>
										</li>
										<li class="ui-state-default">
											<div class="checkbox">
												<label class="form-checkbox form-icon active">
												<input type="checkbox"> Option 1
												</label>
											</div>
										</li>
										<li class="ui-state-default">
											<div class="checkbox">
												<label class="form-checkbox form-icon active">
												<input type="checkbox"> Option 1
												</label>
											</div>
										</li>
										<li class="ui-state-default">
											<div class="checkbox">
												<label class="form-checkbox form-icon active">
												<input type="checkbox"> Option 1
												</label>
											</div>
										</li>
									</ul>
									<ul id="done-items" class="list-unstyled">
										<li class="ui-state-default">
											Some item <a class="remove-item fa fa-remove pull-right" href="#" role="button"></a>
										</li>
									</ul>
								</div>
							</div>
							<div class="panel-footer">
								<input type="text" class="form-control add-todo" placeholder="Enter Your TODO List ">
							</div>
						</div>
					</div>
					<div class="col-lg-3">
						<!--Profile Widget-->
						<!--===================================================-->
						<div class="panel">
							<div class="panel-body pad-no">
								<div class="media pad-all bg-primary">
									<div class="media-left">
										<i class="fa fa-facebook fa-5x"></i>
									</div>
									<div class="media-body">
										<p class="text-2x text-thin text-right">SquareDesign</p>
										<p class="h5 mar-no text-right">#squaredesign</p>
									</div>
								</div>
							</div>
							<div class="panel-footer">
								<ul class="nav nav-section nav-justified">
									<li>
										<div class="section">
											<div class="h4 mar-ver-5"> 12.5k </div>
											<p class="mar-no">Followers</p>
										</div>
									</li>
									<li>
										<div class="section">
											<div class="h4 mar-ver-5"> 1853 </div>
											<p class="mar-no">Following</p>
										</div>
									</li>
									<li>
										<div class="section">
											<div class="h4 mar-ver-5"> 3451 </div>
											<p class="mar-no">Tweets</p>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<!--===================================================-->
						<!--Profile Widget-->
						<!--===================================================-->
						<div class="panel">
							<div class="panel-body pad-no">
								<div class="media pad-all bg-info">
									<div class="media-left">
										<i class="fa fa-twitter fa-5x"></i>
									</div>
									<div class="media-body">
										<p class="text-2x text-thin text-right">SquareDesign</p>
										<p class="h5 mar-no text-right">@ravindra1982</p>
									</div>
								</div>
							</div>
							<div class="panel-footer">
								<ul class="nav nav-section nav-justified">
									<li>
										<div class="section">
											<div class="h4 mar-ver-5"> 12.5k </div>
											<p class="mar-no">Followers</p>
										</div>
									</li>
									<li>
										<div class="section">
											<div class="h4 mar-ver-5"> 1853 </div>
											<p class="mar-no">Following</p>
										</div>
									</li>
									<li>
										<div class="section">
											<div class="h4 mar-ver-5"> 3451 </div>
											<p class="mar-no">Tweets</p>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<!--===================================================-->
					</div>
					<div class="col-lg-6">
						<div class="panel">
							<div class="panel-heading">
								<h3 class="panel-title">Monthly Statistics <small> based on the User Activities </small></h3>
							</div>
							<div class="panel-body pad-no">
								<!--Default Accordion--> 
								<!--===================================================-->
								<div class="panel-group accordion mar-no" id="statistics">
									<div class="panel defaultpanel">
										<!--Accordion title-->
										<div class="panel-heading">
											<h4 class="panel-title"> 
												<a data-parent="#statistics" data-toggle="collapse" href="#statisticsone"> 
												<i class="fa fa-calendar"></i> Top Countries 
												</a> 
											</h4>
										</div>
										<!--Accordion content-->
										<div class="panel-collapse collapse in" id="statisticsone">
											<div class="panel-body pad-no">
												<table class="table mar-no bg-light-gray">
													<thead>
														<tr>
															<th>Country</th>
															<th>Unique User</th>
															<th>Pageviews</th>
															<th>Changes</th>
															<th>New Vs. Return</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>USA</td>
															<td>254152</td>
															<td>635241</td>
															<td>25%</td>
															<td>
																<div class="progress progress-striped progress-md">
																	<div style="width: 75%" class="progress-bar progress-bar-success">
																		<span class="sr-only">75% Complete (success)</span>
																	</div>
																	<div style="width: 25%" class="progress-bar progress-bar-info">
																		<span class="sr-only">25% Complete (warning)</span>
																	</div>
																</div>
															</td>
														</tr>
														<tr>
															<td>India</td>
															<td>325614</td>
															<td>524152</td>
															<td>20%</td>
															<td>
																<div class="progress progress-striped progress-md">
																	<div style="width: 45%" class="progress-bar progress-bar-success">
																		<span class="sr-only">45% Complete (success)</span>
																	</div>
																	<div style="width: 55%" class="progress-bar progress-bar-info">
																		<span class="sr-only">55% Complete (warning)</span>
																	</div>
																</div>
															</td>
														</tr>
														<tr>
															<td>United kingdom</td>
															<td>124563</td>
															<td>654525</td>
															<td>35%</td>
															<td>
																<div class="progress progress-striped progress-md">
																	<div style="width: 25%" class="progress-bar progress-bar-success">
																		<span class="sr-only">25% Complete (success)</span>
																	</div>
																	<div style="width: 75%" class="progress-bar progress-bar-info">
																		<span class="sr-only">75% Complete (warning)</span>
																	</div>
																</div>
															</td>
														</tr>
														<tr>
															<td>Brazil</td>
															<td>325412</td>
															<td>456985</td>
															<td>35%</td>
															<td>
																<div class="progress progress-striped progress-md">
																	<div style="width: 35%" class="progress-bar progress-bar-success">
																		<span class="sr-only">35% Complete (success)</span>
																	</div>
																	<div style="width: 65%" class="progress-bar progress-bar-info">
																		<span class="sr-only">65% Complete (warning)</span>
																	</div>
																</div>
															</td>
														</tr>
														<tr>
															<td>Canada</td>
															<td>124523</td>
															<td>490936</td>
															<td>40%</td>
															<td>
																<div class="progress progress-striped progress-md">
																	<div style="width: 55%" class="progress-bar progress-bar-success">
																		<span class="sr-only">55% Complete (success)</span>
																	</div>
																	<div style="width: 45%" class="progress-bar progress-bar-info">
																		<span class="sr-only">45% Complete (warning)</span>
																	</div>
																</div>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="panel defaultpanel">
										<!--Accordion title-->
										<div class="panel-heading">
											<h4 class="panel-title"> 
												<a data-parent="#statistics" data-toggle="collapse" href="#statisticsTwo"> 
												<i class="fa fa-calendar"></i> Age Group </a> 
											</h4>
										</div>
										<!--Accordion content-->
										<div class="panel-collapse collapse" id="statisticsTwo">
											<div class="panel-body pad-no">
												<table class="table mar-no bg-light-gray">
													<thead>
														<tr>
															<th class="text-center">Gender</th>
															<th>Unique User</th>
															<th>Percentage</th>
															<th class="text-center">Changes</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td class="text-center text-azure"><i class="fa fa-male"></i></td>
															<td>18 to 25 year old</td>
															<td class="center">25%</td>
															<td class="text-center"><i class="fa fa-caret-up text-success fa-2x"></i></td>
														</tr>
														<tr>
															<td class="text-center text-azure"><i class="fa fa-male"></i></td>
															<td>26 to 35 year old</td>
															<td class="center">35%</td>
															<td class="text-center"><i class="fa fa-caret-down text-danger fa-2x"></i></td>
														</tr>
														<tr>
															<td class="text-center text-azure"><i class="fa fa-male"></i></td>
															<td>36 to 45 year old</td>
															<td class="center">45%</td>
															<td class="text-center"><i class="fa fa-caret-up text-success fa-2x"></i></td>
														</tr>
														<tr>
															<td class="text-center text-azure"><i class="fa fa-male"></i></td>
															<td>46 to 55 year old</td>
															<td class="center">40%</td>
															<td class="text-center"><i class="fa fa-caret-up text-success fa-2x"></i></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<!--===================================================--> 
								<!--End Default Accordion--> 
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--===================================================-->
			<!--End page content-->
		</div>
		<!--===================================================-->
		<!--END CONTENT CONTAINER-->
		<?php include "leftmenu.php"; ?>
		<!--ASIDE-->
		<!--===================================================-->
		<aside id="aside-container">
			<div id="aside">
				<div class="nano">
					<div class="nano-content">
						<!--Nav tabs-->
						<!--================================-->
						<ul class="nav nav-tabs nav-justified">
							<li class="active">
								<a href="#demo-asd-tab-1" data-toggle="tab"> <i class="fa fa-comments"></i> </a>
							</li>
							<li>
								<a href="#demo-asd-tab-3" data-toggle="tab"> <i class="fa fa-wrench"></i> </a>
							</li>
						</ul>
						<!--================================-->
						<!--End nav tabs-->
						<!-- Tabs Content Start-->
						<!--================================-->
						<div class="tab-content">
							<!--First tab (Contact list)-->
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<div class="tab-pane fade in active" id="demo-asd-tab-1">
								<h4 class="pad-hor text-thin"> Online Members (7) </h4>
								<div class="list-group bg-trans">
									<div class="list-group-item">
										<div class="pull-left avatar mar-rgt">
										<img src="img/av1.png" alt="" class="img-sm">
										<i class="on bottom text-light"></i>
										</div>
										<div class="inline-block">
											<div class="text-small">John Knight</div>
											<small class="text-mute">Available</small>
										</div>
									</div>
									<div class="list-group-item">
										<div class="pull-left avatar mar-rgt">
										<img src="img/av2.png" alt="" class="img-sm">
										<i class="on bottom text-light"></i>
										</div>
										<div class="inline-block pad-ver-5">
											<div class="text-small">Jose Knight</div>
											<small class="text-mute">Available</small>
										</div>
									</div>
									<div class="list-group-item">
										<div class="pull-left avatar mar-rgt">
										<img src="img/av3.png" alt="" class="img-sm">
										<i class="on bottom text-light"></i>
										</div>
										<div class="inline-block">
											<div class="text-small">Roy Banks</div>
											<small class="text-mute">Available</small>
										</div>
									</div>
									<div class="list-group-item">
										<div class="pull-left avatar mar-rgt">
										<img src="img/av7.png" alt="" class="img-sm">
										<i class="on bottom text-light"></i>
										</div>
										<div class="inline-block">
											<div class="text-small">Steven Jordan</div>
											<small class="text-mute">Available</small>
										</div>
									</div>
									<div class="list-group-item">
										<div class="pull-left avatar mar-rgt">
										<img src="img/av4.png" alt="" class="img-sm">
										<i class="on bottom text-light"></i>
										</div>
										<div class="inline-block">
											<div class="text-small">Scott Owens</div>
											<small class="text-mute">Available</small>
										</div>
									</div>
									<div class="list-group-item">
										<div class="pull-left avatar mar-rgt">
										<img src="img/av5.png" alt="" class="img-sm">
										<i class="on bottom text-light"></i>
										</div>
										<div class="inline-block">
											<div class="text-small">Melissa Hunt</div>
											<small class="text-mute">Available</small>
										</div>
									</div>
								</div>
								<hr>
								<h4 class="pad-hor text-thin"> Busy Members (4) </h4>
								<div class="list-group bg-trans">
									<div class="list-group-item">
										<div class="pull-left avatar mar-rgt">
										<img src="img/av1.png" alt="" class="img-sm">
										<i class="busy bottom text-light"></i>
										</div>
										<div class="inline-block">
											<div class="text-small">John Knight</div>
											<small class="text-mute">Available</small>
										</div>
									</div>
									<div class="list-group-item">
										<div class="pull-left avatar mar-rgt">
										<img src="img/av2.png" alt="" class="img-sm">
										<i class="busy bottom text-light"></i>
										</div>
										<div class="inline-block">
											<div class="text-small">Jose Knight</div>
											<small class="text-mute">Available</small>
										</div>
									</div>
									<div class="list-group-item">
										<div class="pull-left avatar mar-rgt">
										<img src="img/av3.png" alt="" class="img-sm">
										<i class="busy bottom text-light"></i>
										</div>
										<div class="inline-block">
											<div class="text-small">Roy Banks</div>
											<small class="text-mute">Available</small>
										</div>
									</div>
									<div class="list-group-item">
										<div class="pull-left avatar mar-rgt">
										<img src="img/av7.png" alt="" class="img-sm">
										<i class="busy bottom text-light"></i>
										</div>
										<div class="inline-block">
											<div class="text-small">Steven Jordan</div>
											<small class="text-mute">Available</small>
										</div>
									</div>
								</div>
								<hr>
								<h4 class="pad-hor text-thin"> Offline Members (4) </h4>
								<div class="list-group bg-trans">
									<div class="list-group-item">
										<div class="pull-left avatar mar-rgt">
										<img src="img/av1.png" alt="" class="img-sm">
										<i class="off bottom text-light"></i>
										</div>
										<div class="inline-block pad-ver-5">
											<div class="text-small">John Knight</div>
											<small class="text-mute">Available</small>
										</div>
									</div>
									<div class="list-group-item">
										<div class="pull-left avatar mar-rgt">
										<img src="img/av2.png" alt="" class="img-sm">
										<i class="off bottom text-light"></i>
										</div>
										<div class="inline-block pad-ver-5">
											<div class="text-small">Jose Knight</div>
											<small class="text-mute">Available</small>
										</div>
									</div>
									<div class="list-group-item">
										<div class="pull-left avatar mar-rgt">
										<img src="img/av3.png" alt="" class="img-sm">
										<i class="off bottom text-light"></i>
										</div>
										<div class="inline-block pad-ver-5">
											<div class="text-small">Roy Banks</div>
											<small class="text-mute">Available</small>
										</div>
									</div>
									<div class="list-group-item">
										<div class="pull-left avatar mar-rgt">
										<img src="img/av7.png" alt="" class="img-sm">
										<i class="off bottom text-light"></i>
										</div>
										<div class="inline-block">
											<div class="text-small">Steven Jordan</div>
											<small class="text-mute">Available</small>
										</div>
									</div>
								</div>
							</div>
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<!--End first tab (Contact list)-->
							<!--Second tab (Settings)-->
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<div class="tab-pane fade" id="demo-asd-tab-3">
								<ul class="list-group bg-trans">
									<li class="list-header">
										<h4 class="text-thin">Account Settings</h4>
									</li>
									<li class="list-group-item">
										<div class="pull-right">
											<input class="demo-switch" type="checkbox" checked>
										</div>
										<p>Show my personal status</p>
										<small class="text-muted">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</small> 
									</li>
									<li class="list-group-item">
										<div class="pull-right">
											<input class="demo-switch" type="checkbox" checked>
										</div>
										<p>Show offline contact</p>
										<small class="text-muted">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</small> 
									</li>
									<li class="list-group-item">
										<div class="pull-right">
											<input class="demo-switch" type="checkbox">
										</div>
										<p>Invisible mode </p>
										<small class="text-muted">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</small> 
									</li>
								</ul>
								<hr>
								<ul class="list-group bg-trans">
									<li class="list-header">
										<h4 class="text-thin">Public Settings</h4>
									</li>
									<li class="list-group-item">
										<div class="pull-right">
											<input class="demo-switch" type="checkbox" checked>
										</div>
										Online status 
									</li>
									<li class="list-group-item">
										<div class="pull-right">
											<input class="demo-switch" type="checkbox">
										</div>
										Show offline contact 
									</li>
									<li class="list-group-item">
										<div class="pull-right">
											<input class="demo-switch" type="checkbox" checked>
										</div>
										Show my device icon 
									</li>
								</ul>
								<hr>
								<h4 class="pad-hor text-thin">Task Progress</h4>
								<div class="pad-all">
									<p>Upgrade Progress</p>
									<div class="progress progress-sm">
										<div class="progress-bar progress-bar-success" style="width: 15%;"><span class="sr-only">15%</span></div>
									</div>
									<small class="text-muted">15% Completed</small> 
								</div>
								<div class="pad-hor">
									<p>Database</p>
									<div class="progress progress-sm">
										<div class="progress-bar progress-bar-danger" style="width: 75%;"><span class="sr-only">75%</span></div>
									</div>
									<small class="text-muted">17/23 Database</small> 
								</div>
							</div>
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<!--Second tab (Settings)-->
						</div>
						<!-- Tabs Content End -->
						<!--================================-->
					</div>
				</div>
			</div>
		</aside>
		<!--===================================================-->
		<!--END ASIDE-->
	</div>
<?php include "footer.php"; ?>