<?
include "header.php";
include "profile_header.php";
$mp=$db->get_all("select * from membership_plans order by id limit 4");

if(isset($plan)) {
	$plan=trim(addslashes($plan));
	if($plan>=1) {
	    $_SESSION['plid']=$plan;
		echo "<script>location.href='payment-page';</script>";
		header("Location: payment-page"); exit;
	}
	else {
		echo "<script>location.href='membership-plan';</script>";
		header("Location: membership-plan"); exit;
	}
}
else if($_SESSION['noavail']) {
	echo "<script>swal('Oops..', 'Your post availability has been expired. Purchase a membership plan to continue posting properties!', 'error');</script>";
	unset($_SESSION['noavail']);
}
?>

<style>
.db-bk-color-one {
    background-color: #FFF;
}

.db-bk-color-two {
    background-color: #FFF;
}

.db-bk-color-three {
    background-color: #FFF;
}

.db-bk-color-six {
    background-color: #FFF;
}
.db-padding-btm {
    padding-bottom: 50px;
}
.db-button-color-square {
    color: #FFF;
    background-color:rgb(79, 185, 72);
    border: none;
    border-radius: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
}

    .db-button-color-square:hover {
        color: #fff;
        background-color: rgba(0, 0, 0, 0.50);
        border: none;
    }


.db-pricing-eleven {
    margin-bottom: 30px;
    margin-top: 50px;
    text-align: center;
    box-shadow: 0 0 5px rgba(0, 0, 0, .5);
    color: #141313;
    line-height: 30px;
	border-top: 5px solid#4fb948;
	border-bottom: 5px solid#4fb948;
}

    .db-pricing-eleven ul {
        list-style: none;
        margin: 0;
        text-align: center;
        padding-left: 0px;
    }

        .db-pricing-eleven ul li {
            padding-top: 20px;
            padding-bottom: 20px;
            cursor: pointer;
        }

            .db-pricing-eleven ul li i {
                margin-right: 5px;
            }


    .db-pricing-eleven .price {
        background-color: rgba(0, 0, 0, 0.5);
        padding: 40px 20px 20px 20px;
        font-size: 60px;
        font-weight: 900;
        color: #FFFFFF;
    }

        .db-pricing-eleven .price small {
            color: #B8B8B8;
            display: block;
            font-size: 12px;
            margin-top: 22px;
        }

    .db-pricing-eleven .type {
        background-color: #e3e3e3;
        padding: 10px 20px;
        font-weight: 900;
        text-transform: uppercase;
        font-size: 20px;
    }

    .db-pricing-eleven .pricing-footer {
        padding: 20px;
    }



.db-pricing-eleven.popular {
    margin-top: 10px;
    border-top: 3px solid#cc191c;
	border-bottom: 3px solid#cc191c;
}

    .db-pricing-eleven.popular .price {
        padding-top: 80px;
    }
</style>

<div class="container">
   <div class="row text-center">
		<div class="col-md-12">
		   <p style="padding-top:30px;"><span style="font-size:18px;" class="blackhead"> Select Your </span>  <span  style="font-size:18px;" class=" greenhead"> Membership Plan </span></p>
		</div>
    </div>        
           
  <div class="row db-padding-btm db-attached">
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="db-wrapper">
                    <div class="db-pricing-eleven db-bk-color-one <?php if($mp[0]['value']==$row['mplan']) echo "popular"; ?>">
                        
                        <div class="type">
                            <?php echo $mp[0]['name']; ?>
							<br>
							<sup><?php echo $PSCurncy ?></sup><?php echo $mp[0]['cost']; ?>
                        </div>
                        <ul>

                            <li><i class="fa fa-home" aria-hidden="true"></i> <?php echo $mp[0]['canpost']; ?> properties </li>
                            <li><i class="fa fa-star-o" aria-hidden="true"></i> <?php echo $mp[0]['canfeature']; ?> feature properties </li>
							<li><i class=""></i></li>
                        </ul>
                        <div class="pricing-footer">
							<?
							if($mp[0]['value']==$row['mplan']) {
								echo '<span class="btn db-button-color-square btn-lg" disabled>CURRENT PLAN</span>';
							}
							else {
								echo '<span class="btn db-button-color-square btn-lg" disabled>DISABLED</span>';
							}
							?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="db-wrapper">
                <div class="db-pricing-eleven db-bk-color-two <?php if($mp[1]['value']==$row['mplan']) echo "popular"; ?>">
                    <div class="type">
                        <?php echo $mp[1]['name']; ?>
						<br>
						<sup><?php echo $PSCurncy ?></sup><?php echo $mp[1]['cost']; ?>
                    </div>
                    <ul>

                        <li><i class="fa fa-home" aria-hidden="true"></i> <?php echo $mp[1]['canpost']; ?> properties </li>
                        <li><i class="fa fa-star-o" aria-hidden="true"></i> <?php echo $mp[1]['canfeature']; ?> feature properties </li>
                        <li><i class="fa fa-calendar" aria-hidden="true"></i> Validity: <?php echo $mp[1]['validity']; ?> days </li>
                    </ul>
                    <div class="pricing-footer">
						<?
						if($mp[1]['value']==$row['mplan']) {
							echo '<span style="background-color:#cc191c; color:#FFF;" class="btn db-button-color-square btn-lg" disabled>CURRENT PLAN</span>';
						}
						else {
							echo '<a href="membership-plan?plan='.$mp[1]["value"].'" class="btn db-button-color-square btn-lg">UPGRADE</a>';
						}
						?>
                    </div>
                </div>
                      </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="db-wrapper">
                <div class="db-pricing-eleven db-bk-color-three <?php if($mp[2]['value']==$row['mplan']) echo "popular"; ?>">
                    <div class="type">
                        <?php echo $mp[2]['name']; ?>
						<br>
						<sup><?php echo $PSCurncy ?></sup><?php echo $mp[2]['cost']; ?>
                    </div>
                    <ul>

                        <li><i class="fa fa-home" aria-hidden="true"></i> <?php echo $mp[2]['canpost']; ?> properties </li>
                        <li><i class="fa fa-star-o" aria-hidden="true"></i> <?php echo $mp[2]['canfeature']; ?> feature properties </li>
                        <li><i class="fa fa-calendar" aria-hidden="true"></i> Validity: <?php echo $mp[2]['validity']; ?> days </li>
                    </ul>
                    <div class="pricing-footer">
						<?
						if($mp[2]['value']==$row['mplan']) {
							echo '<span style="background-color:#cc191c; color:#FFF;" class="btn db-button-color-square btn-lg" disabled>CURRENT PLAN</span>';
						}
						else {
							echo '<a href="membership-plan?plan='.$mp[2]["value"].'" class="btn db-button-color-square btn-lg">UPGRADE</a>';
						}
						?>
                    </div>
                </div>
                      </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  <div class="db-wrapper">
                <div class="db-pricing-eleven db-bk-color-six  <?php if($mp[3]['value']==$row['mplan']) echo "popular"; ?>">
                    <div class="type">
                        <?php echo $mp[3]['name']; ?>
						<br>
						<sup><?php echo $PSCurncy ?></sup><?php echo $mp[3]['cost']; ?>
                    </div>
                    <ul>

                        <li><i class="fa fa-home" aria-hidden="true"></i> <?php echo $mp[3]['canpost']; ?> properties </li>
                        <li><i class="fa fa-star-o" aria-hidden="true"></i> <?php echo $mp[3]['canfeature']; ?> feature properties </li>
                        <li><i class="fa fa-calendar" aria-hidden="true"></i> Validity: <?php echo $mp[3]['validity']; ?> days </li>
                    </ul>
                    <div class="pricing-footer">
						<?
						if($mp[3]['value']==$row['mplan']) {
							echo '<span style="background-color:#cc191c; color:#FFF;" class="btn db-button-color-square btn-lg" disabled>CURRENT PLAN</span>';
						}
						else {
							echo '<a href="membership-plan?plan='.$mp[3]["value"].'" class="btn db-button-color-square btn-lg">UPGRADE</a>';
						}
						?>
                    </div>
                </div>
                      </div>
            </div>
        </div>
        

    </div>
    </div>

<?php include "footer.php"; ?>