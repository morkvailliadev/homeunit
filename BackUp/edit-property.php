<?php
include "header.php";
include "profile_header.php";
include "mapapi.php";

$prop_id=trim(addslashes($prop));
$usr=$_SESSION['usr'];
$prop=$db->singleasso("select * from listings where randuniq='$prop_id'");

if($prop_id=='') {
   echo "<script>location.href='$siteurl/manage-your-list';</script>";
   header("Location: $siteurl/manage-your-list"); exit;
}
else if($prop['id']=='') {
	echo "<script>location.href='$siteurl/manage-your-list';</script>";
	header("Location: $siteurl/manage-your-list"); exit;
}
else if($usr!=$prop['email']) {
    echo "<script>location.href='$siteurl/manage-your-list';</script>";
	header("Location: $siteurl/manage-your-list");  exit;
}
?>

<div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20">
        <span class="blackhead pdl10">Dashboard</span>
    </div><!--col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20-->
    
         <?php include "profile_left.php"; ?>
         <div class="col-md-9 col-sm-12 col-xs-12 mt20">
             <div class="col-md-12 col-sm-12 col-xs-12 profile-brdr-2">
                 <div class="pdt10">
                 <div class="row col-md-10 col-sm-6 col-xs-12 property-dash-head">Edit Your Property<br><hr></div>
                 </div><!--class="pdt10"-->             
                 <div class=" col-md-12 col-sm-12 col-xs-12 table-responsive">
<?
if(isset($ed_prop)) {
	$title=trim(addslashes($title));
	$title=preg_replace("/[^A-Za-z0-9 ]/", "", $title);
	$cat=trim(addslashes($cat));
	$pfor=trim(addslashes($pfor));
	$loc=trim(addslashes($loc));
	$addr=trim(addslashes($addr));
	$eprice=trim(addslashes($eprice));
	$bprice=trim(addslashes($bprice));
	$carea=trim(addslashes($carea));
	$parea=trim(addslashes($parea));
	$poss=trim(addslashes($poss));
	$bed=trim(addslashes($bed));
	$bath=trim(addslashes($bath));
	$hall=trim(addslashes($hall));
	$furnish=trim(addslashes($furnish));
	$floor=trim(addslashes($floor));
	$descrip=trim(addslashes($descrip));
	$cperson=trim(addslashes($cperson));
	$cno=trim(addslashes($cno));
	$tcall=trim(addslashes($tcall));
	
	if(empty($title) || empty($addr) || empty($loc) || empty($eprice) || empty($bprice) || empty($descrip) || empty($carea) || empty($parea) || empty($cperson) || empty($cno)) {
			echo "<script>swal('Oops..', 'Fill all the required fields and also check whether the details are correct!', 'error');</script>";
	}
	else {
			$slug = str_replace(" ", "-", $title);
			$slug = strtolower($slug);
			$set = "slug='$slug',";
			$set .= "prop_title='$title',";
			$set .= "location='$loc',";
			$set .= "address='$addr',";
			$set .= "exp_price='$eprice',";
			$set .= "booking_price='$bprice',";
			$set .= "description='$descrip',";
			$set .= "bedroom='$bed',";
			$set .= "bathroom='$bath',";
			$set .= "hall='$hall',";
			$set .= "category='$cat',";
			$set .= "prop_for='$pfor',";
			$set .= "covered_area='$carea',";
			$set .= "plot_area='$parea',";
			$set .= "poss_sts='$poss',";
			$set .= "furnished='$furnish',";
			$set .= "floors='$floor',";
			$set .= "con_person='$cperson',";
			$set .= "con_num='$cno',";
			$set .= "call_time='$tcall'";
			$db->insertrec("update listings set $set where randuniq='$prop_id' and email='".$_SESSION['usr']."'");
			$_SESSION['prupd']=true;
			echo "<script>location.href='$siteurl/manage-your-list';</script>";
			header("Location: $siteurl/manage-your-list"); exit;
	}
}
?>		 
            <form id="p_ad" action="<?php echo $siteurl; ?>/edit-property/<?php echo $prop_id; ?>/<?php echo $prop['slug']; ?>" method="post">
                     
            </div>
                           
            <div class="col-md-12 col-sm-12 col-xs-12">
             <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Title<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12 mt20">
                            <label class="col-md-8  col-sm-12 col-xs-12">                    
                                   <input type="text" name="title" class="post-add-form-control" value="<?php echo $prop['prop_title']; ?>" required="required">
                            </label>
                      </label>
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Category<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12 mt20">
                                 <label class="col-md-8 col-sm-12 col-xs-12">
                                       <select name="cat" class="post-add-selectbox ">
										<?php
										$PS_Categ=$db->get_all("select filtername,cat_name from category where cat_status='1'");
										foreach($PS_Categ as $PSCateg):
										$catt=$PSCateg['cat_name'];
										$fcat=$PSCateg['filtername'];
										if(isset($prop['category'])==$fcat) $ch="selected";
										else $ch="";
										echo '<option value="'.$fcat.'" '.$ch.'>'.$catt.'</option>';
										endforeach;
										?>
                                       </select>
                               </label>
                     </label>
                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Property For<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12 mt20">
                                 <label class="col-md-8 col-sm-12 col-xs-12">
                                       <select name="pfor" class="post-add-selectbox ">
                                                <option value="rent" <?php if($prop['prop_for']=='rent') echo "selected"; ?>>Rent</option>
                                                <option value="sale" <?php if($prop['prop_for']=='sale') echo "selected"; ?>>Sale</option>
                                                <option value="lease" <?php if($prop['prop_for']=='lease') echo "selected"; ?>>Lease</option>
                                       </select>
                               </label>
                     </label>
                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">
                     City<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12">
                           <label class="col-md-8 col-sm-12 col-xs-12 mt20"><input id="autocomplete" onFocus="geolocate()" type="text" name="loc" class=" post-add-form-control" autocomplete="off" value="<?php echo $prop['location']; ?>"></label>
                     </label>
					 
					 <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">
                     Address<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12">
                           <label class="col-md-8 col-sm-12 col-xs-12 mt20"><textarea style="resize: vertical;" name="addr" class=" post-add-form-control"><?php echo $prop['address']; ?></textarea></label>
                     </label>

                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">
                     Property Price<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12">
                           <label class="col-md-8 col-sm-6 col-xs-12 mt20"><input type="text" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" name="eprice" class=" post-add-form-control" placeholder="$" value="<?php echo $prop['exp_price']; ?>"></label>
                     </label>
                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">
                     Booking/Token<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12">
                           <label class="col-md-8 col-sm-6 col-xs-12 mt20"><input type="text" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" name="bprice" class=" post-add-form-control" placeholder="$" value="<?php echo $prop['booking_price']; ?>"></label>
                     </label>
                     
                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">
                       Covered Area<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12">
                           <label class="col-md-8 col-sm-6 col-xs-12 mt20"><input type="text" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" name="carea" class=" post-add-form-control" placeholder="In sq ft" value="<?php echo $prop['covered_area']; ?>"></label>
                     </label>
                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">
                       Plot Area<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12">
                           <label class="col-md-8 col-sm-6 col-xs-12 mt20"><input type="text" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" name="parea" class=" post-add-form-control" placeholder="In sq ft" value="<?php echo $prop['plot_area']; ?>"></label>
                     </label>

              </div><!--col-md-12 col-sm-12 col-xs-12-->
          
              <div class="col-md-12 col-sm-12 col-xs-12">
                     <br><hr><br><label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Possession Status<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9 col-sm-6 col-xs-12">
                     <label class="col-md-12 row  col-sm-12 col-xs-12 mt20">
                          <label class="col-md-6  col-sm-6 col-xs-12">
                               <input type="radio" name="poss" value="1" <?php if($prop['poss_sts']==1) echo "checked"; ?>><span class="post-add-box-font">&nbsp;&nbsp; Under Construction</span>
                            </label>
                           <label class="col-md-6  col-sm-6 col-xs-12">
                               <input type="radio" name="poss" value="2" <?php if($prop['poss_sts']==2) echo "checked"; ?>> <span class="post-add-box-font">&nbsp;&nbsp; Ready to Move</span>
                           </label>
                                      
                     </label>

                     </label><!--col-md-8-->
                     
                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Rooms<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9 col-sm-6 col-xs-12">
                               <label class="col-md-4  col-sm-12 col-xs-12 mt20">
                                <span class="post-add-box-font">Bedroom</span>
                                       <select name="bed" class="post-add-selectbox">
                                                <option value="1" <?php if($prop['bedroom']==1) echo "selected"; ?>>1</option>
                                                <option value="2" <?php if($prop['bedroom']==2) echo "selected"; ?>>2</option>
                                                <option value="3" <?php if($prop['bedroom']==3) echo "selected"; ?>>3</option>
                                                <option value="4" <?php if($prop['bedroom']==4) echo "selected"; ?>>4</option>
                                                <option value="5" <?php if($prop['bedroom']==5) echo "selected"; ?>>5</option>
                                                <option value="6" <?php if($prop['bedroom']==6) echo "selected"; ?>>6</option>
                                                <option value="7" <?php if($prop['bedroom']==7) echo "selected"; ?>>7</option>
                                                <option value="8" <?php if($prop['bedroom']==8) echo "selected"; ?>>8</option>
                                                <option value="9" <?php if($prop['bedroom']==9) echo "selected"; ?>>9</option>
                                                <option value="10" <?php if($prop['bedroom']==10) echo "selected"; ?>>10</option>
                                       </select>
                                               
                               </label>
                               <label class="col-md-4  col-sm-12 col-xs-12 mt20">
                                <span class=" post-add-box-font">Bathroom</span>
                                       <select name="bath" class="post-add-selectbox ">
                                                <option value="1" <?php if($prop['bathroom']==1) echo "selected"; ?>>1</option>
                                                <option value="2" <?php if($prop['bathroom']==2) echo "selected"; ?>>2</option>
                                                <option value="3" <?php if($prop['bathroom']==3) echo "selected"; ?>>3</option>
                                                <option value="4" <?php if($prop['bathroom']==4) echo "selected"; ?>>4</option>
                                                <option value="5" <?php if($prop['bathroom']==5) echo "selected"; ?>>5</option>
                                                <option value="6" <?php if($prop['bathroom']==6) echo "selected"; ?>>6</option>
                                                <option value="7" <?php if($prop['bathroom']==7) echo "selected"; ?>>7</option>
                                                <option value="8" <?php if($prop['bathroom']==8) echo "selected"; ?>>8</option>
                                                <option value="9" <?php if($prop['bathroom']==9) echo "selected"; ?>>9</option>
                                                <option value="10" <?php if($prop['bathroom']==10) echo "selected"; ?>>10</option>
                                       </select>
                                               
                               </label>
                     </label><!--col-md-12-->
                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Number Of Hall<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9 col-sm-6 col-xs-12">
                               <label class="col-md-8  col-sm-12 col-xs-12 mt20">
                                       <select name="hall" class="post-add-selectbox ">
                                                <option value="1" <?php if($prop['hall']==1) echo "selected"; ?>>1</option>
                                                <option value="2" <?php if($prop['hall']==2) echo "selected"; ?>>2</option>
                                                <option value="3" <?php if($prop['hall']==3) echo "selected"; ?>>3</option>
                                                <option value="4" <?php if($prop['hall']==4) echo "selected"; ?>>4</option>
                                                <option value="5" <?php if($prop['hall']==5) echo "selected"; ?>>5</option>
                                                <option value="6" <?php if($prop['hall']==6) echo "selected"; ?>>6</option>
                                                <option value="7" <?php if($prop['hall']==7) echo "selected"; ?>>7</option>
                                                <option value="8" <?php if($prop['hall']==8) echo "selected"; ?>>8</option>
                                                <option value="9" <?php if($prop['hall']==9) echo "selected"; ?>>9</option>
                                                <option value="10" <?php if($prop['hall']==10) echo "selected"; ?>>10</option>
                                       </select>
                                               
                               </label>
                     </label><!--col-md-12-->
                     
                     
                  <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Furnished Status<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9 col-sm-6 col-xs-12">
                               <label class="col-md-8  col-sm-12 col-xs-12 mt20">
                                       <select name="furnish" class="post-add-selectbox ">
                                                <option value="1" <?php if($prop['furnished']==1) echo "selected"; ?>>Furnished</option>
                                                <option value="2" <?php if($prop['furnished']==2) echo "selected"; ?>>Unfurnished</option>
                                                <option value="3" <?php if($prop['furnished']==3) echo "selected"; ?>>Partially Furnished</option>
                                       </select>
                                               
                               </label>
                     </label><!--col-md-12-->
                     
                   <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Total Floors<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9 col-sm-6 col-xs-12">
                               <label class="col-md-8  col-sm-12 col-xs-12 mt20">
                                       <select name="floor" class="post-add-selectbox ">
                                                <option value="1" <?php if($prop['floors']==1) echo "selected"; ?>>1</option>
                                                <option value="2" <?php if($prop['floors']==2) echo "selected"; ?>>2</option>
                                                <option value="3" <?php if($prop['floors']==3) echo "selected"; ?>>3</option>
                                                <option value="4" <?php if($prop['floors']==4) echo "selected"; ?>>4</option>
                                                <option value="5" <?php if($prop['floors']==5) echo "selected"; ?>>5</option>
                                                <option value="6" <?php if($prop['floors']==6) echo "selected"; ?>>6</option>
                                                <option value="7" <?php if($prop['floors']==7) echo "selected"; ?>>7</option>
                                                <option value="8" <?php if($prop['floors']==8) echo "selected"; ?>>8</option>
                                                <option value="9" <?php if($prop['floors']==9) echo "selected"; ?>>9</option>
                                                <option value="10" <?php if($prop['floors']==10) echo "selected"; ?>>10</option>
                                       </select>
                                               
                               </label>
                     </label><!--col-md-12-->
                     

                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Ad Summary<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9 col-sm-6 col-xs-12">
                             <label class="col-md-8 col-sm-12 col-xs-12"> 
                                   <textarea style="resize:vertical;" class="post-add-form-control mt10" name="descrip" rows="5" cols="5"><?php echo $prop['description']; ?></textarea>
                             </label>
                     </label>


              </div><!--col-md-12 col-sm-12 col-xs-12-->    
                              
               
               <div class="col-md-12 col-sm-12 col-xs-12">
                     <br><hr><br><label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Contact Person<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12 mt20">
                            <label class="col-md-8  col-sm-12 col-xs-12">                    
                                   <input type="text" name="cperson" class=" post-add-form-control" placeholder="Name" value="<?php echo $prop['con_person']; ?>">
                            </label>
                      </label>
                      <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Contact  No<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12 mt20">
                            <label class="col-md-8  col-sm-12 col-xs-12">                    
                                   <input type="text" name="cno" maxlength="10" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" class=" post-add-form-control" placeholder="Number" value="<?php echo $prop['con_num']; ?>">
                            </label>
                      </label>
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Best time to call</label>
                     <label class="col-md-9  col-sm-6 col-xs-12 mt20">
                            <label class="col-md-12  col-sm-12 col-xs-12"> 
                                     <label class="col-md-4  col-sm-7 col-xs-12">                   
                                         <input type="radio" name="tcall" value="anytime" <?php if($prop['call_time']=='anytime') echo "checked"; ?>> <span class="post-add-box-font">Anytime</span>
                                     </label>
                                     <label class="col-md-4  col-sm-7 col-xs-12">
                                         <input type="radio" name="tcall" value="morning" <?php if($prop['call_time']=='morning') echo "checked"; ?>><span class="post-add-box-font">Morning</span>
                                      </label>
                                     <label class="col-md-4  col-sm-7 col-xs-12">
                                         <input type="radio" name="tcall" value="evening" <?php if($prop['call_time']=='evening') echo "checked"; ?>> <span class="post-add-box-font">Evening</span>
                                     </label>
                            </label>
                      </label>
                      <div class="col-md-9  col-sm-6 col-xs-12 mt20 col-md-offset-3">
                               <input type="submit" name="ed_prop" value="Submit" class="btn-post-add"><br><br>
                      </div>
               
	</form>
							
                 </div><!--table-responsive-->
             </div><!--col-md-12 col-sm-12 col-xs-12 profile-brdr-->
         </div><!--col-md-9 col-sm-12 col-xs-12-->
    </div><!--row-->
</div> <!--container-->

<?php include "footer.php"; ?>