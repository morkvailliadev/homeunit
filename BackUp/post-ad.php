<?php 
include "header.php";
include "profile_header.php";
include "mapapi.php";
?>

<div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20">
        <span class="blackhead pdl10">Dashboard 
		<?
		if(isset($_SESSION['pmt'])) { 
			echo "&bull; <span class='err'>Fill all the required fields!</span>";
			unset($_SESSION['pmt']);
		}
		else if(isset($_SESSION['ptper'])) {
			echo "&bull; <span class='err'>Unrecognised image type. Try uploading an image.</span>";
			unset($_SESSION['ptper']);
		}
		else if(isset($_SESSION['szerr'])) {
			echo "&bull; <span class='err'>Image must be atleast 800*550 pixels.</span>";
			unset($_SESSION['szerr']);
		}
		else if(isset($_SESSION['ab5'])) {
			echo '&bull; <span class="err">You can`t upload more than 5 images.</span>';
			unset($_SESSION['ab5']);
		}
		?>
		</span>
    </div><!--col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20-->
    
         <?php include "profile_left.php"; ?>       
         <div class="col-md-9 col-sm-12 col-xs-12 mt20">
             <div class="col-md-12 col-sm-12 col-xs-12 profile-brdr-2">
                 <div class="pdt10">
                 <div class="row col-md-10 col-sm-6 col-xs-12 property-dash-head">Post Ad<br><hr></div>
                 </div><!--class="pdt10"-->
<?
if(isset($p_ad)) {
		$role=trim(addslashes($role));
		$mobile=trim(addslashes($mobile));
		$title=trim(addslashes($title));
		$title=preg_replace("/[^A-Za-z0-9 ]/", "", $title);
		$cat=trim(addslashes($cat));
		$pfor=trim(addslashes($pfor));
		$loc=trim(addslashes($loc));
		$eprice=trim(addslashes($eprice));
		$bprice=trim(addslashes($bprice));
		$carea=trim(addslashes($carea));
		$parea=trim(addslashes($parea));
		$mplan=trim(addslashes($mplan));
		$sfor=trim(addslashes($sfor));
		$poss=trim(addslashes($poss));
		$bed=trim(addslashes($bed));
		$bath=trim(addslashes($bath));
		$hall=trim(addslashes($hall));
		$furnish=trim(addslashes($furnish));
		$floor=trim(addslashes($floor));
		$descrip=trim(addslashes($descrip));
		$cperson=trim(addslashes($cperson));
		$cno=trim(addslashes($cno));
		$tcall=trim(addslashes($tcall));
		$posted=time();
		
		if($_FILES['file']['tmp_name'] != "" && $_FILES['file']['tmp_name'] != "null") {
		    if(count($_FILES['file']['name'])>5) {
					/* $_SESSION['ab5']=true;
					echo "<script>location.href='post-ad';</script>";
					header("Location: post-ad"); exit; */
					echo "<script>swal('Oops..', 'You can`t upload more than 5 images.', 'error');</script>";
            }
			else {
				$files=array();
				$fdata=$_FILES['file'];
				if(is_array($fdata['name'])){
					for($i=0;$i<count($fdata['name']);++$i){
						$files[]=array(
						'name'    =>$fdata['name'][$i],
						'type'  => $fdata['type'][$i],
						'tmp_name'=>$fdata['tmp_name'][$i],
						'error' => $fdata['error'][$i], 
						'size'  => $fdata['size'][$i] );
					}
				}
				else $files[]=$fdata;
				$err=0;
				foreach($files as $file) {
					$fl=$file['tmp_name'];
					$fln=$file['name'];
					$ext=end(explode(".", $fln));
					$ext=strtolower($ext);
					list($width,$height,$type,$attr)=getimagesize($fl);
					if(($ext != "jpg") && ($ext != "jpeg") && ($ext != "gif") && ($ext != "png")) {
						/* $_SESSION['ptper']=true;
						echo "<script>location.href='post-ad';</script>";
						header("Location: post-ad"); exit; */
						$err++;
					}
					else if($width<800 || $height<500) {
						/* $_SESSION['szerr']=true;
						echo "<script>location.href='post-ad';</script>";
						header("Location: post-ad"); exit; */
						$err++;
					}
				}
				if($err>0) {
					echo "<script>swal('Oops', 'File must be an image and Image must be atleast 800*550 pixels', 'error');</script>";
				}
				else {
					if(isset($featured)) { $ftr=1; }
					else { $ftr=0; }
					$countryname=$db->Extract_Single("select country_name from mlm_country where country_id='$countryid'");
					$statename=$db->Extract_Single("select state_name from mlm_state where state_id='$stateid'");
					$location=$db->Extract_Single("select city_name from mlm_city where city_id='$locationid'");
					$randuniq = $com_obj->randuniq();
					$slug = str_replace(" ", "-", $title);
					$slug = strtolower($slug);
					$set = "randuniq='$randuniq',";
					$set .= "slug='$slug',";
					$set .= "email='".$_SESSION['usr']."',";
					//$set .= "role='$role',";
					//$set .= "mobile='$mobile',";
					$set .= "prop_title='$title',";
					$set .= "exp_price='$eprice',";
					$set .= "booking_price='$bprice',";
					$set .= "description='$descrip',";
					$set .= "bedroom='$bed',";
					$set .= "bathroom='$bath',";
					$set .= "hall='$hall',";
					$set .= "category='$cat',";
					$set .= "prop_for='$pfor',";
					$set .= "countryid='$countryid',";
					$set .= "stateid='$stateid',";
					$set .= "locationid='$locationid',";
					$set .= "area_id='$area_id',";
					$set .= "countryname='$countryname',";
					$set .= "statename='$statename',";
					$set .= "location='$location',";
					$set .= "address='$address',";
					$set .= "covered_area='$carea',";
					$set .= "plot_area='$parea',";
					//$set .= "suitable_for='$sfor',";
					$set .= "poss_sts='$poss',";
					$set .= "furnished='$furnish',";
					$set .= "floors='$floor',";
					$set .= "con_person='$cperson',";
					$set .= "con_num='$cno',";
					$set .= "call_time='$tcall',";
					$set .= "posted_at='$posted',";
					$set .= "post_sts=0,";
					$set .= "featured=0,";
					$set .= "mplan='$mplan'";
					//$set .= "pmdone=0";
					$que="insert into listings set $set";
					$id=$db->insertid($que);
					$_SESSION['pid']=$id;
					
					foreach($files as $file) {
						$fl=$file['tmp_name'];
						$fln=$file['name'];
						$ext=end(explode(".", $fln));
						$ext=strtolower($ext);
						$tmpname=basename($fl);
						$pat=array();
						$pat[0]='/php/';
						$pat[1]='/.tmp/';
						$repl=array();
						$repl[0]='';
						$repl[1]='';
						$oname=strtolower(preg_replace($pat, $repl, $tmpname));
						$newname='prop'.$_SESSION['pid'].'_'.$oname.'.'.$ext;
						$org='images/prop/org/'.$newname;
						$rz='images/prop/'.$newname;
						move_uploaded_file($fl, $org);

						$resizeObj = new resize($org);
						$resizeObj -> resizeImage(800, 500, 'exact');
						$resizeObj -> saveImage($rz, 100);
						
						$set="pid='".$_SESSION['pid']."',";
						$set.="image='$newname'";
						$db->insertrec("insert into listing_images set $set");
					}
					$mp=$db->singlerec("select * from listings where id='".$_SESSION['pid']."'");
					if($mp['mplan']==0) {
						unset($_SESSION['pid']);
						$_SESSION['posted']=true;
						echo "<script>location.href='manage-your-list';</script>";
						header("Location: manage-your-list"); exit;
					}
					else {
						echo "<script>location.href='payment-page';</script>";
						header("Location: payment-page"); exit;
					}
				}
			}
		}
}
$statelist = "<option value=''>- - Select- -</option>";
$citylist = "<option value=''>- - Select- -</option>";
$arealist = "<option value=''>- - Select- -</option>";

$GetRecord = $db->singlerec("select * from listings where id='$id'");
@extract($GetRecord);	

$DropDownQry = "select state_id as stateid,state_name from mlm_state where state_id='$stateid'";
$statelist .= $drop->get_dropdown($db,$DropDownQry,$stateid);

$DropDownQry = "select city_id as locationid,city_name from mlm_city where city_id='$locationid'";
$citylist .= $drop->get_dropdown($db,$DropDownQry,$locationid);

$DropDownQry = "select area_id,area_name from mlm_area where area_id='$area_id'";
$arealist .= $drop->get_dropdown($db,$DropDownQry,$area_id);

$countrylist = "<option value=''>- - Select- -</option>";
$DropDownQry = "select country_id as countryid,country_name from mlm_country order by country_id asc";
$countrylist .= $drop->get_dropdown($db,$DropDownQry,$countryid);
?>               				 
<form class="contain" id="p_ad" action="post-ad" method="post" enctype="multipart/form-data">
    <div class="col-md-9 col-sm-12 col-xs-12 mt30 row">
          <div class="col-md-12 col-sm-12 col-xs-12"><span class="blackhead">User Info</span></div><!--col-md-12-->
               <div class="col-md-12 col-sm-12 col-xs-12">              
                 
                  <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">I Am a<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12 mt20">
                            <label class="col-md-12  col-sm-12 col-xs-12"> 
                                     <label class="col-md-4  col-sm-12 col-xs-12">                   
                                         <input type="radio" name="role" value="1" <?php if(isset($role)==1 || isset($role)=='') echo "checked"; ?>> <span class="post-add-box-font">Owner</span>
                                     </label>
                                     <label class="col-md-4  col-sm-12 col-xs-12">
                                         <input type="radio" name="role" value="2" <?php if(isset($role)==2) echo "checked"; ?>><span class="post-add-box-font">Agent</span>
                                      </label>
                                     <label class="col-md-4  col-sm-12 col-xs-12">
                                         <input type="radio" name="role" value="3" <?php if(isset($role)==3) echo "checked"; ?>> <span class="post-add-box-font">Builder</span>
                                     </label>
                            </label>
                      </label>
                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">
                               Mobile Number<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12">
                           <label class="col-md-8 col-sm-6 col-xs-12 mt20"><input type="text" name="mobile" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class=" post-add-form-control" value="<?php echo @$mobile; ?>"></label>
                     </label>
                     <div style="border-bottom:2px solid#rgb(79, 185, 72) !important;"></div>
               </div>
                          
               
          <div class="col-md-12 col-sm-12 col-xs-12"><span class="blackhead"><br>Property Details</span></div><!--col-md-12-->
              <div class="col-md-12 col-sm-12 col-xs-12">
              <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Property Title<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12 mt20">
                            <label class="col-md-12  col-sm-12 col-xs-12">                    
                                   <input type="text" name="title" class=" post-add-form-control" value="<?php echo @$title; ?>">
                            </label>
                      </label>
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Category<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12 mt20">
                                 <label class="col-md-8 col-sm-12 col-xs-12">
                                       <select name="cat" class="post-add-selectbox ">
										<?php
										$PS_Categ=$db->get_all("select filtername,cat_name from category where cat_status='1'");
										foreach($PS_Categ as $PSCateg):
										$catt=$PSCateg['cat_name'];
										$fcat=$PSCateg['filtername'];
										if(isset($cat)==$fcat) $ch="selected";
										else $ch="";
										echo '<option value="'.$fcat.'" '.$ch.'>'.$catt.'</option>';
										endforeach;
										?>
                                       </select>
                               </label>
                     </label>
                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Property For<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12 mt20">
                                 <label class="col-md-8 col-sm-12 col-xs-12">
                                       <select name="pfor" class="post-add-selectbox ">
                                                <option value="rent" <?php if(isset($pfor)=='rent') echo "selected"; ?>>Rent</option>
                                                <option value="sale" <?php if(isset($pfor)=='sale') echo "selected"; ?>>Sale</option>
                                                <option value="lease" <?php if(isset($pfor)=='lease') echo "selected"; ?>>Lease</option>
                                       </select>
                               </label>
                     </label>
					 
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">
                     Country<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12">
                           <label class="col-md-8 col-sm-12 col-xs-12 mt20"><select onchange="countryfn();" name="countryid" id="countryid" class=" post-add-form-control" required><?php echo $countrylist;?></select></label>
                     </label>
					 
					 <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">
                     State<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12">
                           <label class="col-md-8 col-sm-12 col-xs-12 mt20"><select onchange="statefn();" name="stateid" id="stateid" class=" post-add-form-control" required><?php echo $statelist;?></select></label>
                     </label>
					 
					 <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">
                     City<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12">
                           <label class="col-md-8 col-sm-12 col-xs-12 mt20"><select onchange="cityfn();" name="locationid" id="locationid" class=" post-add-form-control" required><?php echo $citylist;?></select></label>
                     </label>
					 
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">
                     Area<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12">
                           <label class="col-md-8 col-sm-12 col-xs-12 mt20"><select name="area_id" id="area_id" class=" post-add-form-control"><?php echo $arealist;?></select></label>
                     </label>
					 
					 <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">
                     Address<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12">
                           <label class="col-md-8 col-sm-12 col-xs-12 mt20"><input type="text" name="address" value="<?php echo $address; ?>" class=" post-add-form-control" required></label>
                     </label>
                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">
                     Expected Price<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12">
                           <label class="col-md-5 col-sm-6 col-xs-12 mt20"><input type="number" name="eprice" class=" post-add-form-control" placeholder="$" value="<?php echo @$eprice; ?>"></label>
                     </label>
                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">
                     Booking/Token<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12">
                           <label class="col-md-5 col-sm-6 col-xs-12 mt20"><input type="number" name="bprice" class=" post-add-form-control" placeholder="$" value="<?php echo @$bprice; ?>"></label>
                     </label>
                     
                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">
                       Covered Area<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12">
                           <label class="col-md-5 col-sm-6 col-xs-12 mt20"><input type="number" name="carea" class=" post-add-form-control" placeholder="In sq ft" value="<?php echo @$carea; ?>"></label>
                     </label>
                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">
                       Plot Area<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12">
                           <label class="col-md-5 col-sm-6 col-xs-12 mt20"><input type="number" name="parea" class=" post-add-form-control" placeholder="In sq ft" value="<?php echo @$parea; ?>"></label>
                     </label>
					 
					 <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Plan Type<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12 mt20">
                                 <label class="col-md-8 col-sm-12 col-xs-12">
                                       <select name="mplan" class="post-add-selectbox ">
										<?
										$plan=$db->get_all("select * from membership_plans order by id");
										$i=0;
										foreach($plan as $pl):
										?>
											<option value="<?php echo $i; ?>"><?php echo $pl['name']; ?></option>
										<?
										$i++;
										endforeach;
										?>
                                       </select>
                               </label>
                     </label>
					 					  
					  <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Upload Image<span class="post-add-box-form-font-redfont">*</span></label>
					  <label class="col-md-9  col-sm-6 col-xs-12 mt20">
                            <label class="col-md-12  col-sm-12 col-xs-12">
								<label class="col-md-4  col-sm-12 col-xs-12">
								<input type="file" name="file[]" required="required" multiple="multiple">
							    </label>
							</label>
                      </label>
					  
              </div><!--col-md-12 col-sm-12 col-xs-12-->
          <div class="col-md-12 col-sm-12 col-xs-12 mt30"><span class="blackhead">Property Features</span></div><!--col-md-12-->
          
              <div class="col-md-12 col-sm-12 col-xs-12">
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Suitable for<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9 col-sm-6 col-xs-12">
                     <label class="col-md-6  col-sm-12 col-xs-12 mt20">
                             <select name="sfor" class="post-add-selectbox">
                                      <option value="1" <?php if(isset($sfor)==1) echo "selected"; ?>>1 Member</option>
                                      <option value="2" <?php if(isset($sfor)==2) echo "selected"; ?>>2 Members</option>
                                      <option value="3" <?php if(isset($sfor)==3) echo "selected"; ?>>3 Members</option>
                                      <option value="4" <?php if(isset($sfor)==4) echo "selected"; ?>>4 Members</option>
                                      <option value="5" <?php if(isset($sfor)==5) echo "selected"; ?>>5 Members</option>
                                      <option value="6" <?php if(isset($sfor)==6) echo "selected"; ?>>6 Mebers</option>
                                      <option value="7" <?php if(isset($sfor)==7) echo "selected"; ?>>7 Members</option>
                                      <option value="8" <?php if(isset($sfor)==8) echo "selected"; ?>>8 Members</option>
                                      <option value="9" <?php if(isset($sfor)==9) echo "selected"; ?>>9 Members</option>
                                      <option value="10" <?php if(isset($sfor)==10) echo "selected"; ?>>10 Members</option>
                             </select>
                                      
                     </label>
                      <label class="col-md-6  col-sm-12 col-xs-12 mt20"></label>
                     </label><!--col-md-8-->
                     
                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Possession Status<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9 col-sm-6 col-xs-12">
                     <label class="col-md-12  col-sm-12 col-xs-12 mt20">
                          <label class="col-md-6  col-sm-6 col-xs-12">
                               <input type="radio" name="poss" value="1" <?php if(isset($poss)==1 || isset($poss)=='') echo "checked"; ?>><span class="post-add-box-font">&nbsp;&nbsp; Under Construction</span>
                            </label>
                           <label class="col-md-6  col-sm-6 col-xs-12">
                               <input type="radio" name="poss" value="2" <?php if(isset($poss)==2) echo "checked"; ?>> <span class="post-add-box-font">&nbsp;&nbsp; Ready to Move</span>
                           </label>
                                      
                     </label>

                     </label><!--col-md-8-->
                     
                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Rooms<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9 col-sm-6 col-xs-12">
                               <label class="col-md-4  col-sm-12 col-xs-12 mt20">
                                <span class="post-add-box-font">Bedroom</span>
                                       <select name="bed" class="post-add-selectbox">
                                                <option value="1" <?php if(isset($bed)==1) echo "selected"; ?>>1</option>
                                                <option value="2" <?php if(isset($bed)==2) echo "selected"; ?>>2</option>
                                                <option value="3" <?php if(isset($bed)==3) echo "selected"; ?>>3</option>
                                                <option value="4" <?php if(isset($bed)==4) echo "selected"; ?>>4</option>
                                                <option value="5" <?php if(isset($bed)==5) echo "selected"; ?>>5</option>
                                                <option value="6" <?php if(isset($bed)==6) echo "selected"; ?>>6</option>
                                                <option value="7" <?php if(isset($bed)==7) echo "selected"; ?>>7</option>
                                                <option value="8" <?php if(isset($bed)==8) echo "selected"; ?>>8</option>
                                                <option value="9" <?php if(isset($bed)==9) echo "selected"; ?>>9</option>
                                                <option value="10" <?php if(isset($bed)==10) echo "selected"; ?>>10</option>
                                       </select>
                                               
                               </label>
                               <label class="col-md-4  col-sm-12 col-xs-12 mt20">
                                <span class=" post-add-box-font">Bathroom</span>
                                       <select name="bath" class="post-add-selectbox ">
                                                <option value="1" <?php if(isset($bath)==1) echo "selected"; ?>>1</option>
                                                <option value="2" <?php if(isset($bath)==2) echo "selected"; ?>>2</option>
                                                <option value="3" <?php if(isset($bath)==3) echo "selected"; ?>>3</option>
                                                <option value="4" <?php if(isset($bath)==4) echo "selected"; ?>>4</option>
                                                <option value="5" <?php if(isset($bath)==5) echo "selected"; ?>>5</option>
                                                <option value="6" <?php if(isset($bath)==6) echo "selected"; ?>>6</option>
                                                <option value="7" <?php if(isset($bath)==7) echo "selected"; ?>>7</option>
                                                <option value="8" <?php if(isset($bath)==8) echo "selected"; ?>>8</option>
                                                <option value="9" <?php if(isset($bath)==9) echo "selected"; ?>>9</option>
                                                <option value="10" <?php if(isset($bath)==10) echo "selected"; ?>>10</option>
                                       </select>
                                               
                               </label>
                     </label><!--col-md-12-->
                     
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Number Of Hall<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9 col-sm-6 col-xs-12">
                               <label class="col-md-6  col-sm-12 col-xs-12 mt20">
                                       <select name="hall" class="post-add-selectbox ">
                                                <option value="1" <?php if(isset($hall)==1) echo "selected"; ?>>1</option>
                                                <option value="2" <?php if(isset($hall)==2) echo "selected"; ?>>2</option>
                                                <option value="3" <?php if(isset($hall)==3) echo "selected"; ?>>3</option>
                                                <option value="4" <?php if(isset($hall)==4) echo "selected"; ?>>4</option>
                                                <option value="5" <?php if(isset($hall)==5) echo "selected"; ?>>5</option>
                                                <option value="6" <?php if(isset($hall)==6) echo "selected"; ?>>6</option>
                                                <option value="7" <?php if(isset($hall)==7) echo "selected"; ?>>7</option>
                                                <option value="8" <?php if(isset($hall)==8) echo "selected"; ?>>8</option>
                                                <option value="9" <?php if(isset($hall)==9) echo "selected"; ?>>9</option>
                                                <option value="10" <?php if(isset($hall)==10) echo "selected"; ?>>10</option>
                                       </select>
                                               
                               </label>
                     </label><!--col-md-12-->
                     
                     
                  <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Furnished Status<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9 col-sm-6 col-xs-12">
                               <label class="col-md-6  col-sm-12 col-xs-12 mt20">
                                       <select name="furnish" class="post-add-selectbox ">
                                                <option value="1" <?php if(isset($furnish)==1) echo "selected"; ?>>Furnished</option>
                                                <option value="2" <?php if(isset($furnish)==2) echo "selected"; ?>>Unfurnished</option>
                                                <option value="3" <?php if(isset($furnish)==3) echo "selected"; ?>>Partially Furnished</option>
                                       </select>
                                               
                               </label>
                     </label><!--col-md-12-->
                     
                   <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Total Floors<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9 col-sm-6 col-xs-12">
                               <label class="col-md-6  col-sm-12 col-xs-12 mt20">
                                       <select name="floor" class="post-add-selectbox ">
                                                <option value="1" <?php if(isset($floor)==1) echo "selected"; ?>>1</option>
                                                <option value="2" <?php if(isset($floor)==2) echo "selected"; ?>>2</option>
                                                <option value="3" <?php if(isset($floor)==3) echo "selected"; ?>>3</option>
                                                <option value="4" <?php if(isset($floor)==4) echo "selected"; ?>>4</option>
                                                <option value="5" <?php if(isset($floor)==5) echo "selected"; ?>>5</option>
                                                <option value="6" <?php if(isset($floor)==6) echo "selected"; ?>>6</option>
                                                <option value="7" <?php if(isset($floor)==7) echo "selected"; ?>>7</option>
                                                <option value="8" <?php if(isset($floor)==8) echo "selected"; ?>>8</option>
                                                <option value="9" <?php if(isset($floor)==9) echo "selected"; ?>>9</option>
                                                <option value="10" <?php if(isset($floor)==10) echo "selected"; ?>>10</option>
                                       </select>
                                               
                               </label>
                     </label><!--col-md-12-->
                     

                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Ad Summary<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9 col-sm-6 col-xs-12">
                             <label class="col-md-12 col-sm-12 col-xs-12"> 
                                   <textarea style="resize:vertical;" class="post-add-form-control mt10" name="descrip" rows="5" cols="5"><?php echo @$descrip; ?></textarea>
                             </label>
                     </label>


              </div><!--col-md-12 col-sm-12 col-xs-12-->    
                              
               
               <div class="col-md-12 col-sm-12 col-xs-12 mt30"><span class="blackhead">Enter Contact Details</span></div><!--col-md-12-->
               <div class="col-md-12 col-sm-12 col-xs-12">
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Contact Person<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12 mt20">
                            <label class="col-md-12  col-sm-12 col-xs-12">                    
                                   <input type="text" name="cperson" class=" post-add-form-control" value="<?php echo @$cperson; ?>">
                            </label>
                      </label>
                      <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Contact  No<span class="post-add-box-form-font-redfont">*</span></label>
                     <label class="col-md-9  col-sm-6 col-xs-12 mt20">
                            <label class="col-md-12  col-sm-12 col-xs-12">                    
                                   <input type="text" name="cno" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class=" post-add-form-control" value="<?php echo @$cno; ?>">
                            </label>
                      </label>
                     <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">Best time to call</label>
                     <label class="col-md-9  col-sm-6 col-xs-12 mt20">
                            <label class="col-md-12  col-sm-12 col-xs-12"> 
                                     <label class="col-md-4  col-sm-7 col-xs-12">                   
                                         <input type="radio" name="tcall" value="anytime" <?php if(isset($tcall)=='anytime' || isset($tcall)=='') echo "checked"; ?>> <span class="post-add-box-font">Anytime</span>
                                     </label>
                                     <label class="col-md-4  col-sm-7 col-xs-12">
                                         <input type="radio" name="tcall" value="morning" <?php if(isset($tcall)=='morning') echo "checked"; ?>><span class="post-add-box-font">Morning</span>
                                      </label>
                                     <label class="col-md-4  col-sm-7 col-xs-12">
                                         <input type="radio" name="tcall" value="evening" <?php if(isset($tcall)=='evening') echo "checked"; ?>> <span class="post-add-box-font">Evening</span>
                                     </label>
                            </label>
                      </label>
                      <label class="col-md-3 col-sm-6 col-xs-12 post-add-box-form-font mt20">&nbsp;</label>
                     <label class="col-md-9  col-sm-6 col-xs-12 mt20">
                                               
				   <input type="radio" checked="checked"> <span style="font-weight: normal;font-style: italic;">I am the owner/I have the authority to post this property. I agree not to provide incorrect property information or state a discriminatory preference.
In case, the information does not comply with CompanyName terms, CompanyName.com has the right to edit/remove the property from their site.</span>
                           
                      </label>
                      <div class="col-md-9  col-sm-6 col-xs-12 mt20 col-md-offset-3">
                               <input type="submit" name="p_ad" value="Submit" class="btn-post-add">
                      </div>
                      
               </div><!--col-md-12 col-sm-12 col-xs-12-->
               
    </div><!--col-md-9-->
</form>				 
				  

             </div><!--col-md-12 col-sm-12 col-xs-12 profile-brdr-->
         </div><!--col-md-9 col-sm-12 col-xs-12-->
    </div><!--row-->
</div> <!--container-->

<?php include "footer.php"; ?>
<script>
function countryfn(){
	var getval = document.getElementById("countryid").value;
	if (window.XMLHttpRequest)
	  {
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
		document.getElementById("stateid").innerHTML = xmlhttp.responseText;
		document.getElementById("locationid").innerHTML = "<option>--select--</option>";
		}
	  }
	var url = "ajaxstate.php?getval="+getval;
	xmlhttp.open("GET",url,true);
	xmlhttp.send();
} 
function statefn(){
	var getval = document.getElementById("stateid").value;
	if (window.XMLHttpRequest)
	  {
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
		document.getElementById("locationid").innerHTML = xmlhttp.responseText;
		}
	  }
	var url = "ajaxcity.php?getval="+getval;
	xmlhttp.open("GET",url,true);
	xmlhttp.send();
} 
function cityfn(){
	var getval = document.getElementById("locationid").value;
	if (window.XMLHttpRequest)
	  {
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
		document.getElementById("area_id").innerHTML = xmlhttp.responseText;
		}
	  }
	var url = "ajaxarea.php?getval="+getval;
	xmlhttp.open("GET",url,true);
	xmlhttp.send();
} 
</script>