<?php 
include 'header.php';
include 'profile_header.php';

if(!$_SESSION['plid']) {
	echo "<script>location.href='membership-plan';</script>";
	header("Location: membership-plan"); exit;
}
$mplan=$_SESSION['plid'];
$pl=$db->singlerec("select * from membership_plans where value='$mplan'");
$prc=$pl['cost'];
$itm="Membership - ".$pl['name'];
$time=time();
$val=$pl['validity'];
$exp=strtotime("+".$val." days");

$formaction = "https://www.sandbox.paypal.com/cgi-bin/webscr"; // Sandbox
//$formaction = "https://www.paypal.com/cgi-bin/webscr"; // Live
$paypalmail="rajesh@i-netsolution.com";
$item_name = $itm;
$price = $prc;
$item_number = 01;
$return_url = "http://".$file_with_path;
$cancel_return = "http://".$file_with_path."?canceled";

$item_st=isset($item_st)?$item_st:'';
$txn_id=isset($txn_id)?$txn_id:'';
$txn_amt=isset($txn_amt)?$txn_amt:'';
$txn_crncy=isset($txn_crncy)?$txn_crncy:'';
$txn_date=isset($txn_date)?$txn_date:'';

$item_st=@$_REQUEST['payment_status'];
$txn_id=@$_REQUEST['txn_id'];
$txn_amt=@$_REQUEST['mc_gross'];
$txn_crncy=@$_REQUEST['mc_currency'];
$txn_date=@$_REQUEST['payment_date'];

if($txn_id !="" && $txn_amt !="" && $txn_crncy !=""){
	$set="user_mail='".$_SESSION['usr']."',";
	$set.="txn_id='$txn_id',";
	$set.="amount='$txn_amt',";
	$set.="txn_crncy='$txn_crncy',";
	$set.="txn_date='$txn_date'";
	$db->insertrec("insert into payments set $set");
	$rset="mplan=$mplan,";
	$rset.="pmdone=1,";
	$rset.="canpost='".$pl['canpost']."',";
	$rset.="canpost_avail='".$pl['canpost']."',";
	$rset.="canfeature='".$pl['canfeature']."',";
	$rset.="canfeature_avail='".$pl['canfeature']."',";
	$rset.="pn_purchase='$time',";
	$rset.="pn_expire='$exp'";
	$db->insertrec("update register set $rset where email='".$_SESSION['usr']."'");
	$_SESSION['pmdone']=true;
	unset($_SESSION['plid']);
	echo "<script>location.href='plan-detail'</script>";
	header("Location: plan-detail"); exit;
}
?>

<div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20">
        <span class="blackhead pdl10">Dashboard</span>		
    </div><!--col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20-->
         
		 <?php include "profile_left.php"; ?>
         <div class="col-md-9 col-sm-12 col-xs-12 mt20">
             <div class="col-md-12 col-sm-12 col-xs-12 profile-brdr-2">
                 <div class="pdt10">
                 <div class="row col-md-10 col-sm-6 col-xs-12 property-dash-head">Payment Page</div><br><hr>
                 </div><!--class="pdt10"--><center>
				<?
				if(isset($_REQUEST['canceled'])) {
						echo "<span class='err'>You just canceled the transaction. Your ad may not be visible in the site without completing the transaction.</span><br><br><a href='payment-page?paypal'>Go Back to Payment Page</a><br><br>";
				}
				else if(isset($_REQUEST['manual'])) {
						$set="mplan='".$_SESSION['plid']."',";
						$set.="pmethod='manual'";
						$db->insertrec("update register set $set where email='".$_SESSION['usr']."'");
						unset($_SESSION['plid']);
						$_SESSION['manual']=true;
						echo "<script>location.href='bank-account';</script>";
						header("Location: bank-account"); exit;
				}
				else if(isset($_REQUEST['paypal'])) {
						$db->insertrec("update register set pmethod='paypal' where email='".$_SESSION['usr']."'");
				?>
					Please click the below button to complete your payment.<br><br>
					<form action="<?echo $formaction;?>" method="post">
							<input name="currency_code" type="hidden" value="USD">
							<input name="shipping" type="hidden" value="0.00">
							<input name="tax" type="hidden" value="0.00">
							<input id="ret_url" type="hidden" value="<?php echo $return_url;?>">
							<input name="return" type="hidden" value="<?php echo $return_url;?>">
							<input name="cancel_return" type="hidden" value="<?php echo $cancel_return;?>">
							<input name="notify_url" type="hidden" value="<?php echo $return_url;?>">
							<input name="cmd" type="hidden" value="_xclick">
							<input name="business" type="hidden" value="<?php echo $paypalmail;?>">
							<input name="item_name" type="hidden" value="<?php echo $item_name;?>">
							<input name="no_note" type="hidden" value="1">
							<input type="hidden" name="no_shipping" value="1">
							<input name="lc" type="hidden" value="EN">
							<input name="bn" type="hidden" value="PP-BuyNowBF">
							<input name="custom" type="hidden" value="custom data">
							<input type="hidden" name="amount" value="<?php echo $price; ?>">
							<input type="submit" class="submit" name="_mric" value="Complete transaction">
							<br><br>
					</form>
				<?
				}
				else {
					echo 'Thanks for choosing a premium membership plan. Select your payment method to continue transaction.<br><br><br>
				    <select onchange="location=this.options[this.selectedIndex].value;">
					<option value="">Select Payment Method</option>
					<option value="payment-page?manual">Manual</option>
				    <option value="payment-page?paypal">Paypal</option></select><br><br><br>';
			    }
				?>
					
			    </div><!--settings-->
				</div><!--tab-content-->
				<!-- Nav tabs end-->
                    
                 
             </div><!--col-md-12 col-sm-12 col-xs-12 profile-brdr-->
         </div><!--col-md-9 col-sm-12 col-xs-12-->
    </div><!--row-->
</div> <!--container-->

<?php include 'footer.php'; ?>