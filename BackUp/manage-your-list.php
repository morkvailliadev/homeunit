<?php 
include 'header.php';
include "pagination.php";
$perpage=5;
$limit=limitation($perpage);
include 'profile_header.php';
$header_cms = $db->Extract_Single("select content from cms_new where label='page_MP1'");
$header_cms_text = $com_obj->decrypt($header_cms,CMS_INT_KEY);
$header_cms_text = base64_decode($header_cms_text);

?>

<div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20">
        <span class="blackhead pdl10">Dashboard</span>
    </div><!--col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20-->
    
         <?php include "profile_left.php"; ?> 
         <div class="col-md-9 col-sm-12 col-xs-12 mt20">
             <div class="col-md-12 col-sm-12 col-xs-12 profile-brdr-2">
                 <div class="pdt10">
                 <div class="row col-md-10 col-sm-6 col-xs-12 property-dash-head">Manage Properties<br><hr></div>
                 <div class="row col-md-2 col-sm-6 col-xs-12 mb10 pull-right"><a href="post-ad"><input type="button" class="btn btn-view-detail" value="Add Property" /></a><br><br></div>
                 </div><!--class="pdt10"-->
						<?
						$que="select * from listings where email='".$_SESSION['usr']."' order by id desc";
						$result=$db->get_all($que . $limit);
						if(count($result)>=1) {
								echo '<div class=" col-md-12 col-sm-12 col-xs-12 table-responsive">
								<table class="table table-striped brdr">
								<tr class="tabl-head">
								<td style="width:20%">Property</td>
								<td style="width:20%">Title</td>
								<td style="width:20%">Posted Date</td>
								<td style="width:20%">Price</td>
								<td style="width:20%">Action</td>
								</tr>';
								foreach($result as $row) {
								$im=$db->singlerec("select * from listing_images where pid='".$row['id']."' limit 1");
							
									echo eval(' ?>'.$header_cms_text.'<?php ');
							} echo '</table></div>';
							} else echo '<br><br><br><br>Nothing found here!<br><br>';
							?>
					<div class="text-center boxs">
						<div class="row">
							<div class="col-md-12">
								<?php $db->insertrec(getPagingQuery1($que, $perpage, "")); ?>
								<nav class="pagination-wrapper">
								   <?echo $pagingLink = getPagingLink1($que, $perpage, ""); ?>
								</nav>
							</div>
						</div>
					</div>
             </div><!--col-md-12 col-sm-12 col-xs-12 profile-brdr-->
         </div><!--col-md-9 col-sm-12 col-xs-12-->
    </div><!--row-->
</div> <!--container-->

<?
include "footer.php";

if(isset($_SESSION['posted'])) {
	echo "<script>swal('Great!', 'Your property has been posted!', 'success');</script>";
	unset($_SESSION['posted']);
}
else if(isset($_SESSION['prupd'])) {
	echo "<script>swal('Great!', 'Your property details has been updated!', 'success');</script>";
	unset($_SESSION['prupd']);
}
else if(isset($_SESSION['prdel'])) {
	echo "<script>swal('Success!', 'Your property has been deleted!', 'success');</script>";
	unset($_SESSION['prdel']);
}
else if(isset($_SESSION['manual'])) {
	echo "<script>swal('Great!', 'You just selected a manual payment option. You will receive our bank details soon via email.', 'success');</script>";
	unset($_SESSION['manual']);
}
?>