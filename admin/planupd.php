<?php include"header.php"; ?>
<div class="boxed">
	<!--CONTENT CONTAINER-->
	<!--===================================================-->
	<div id="content-container">
		<?php include "header_nav.php"; ?>
		<div class="pageheader">
			<h3><i class="fa fa-home"></i> Membership Plan </h3>
			<div class="breadcrumb-wrapper">
				<span class="label">You are here:</span>
				<ol class="breadcrumb">
					<li> <a href="welcome"> Home </a> </li>
					<li class="active"> Membership Plan </li>
				</ol>
			</div>
		</div>
<?
$upd = isset($upd)?$upd:'';
$id = isSet($id) ? $id : '' ;
$act = isSet($act) ? $act : '' ;
$page = isSet($page) ? $page : '' ;

if($submit) {
    $crcdt = time();
	$name = trim(addslashes($name));
	$cost = trim(addslashes($cost));
	$canpost = trim(addslashes($canpost));
	$canfeature = trim(addslashes($canfeature));
	$validity = trim(addslashes($validity));
	
	$checkStatus = $db->check1column("membership_plans","name",$name);
	if($upd == 2)
		$checkStatus = 0;
		
	if($checkStatus == 0){
		$set  = "name = '$name'";
		$set  .= ",cost = '$cost'";
		$set .= ",canpost = '$canpost'";
		$set .= ",canfeature = '$canfeature'";
		$set .= ",validity = '$validity'";
		$db->insertrec("update membership_plans set $set where id='$idvalue'");
		$act = "upd";
		header("location:plan?page=$pg&act=$act");
		exit;
	}	
	 else {
		 $id = $idvalue;
		$Message = "<font color='red'>$name Already Exit's</font>";
	}
}

$GetRecord = $db->singlerec("select * from membership_plans where id='$id'");
@extract($GetRecord);	
?>
		<!--Page content-->
		<!--===================================================-->
		<div id="page-content">
			<div class="row">
			  <div class="eq-height">
				 <div class="col-sm-6 eq-box-sm">
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Edit Plan </h3>
						</div>
						<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
							<input type="hidden" name="idvalue" value="<?echo $id;?>" />
							<input type="hidden" name="upd" value="<?echo $upd;?>" />							
							<div class="panel-body">
								<table style="padding:25px;">
									<tr>
										<td>Plan name <font color="red">*</font></td>
										<td><input type="text" name="name" id="name" value="<?php echo $name; ?>" class="form-control">
										</td>
									</tr>
									<tr>
										<td>Cost <font color="red">*</font></td>
										<td><input type="text" name="cost" id="cost" value="<?php echo $cost; ?>" class="form-control">
										</td>
									</tr>
									<tr>
										<td>Can Post <font color="red">*</font></td>
										<td><input type="text" name="canpost" id="canpost" value="<?php echo $canpost; ?>" class="form-control">
										</td>
									</tr>
									<tr>
										<td>Can Feature <font color="red">*</font></td>
										<td><input type="text" name="canfeature" id="canfeature" value="<?php echo $canfeature; ?>" class="form-control">
										</td>
									</tr>
									<tr>
										<td>Validity <font color="red">*</font></td>
										<td><input type="text" name="validity" id="validity" value="<?php echo $validity; ?>" class="form-control">
										</td>
									</tr>
								</table>
							</div>
							<div class="panel-footer text-left">
								<div class="col-md-4  text-right"><input class="btn btn-info" type="submit" name="submit" value="Submit"></div>
								<a class="btn btn-info" href="newsletter">Cancel</a>
							</div>
						</form>
						<!--===================================================-->
						<!--End Horizontal Form-->
					</div>
				</div>
			  </div>
			</div>
		</div>
		<!--===================================================-->
		<!--End page content-->
	</div>
	<!--===================================================-->
	<!--END CONTENT CONTAINER-->
	<?php include "leftmenu.php"; ?>
</div>
<?php include "footer.php"; ?>