<?php include "header.php"; ?>
            <!--===================================================-->
            <!--END NAVBAR-->
            <div class="boxed">
                <!--CONTENT CONTAINER-->
                <!--===================================================-->
                <div id="content-container">
                    <?php include "header_nav.php"; ?>
                    <div class="pageheader">
                        <h3><i class="fa fa-home"></i> Membership Plans </h3>
                        <div class="breadcrumb-wrapper">
                            <span class="label">You are here:</span>
                            <ol class="breadcrumb">
                                <li> <a href="welcome"> Home </a> </li>
                                <li class="active"> Membership Plans </li>
                            </ol>
                        </div>
                    </div>
<?
$act = isSet($act) ? $act : '' ; 
$id = isSet($id) ? $id : '' ;
$upd = isSet($upd) ? $upd : '' ;

$GetRecord=$db->get_all("select * from membership_plans order by id");
if(count($GetRecord)==0)
    $Message="No Record found";
$disp = "";
for($i = 0 ; $i < count($GetRecord) ; $i++) {
	$idvalue = $GetRecord[$i]['id'] ;
	$name=$GetRecord[$i]['name'];
	$cost=$GetRecord[$i]['cost'];
	$canpost=$GetRecord[$i]['canpost'];
	$canfeature=$GetRecord[$i]['canfeature'];
	$validity = $GetRecord[$i]['validity'];
	
	$slno = $i + 1 ;
    $disp .="<tr>
				<td>$slno</td>
				<td  align='left'>$name</td>
				<td  align='left'>$$cost</td>
				<td  align='left'>$canpost properties</td>
				<td  align='left'>$canfeature properties</td>
				<td  align='left'>$validity days</td>
				<td width='20%'>
				<div class='btn-group btn-group-xs'>
				<a href='planview?id=$idvalue' title='View User Details' class='btn btn-default' data-toggle='tooltip'>$GT_View</a>
					<a href='planupd?upd=2&id=$idvalue' data-toggle='tooltip' title='Edit' class='btn btn-default' ><i class='fa fa-edit'></i></a>
				</div>
				</td>
			</tr>";
}

if($act == "'del'")
    $Message = "<font color='green'><b>Plan deleted</b></font>" ;
else if($act == "upd")
    $Message = "<font color='green'><b>Plan updated</b></font>" ;
else if($act == "add")
    $Message = "<font color='green'><b>Plan added</b></font>" ;
?>
                    <!--Page content-->
                    <!--===================================================-->
                    <div id="page-content">
                        <!-- Basic Data Tables -->
                        <!--===================================================-->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"><?echo $Message;?></h3>
                            </div>
                            <div class="panel-body">
							    <table id="demo-dt-basic" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
											<th>S.No</th>
											<th>Plan Name</th>
											<th>Cost</th>
											<th>Can Post</th>
											<th>Can Feature</th>
											<th>Validity</th>
											<th class='cntrhid'>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody><?echo $disp; ?></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--===================================================-->
                    <!--End page content-->
                </div>
                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
			<?php include "leftmenu.php"; ?>
            </div>
<?php include "footer.php"; ?>
