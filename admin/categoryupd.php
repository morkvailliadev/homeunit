<?php include"header.php"; ?>
<div class="boxed">
	<!--CONTENT CONTAINER-->
	<!--===================================================-->
	<div id="content-container">
		<?php include "header_nav.php"; ?>
		<div class="pageheader">
			<h3><i class="fa fa-home"></i>Category </h3>
			<div class="breadcrumb-wrapper">
				<span class="label">You are here:</span>
				<ol class="breadcrumb">
					<li> <a href="welcome.php"> Home </a> </li>
					<li class="active"> Category </li>
				</ol>
			</div>
		</div>
		
<?
$upd = isset($upd)?$upd:'';
$id = isSet($id) ? $id : '' ;
$act  = isSet($act) ? $act : '' ;
$page  = isSet($page) ? $page : '' ;
$Message  = isSet($Message) ? $Message : '' ;
$cat_name = isSet($cat_name) ? $cat_name : '' ;
$filtername = isSet($filtername) ? $filtername : '' ;
if($submit) {
    $crcdt = time();
    $cat_name  = trim(addslashes($cat_name));
	$checkStatus = $db->check1column("category","cat_name",$cat_name);
	if($cat_name != ''){
		if($upd == 2)
			$checkStatus = 0;
			
		if($checkStatus == 0){
			$filtername=strtolower($filtername);
			$set ="cat_name = '$cat_name'";
			$set .=",filtername = '$filtername'";
			if($upd == 1){  
				$set .=",cat_status = '1'";
				$db->insertrec("insert into category set $set");
				$act = "add";
			}
			else if($upd == 2){
				$db->insertrec("update category set $set where id='$id'");
				$act = "upd";
			}
			echo "<script>location.href='category.php?&page=$pg&act=$act';</script>";
			header("location:category.php?&page=$pg&act=$act");
			exit;
		}else{
			$upd = $upd ;
			$Message = "<font color='red'>$cat_name Already Exit</font>";
		}
    } 
	else{
		$upd = $upd ;
		$Message = "<font color='red'>Input Fields Marked With * are compulsory</font>";
	}
}
if($upd==1){
	$TextChange = "Add";
}
else if($upd==2){
	$TextChange = "Update";
	$Getmaincat=$db->singlerec("select * from category where id='$id'");
    $cat_name = stripslashes($Getmaincat['cat_name']);
	$filtername = stripslashes($Getmaincat['filtername']);
}


?>
		
			<!--Page content-->
		<!--===================================================-->
		<div id="page-content">
			<div class="row">
			  <div class="eq-height">
				 <div class="col-sm-6 eq-box-sm">
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"><?php echo $TextChange;?> Category</h3>
						</div>
						<form class="form-horizontal" method="post" action="" enctype="multipart/form-data" data-parsley-validate>
							<input type="hidden" name="idvalue" value="<?echo $id;?>" />
							<input type="hidden" name="upd" value="<?echo $upd;?>" />							
							<div class="panel-body">
								<table style="padding:25px;">
									<tr>
										<td>Display Name <font color="red">*</font></td>
										<td><input type="text" name="cat_name" value="<?php echo $cat_name; ?>" class="form-control">
										</td>
									</tr>
									<tr>
										<td>Filter Name <font color="red">*</font></td>
										<td><input type="text" name="filtername" value="<?php echo $filtername; ?>" class="form-control">
										</td>
									</tr>
							    </table>
							</div>
							<div class="panel-footer text-left">
								<div class="col-md-4  text-right"><input class="btn btn-info" type="submit" name="submit" value="Submit"></div>
								<a class="btn btn-info" href="category.php">Cancel</a>
							</div>
						</form>
						<!--===================================================-->
						<!--End Horizontal Form-->
					</div>
				</div>
			  </div>
			</div>
		</div>
		<!--===================================================-->
		<!--End page content-->
	</div>
	<!--===================================================-->
	<!--END CONTENT CONTAINER-->
	<?php include "leftmenu.php"; ?>
</div>
<?php include "footer.php"; ?>
<script src="plugins/parsley/parsley.min.js"></script>
        <!--Jquery Steps [ OPTIONAL ]-->
		