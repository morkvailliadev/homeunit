<?php include "header.php"; ?>
            <!--===================================================-->
            <!--END NAVBAR-->
            <div class="boxed">
                <!--CONTENT CONTAINER-->
                <!--===================================================-->
                <div id="content-container">
                    <?php include "header_nav.php"; ?>
                    <div class="pageheader">
                        <h3><i class="fa fa-home"></i> General Setting </h3>
                        <div class="breadcrumb-wrapper">
                            <span class="label">You are here:</span>
                            <ol class="breadcrumb">
                                <li> <a href="welcome.php"> Home </a> </li>
                                <li class="active"> General Setting </li>
                            </ol>
                        </div>
                    </div>
<?
$upd = isset($upd)?$upd:'';
$id = isSet($id) ? $id : '' ;
$act = isSet($act) ? $act : '' ;
$page = isSet($page) ? $page : '' ;
$Message = isSet($Message) ? $Message : '' ;
$website_title=isSet($website_title)?$website_title:'';
$website_keyword=isSet($website_keyword)?$website_keyword:'';
$website_url=isSet($website_url)?$website_url:'';
$admin_email=isSet($admin_email)?$admin_email:'';
$paypal_email=isSet($paypal_email)?$paypal_email:'';
$facebook=isSet($facebook)?$facebook:'';
$twitter=isSet($twitter)?$twitter:'';
$gplus=isset($gplus)?$gplus:'';
$skype=isset($skype)?$skype:'';
$img = isset($img)?$img : '' ;
$ImgExt = isSet($ImgExt) ? $ImgExt : '' ;
$DisplayDeleteImgLink = isSet($DisplayDeleteImgLink) ? $DisplayDeleteImgLink : '' ;
$contactus = isSet($contactus) ? $contactus : '' ;

if($act ==  "del" && $nimg != "") {
    $RemoveImage = "uploads/general_setting/$nimg";
    @unlink($RemoveImage);
    $db->insertrec("update general_setting set img='noimage.jpg' where id='$id'");
    header("Location:general?upd=2&msg=nimgscs&id=$id") ;
    exit ;
}

if($submit) {
    $crcdt = time();
	$website_title = trim(addslashes($website_title));
	$website_keyword = trim(addslashes($website_keyword));
	$website_url = trim(addslashes($website_url));
	$admin_email = trim(addslashes($admin_email));
	$paypal_email = trim(addslashes($paypal_email));
	$copyrights = trim(addslashes($copyrights));
	$contactus = trim(addslashes($contact_us));
		
	if($_FILES['Img']['tmp_name'] != "" && $_FILES['Img']['tmp_name'] != "null") {
		$fpath = $_FILES['Img']['tmp_name'] ;
		$fname = $_FILES['Img']['name'] ;
		$getext = substr(strrchr($fname, '.'), 1);
		$ImgExt = strtolower($getext); 
	}
	if($ImgExt=="jpg" || $ImgExt == "jpeg" || $ImgExt == "gif" || $ImgExt == "png" || $ImgExt == ''){	
			$set  = "website_title = '$website_title'";
			$set  .= ",website_keyword = '$website_keyword'";
			$set  .= ",website_url = '$website_url'";
			$set  .= ",admin_email = '$admin_email'";
			$set  .= ",admin_mobile = '$admin_mobile'";
			$set  .= ",paypal_email = '$paypal_email'";
			$set  .= ",ipaddr = '$ipaddress'";		
			$set  .= ",chngdt = '$crcdt'";    
			$set  .= ",copyrights = '$copyrights'";
			$set  .= ",userchng = '$usrcre_name'";
			$set  .= ",contactus = '$contactus'";
			$db->insertid("update general_setting set $set where id='1'");

			$sset  = "facebook = '$facebook'";
			$sset  .= ",twitter = '$twitter'";
			$sset  .= ",gplus = '$gplus'";
			$sset  .= ",skype = '$skype'";

			$db->insertrec("update social_links set $sset where id='1'");
			if($_FILES['Img']['tmp_name'] != "" && $_FILES['Img']['tmp_name'] != "null") {
				$fpath = $_FILES['Img']['tmp_name'] ;
				$fname = $_FILES['Img']['name'] ;
				$getext = substr(strrchr($fname, '.'), 1);
				$ext = strtolower($getext);
				$NgImg= "logo".".".$ext;
				$set_img = "img = '$NgImg'" ;
				$des = "uploads/general_setting/$NgImg";
				move_uploaded_file($fpath,$des) ;
				chmod($des,0777);
				$iimg=$db->insertrec("select img from general_setting where id='1'");
					if($iimg!= "noimage.jpg") {
						$RemoveImage = "uploads/general_setting/$nimg";
						@unlink($RemoveImage);
					}
				$db->insertrec("update general_setting set $set_img where id='1'");
			}
			header("location:general?page=$pg&act=$act");
			exit;
		}	
		else{
			$Message = "<font color='red'>kindly upload jpg,gif,png image format only</font>";
		}
}
if($upd == 1)
	$hidimg = "style='display:none;'";
else if($upd == 2)
	$hidimg = "";

$GetRec= $db->singlerec("select facebook,twitter,gplus,skype from social_links where id='1'");
@extract($GetRec);
	
$GetRecord = $db->singlerec("select * from general_setting where id='1'");
@extract($GetRecord);
$website_title = stripslashes($website_title);
$website_keyword = stripslashes($website_keyword);
$website_url = stripslashes($website_url);
$admin_email = stripslashes($admin_email);
$paypal_email = stripslashes($paypal_email);
//code for images 
if($img == "noimage.jpg") {
    $ShowOldImg = "";
	$DisplayDeleteImgLink = '';
    }
else if($img != "") {
    $ShowOldImg = "";
	$DisplayDeleteImgLink = '<a href="general?act=del&nimg='.$img.'&id='.$id.'">Delete</a>';
	}
?>
<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
<script type="text/javascript" src="js/tinymce.js" ></script> 
<!--Page content-->
<!--===================================================-->
<div id="page-content">
<!-- Basic Data Tables -->
<!--===================================================-->
<h3>View General Setting Details</h3></br>
<div class="panel">
<div class="panel-heading">
	</div>
		<div class="panel-body">
			<table>
				<thead>
					<form method="post" action="general" enctype="multipart/form-data" class="form-horizontal">
					<tr>
						<td><label>Website Title <font color="red">*</font></label></td>
						<td><input type="text" class="form-control" name="website_title" value="<?php echo $website_title; ?>" placeholder="Enter Title" required></td>
					</tr>
					<tr>
						<td valign="top"><label valign="top">Website Keywords <font color="red">*</font></label></td>
						<td><textarea name="website_keyword" class="form-control tiny" rows="4" cols="45"><?php echo $website_keyword; ?></textarea></td>
					</tr>
					<tr>
						<td valign="top"><label valign="top">Contact Address <font color="red">*</font></label></td>
						<td><textarea name="contact_us" class="form-control tiny" rows="4" cols="45"><?php echo $contactus; ?></textarea></td>
					</tr>
					
					<tr>
						<td><label>Website Url <font color="red">*</font></label></td>
						<td><input type="text" class="form-control" name="website_url" value="<?php echo $website_url; ?>" placeholder="Enter Url" required></td>
					</tr>
					<tr>
						<td><label>Admin Email<font color="red">*</font></label></td>
						<td><input type="email" class="form-control" name="admin_email" value="<?php echo $admin_email; ?>" placeholder="Enter Email" required></td>
					</tr>
					<tr>
						<td><label>Admin Mobile<font color="red">*</font></label></td>
						<td><input type="text" class="form-control" name="admin_mobile" value="<?php echo $admin_mobile; ?>" placeholder="Enter Mobile" required></td>
					</tr>
					<tr>
						<td><label>Paypal Email<font color="red">*</font></label></td>
						<td><input type="email" class="form-control" name="paypal_email" value="<?php echo $paypal_email; ?>" placeholder="Enter Title" required></td>
					</tr>
					<tr>
						<td><label>Facebook</label></td>
						<td><input type="text" class="form-control" name="facebook" value="<?php echo $facebook; ?>"></td>
					</tr>
					<tr>
						<td><label>Twitter</label></td>
						<td><input type="text" class="form-control" name="twitter" value="<?php echo $twitter; ?>"></td>
					</tr>
					<tr>
						<td><label>Google Plus</label></td>
						<td><input type="text" class="form-control" name="gplus" value="<?php echo $gplus; ?>"></td>
					</tr>
					<!--<tr>
						<td><label>Skype</label></td>
						<td><input type="text" class="form-control" name="skype" value="<?php echo $skype; ?>"></td>
					</tr>-->
					<tr>
						<td><label>copyrights</label></td>
						<td><input type="text" class="form-control" name="copyrights" value="<?php echo $copyrights; ?>"></td>
					</tr>
					<tr>
						<td>Website Logo</td>
						<td><img src="uploads/general_setting/<?php echo $img; ?>" width="120px" height="120px"><br>
						<?php echo $DisplayDeleteImgLink; ?>
						<input name="Img" type="file" class="form-control">
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">
						<input type="submit" name="submit" class="btn btn-primary" value="Save">
						<input type="reset" name="reset" class="btn btn-primary" value="Reset">
						</td>
					</tr>
					</form>
				</thead>
			</table>
		</div>
	</div>
</div>
<!--===================================================-->
<!--End page content-->
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<?php include "leftmenu.php"; ?>
</div>
<?

include "footer.php";
?>