<?php include "header.php"; ?>
            <!--===================================================-->
            <!--END NAVBAR-->
            <div class="boxed">
                <!--CONTENT CONTAINER-->
                <!--===================================================-->
                <div id="content-container">
                    <?php include "header_nav.php"; ?>
                    <div class="pageheader">
                        <h3><i class="fa fa-home"></i> Manage Properties </h3>
                        <div class="breadcrumb-wrapper">
                            <span class="label">You are here:</span>
                            <ol class="breadcrumb">
                                <li> <a href="welcome"> Home </a> </li>
                                <li class="active">Manage Properties </li>
                            </ol>
                        </div>
                    </div>
<?

$id = isSet($id) ? $id : '' ;
$page = isSet($page) ? $page : '' ;
$usercre = isSet($usercre) ? $usercre : '' ;


$GetRecordView = $db->singlerec("select * from listings where id='$id'");
@extract($GetRecordView);
$GetRefereBy = $db->get_all("select * from listings where id='$id'");
@extract($GetRefereBy);

?> <!--Page content-->
                    <!--===================================================-->
                    <div id="page-content">
                        <!-- Basic Data Tables -->
                        <!--===================================================-->
						<h3>View Property Details</h3><a href="prop"  class="btn btn-success">Back</a></br>
                        <div class="panel">
                            <div class="panel-heading">
                            </div>
                            <div class="panel-body">
								<table>
                                    <thead>
                                        <tr>
											<td>Title</td><td width="75">:</td><td><?php echo $prop_title;?></td>
											</tr>
											<tr><td>Email</td><td width="75">:</td><td><?php echo $email;?></td></tr>
											<tr><td>Location</td><td width="75">:</td><td><?php echo $location;?></td></tr>
											<tr><td>Address</td><td width="75">:</td><td><?php echo $address;?></td></tr>
											<tr><td>Category</td><td width="75">:</td><td><?php echo ucwords($category); ?></td></tr>
											<tr><td>Property Price</td><td width="75">:</td><td><?php echo $exp_price;?></td></tr>
											<tr><td>Booking/Token</td><td width="75">:</td><td><?php echo $booking_price;?></td></tr>
											<tr><td>Description</td><td width="75">:</td><td><?php echo $description;?></td></tr>
											<tr><td>Bedroom</td><td width="75">:</td><td><?php echo $bedroom;?></td></tr>
											<tr><td>Bathroom</td><td width="75">:</td><td><?php echo $bathroom;?></td></tr>
											<tr><td>Hall</td><td width="75">:</td><td><?php echo $hall;?></td></tr>
											<tr><td>Property For</td><td width="75">:</td><td><?php echo ucwords($prop_for); ?></td></tr>
											<tr><td>Covered Area</td><td width="75">:</td><td><?php echo $covered_area;?></td></tr>
											<tr><td>Plot Area</td><td width="75">:</td><td><?php echo $plot_area;?></td></tr>
											<tr><td>Floors</td><td width="75">:</td><td><?php echo $floors;?></td></tr>
											<tr><td>Contact Person</td><td width="75">:</td><td><?php echo $con_person;?></td></tr>
											<tr><td>Contact Number</td><td width="75">:</td><td><?php echo $con_num;?></td></tr>
											<tr><td>Call Time</td><td width="75">:</td><td><?php echo $call_time;?></td></tr>
											<tr><td>Posted at</td><td width="75">:</td><td><?php echo date("d/M/Y", $posted_at); ?></td></tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--===================================================-->
                    <!--End page content-->
                </div>
                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
			<?php include "leftmenu.php"; ?>
            </div>
<?
include "footer.php";
?>