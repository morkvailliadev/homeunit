<?php include"header.php"; ?>
<div class="boxed">
	<!--CONTENT CONTAINER-->
	<!--===================================================-->
	<div id="content-container">
		<?php //include "header_nav.php"; ?>
		<div class="pageheader">
			<h3><i class="fa fa-home"></i> Area </h3>
			<div class="breadcrumb-wrapper">
				<span class="label">You are here:</span>
				<ol class="breadcrumb">
					<li> <a href="welcome.php"> Home </a> </li>
					<li class="active"> Area </li>
				</ol>
			</div>
		</div>
<?	
$act = isSet($act) ? $act : '' ; 
$sid = isSet($sid) ? $sid : '' ;
$cid = isSet($cid) ? $cid : '' ;
$ctid = isSet($ctid) ? $ctid : '' ;
$upd = isSet($upd) ? $upd : '' ;
$status = isSet($status) ? $status : '' ;
$Message = isSet($Message) ? $Message : '' ;
$area_name = isSet($area_name) ? $area_name : '' ;
$area_id = isSet($area_id) ? $area_id : '' ;

if($act == "del") {
	$db->insertrec("delete from mlm_area where area_id='$area_id'");
    header("location:area.php?act='del'&area_id=$area_id&cid=$cid&sid=$sid&ctid=$ctid&page=$page");
    exit ;
}
if($status == "1") {
    $aa = $db->insertrec("update mlm_area set area_status='0' where area_id='$area_id'");
    header("location:area.php?upd=2&area_id=$area_id&cid=$cid&sid=$sid&ctid=$ctid&act=sts");
    exit ;
}
else if($status == "0") {
    $db->insertrec("update mlm_area set area_status='1' where area_id='$area_id'");
    header("location:area.php?upd=2&area_id=$area_id&cid=$cid&sid=$sid&ctid=$ctid&act=sts");
    exit ;
}

$GetRecord=$db->get_all("select * from mlm_area where city_id='$ctid' order by area_id desc");
if(count($GetRecord)==0)
    $Message="No Record found";
$disp = "";
for($i = 0 ; $i < count($GetRecord) ; $i++) {
	$idvalue = $GetRecord[$i]['state_id'] ;
    $area_id = $GetRecord[$i]['area_id'] ;
	$area_name=$GetRecord[$i]['area_name'];
	$area_status = $GetRecord[$i]['area_status'] ;
	
	$slno = $i + 1 ;
	if($area_status == '0'){
        $DisplayStatus = $GT_Active;
		$Title = "Active";
		$status_active = "Deactive";
		$EditLink = "<a href='areaupd.php?upd=2&area_id=$area_id&sid=$idvalue&cid=$cid&ctid=$ctid' data-toggle='tooltip' title='Edit' class='btn btn-default' ><i class='fa fa-edit'></i></a>";
	}	
    else if($area_status == '1'){
        $DisplayStatus = $GT_InActive;
		$Title = "Deactive";
		$status_active = "Active";
		$EditLink = "<a class='btn btn-default' ><i class='fa ><font color='red'>--</font></i></a>";
	}
    $disp .="<tr>
				<td>$slno</td>
				<td align='left'>$area_name</td>
				<td width='20%'>
				<div class='btn-group btn-group-xs'>
					<a href='area.php?area_id=$area_id&sid=$idvalue&cid=$cid&ctid=$ctid&status=$area_status' title='$Title' class='btn btn-default' data-toggle='tooltip'>$DisplayStatus</a>
					$EditLink
					<a href='area.php?act=del&area_id=$area_id&sid=$idvalue&cid=$cid&ctid=$ctid' onclick='return confirm('Are you sure to delete?');' class='btn btn-default' title='Delete' data-toggle='tooltip'>$GT_Delete</a>
				</div>
				</td>
			</tr>";
			
}

if($act == "'del'")
    $Message = "<font color='green'><b>Deleted Successfully</b></font>" ;
else if($act == "upd")
    $Message = "<font color='green'><b>Updated Successfully</b></font>" ;
else if($act == "add")
    $Message = "<font color='green'><b>Added Successfully</b></font>" ;
else if($act == "sts")
    $Message = "<font color='green'><b>Status Changed Successfully</b></font>" ;
?>  
<!--Page content-->
                    <!--===================================================-->
                    <div id="page-content">
                        <!-- Basic Data Tables -->
                        <!--===================================================-->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"><?echo $Message;?></h3>
                            </div>
                            <div class="panel-body">
							<div class="col-sm-1 text-left"><a class="btn btn-info" href="city.php?sid=<?echo $sid;?>&cid=<?echo $cid;?>">Back</a></div>
								<div class="col-sm-12 text-right"><a class="btn btn-info" href="areaupd.php?upd=1&cid=<?php echo $cid; ?>&sid=<?php echo $sid; ?>&ctid=<?php echo $ctid; ?>">Add New</a></div>
							    <table id="demo-dt-basic" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
											<th></th>
											<th>Name</th>
											<th class='cntrhid'>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody><?echo $disp; ?></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--===================================================-->
                    <!--End page content-->
                </div>
                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
			<?php include "leftmenu.php"; ?>
            </div>
<?php include "footer.php"; ?>