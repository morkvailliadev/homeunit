<?
include"header.php";
include "../mapapi.php";
?>
<div class="boxed">
	<!--CONTENT CONTAINER-->
	<!--===================================================-->
	<div id="content-container">
		<?php //include "header_nav.php"; ?>
		<div class="pageheader">
			<h3><i class="fa fa-home"></i>Manage Properties </h3>
			<div class="breadcrumb-wrapper">
				<span class="label">You are here:</span>
				<ol class="breadcrumb">
					<li> <a href="welcome"> Home </a> </li>
					<li class="active"> Manage Properties </li>
				</ol>
			</div>
		</div>
<?
$upd = isset($upd)?$upd:'';
$id = isSet($id) ? $id : '' ;
$act = isSet($act) ? $act : '' ;
$page = isSet($page) ? $page : '' ;
$Message = isSet($Message) ? $Message : '' ;
$name = isSet($name) ? $name : '' ;
$img = isSet($img) ? $img : '' ;
$ImgExt = isSet($ImgExt) ? $ImgExt : '' ;
$UsrImg = isSet($UsrImg) ? $UsrImg : '' ;

if($submit) {
    $crcdt = time();
	$email = trim(addslashes($email));
	$prop_title = trim(addslashes($prop_title));
	$category = trim(addslashes($category));
	$prop_for = trim(addslashes($prop_for));
	$location = trim(addslashes($location));
	$address = trim(addslashes($address));
	$exp_price = trim(addslashes($exp_price));
	$booking_price= trim(addslashes($booking_price));
	$covered_area = trim(addslashes($covered_area));
	$plot_area = trim(addslashes($plot_area));
	$poss_sts = trim(addslashes($poss_sts));
	$bedroom = trim(addslashes($bedroom));
	$bathroom = trim(addslashes($bathroom));
	$hall = trim(addslashes($hall));
	$furnished = trim(addslashes($furnished));
	$floors = trim(addslashes($floors));
	$description = trim(addslashes($description));
	$con_person = trim(addslashes($con_person));
	$con_num = trim(addslashes($con_num));
	$call_time = trim(addslashes($call_time));
	
	if(isset($featured)) { $ftr=1; }
	else { $ftr=0; }
	$countryname=$db->Extract_Single("select country_name from mlm_country where country_id='$countryid'");
	$statename=$db->Extract_Single("select state_name from mlm_state where state_id='$stateid'");
	$location=$db->Extract_Single("select city_name from mlm_city where city_id='$locationid'");
	$randuniq = $com_obj->randuniq();
	$slug = str_replace(" ", "-", $prop_title);
	$slug = strtolower($slug);
	$set="slug='$slug'";
	$set.=",email='$email'";
	$set.=",prop_title='$prop_title'";
	$set.=",category='$category'";
	$set.=",prop_for='$prop_for'";
	$set.=",countryid='$countryid'";
	$set.=",stateid='$stateid'";
	$set.=",locationid='$locationid'";
	$set.=",area_id='$area_id'";
	$set.=",countryname='$countryname'";
	$set.=",statename='$statename'";
	$set.=",location='$location'";
	$set.=",address='$address'";
	$set.=",exp_price='$exp_price'";
	$set.=",booking_price='$booking_price'";
	$set.=",covered_area='$covered_area'";
	$set.=",plot_area='$plot_area'";
	$set.=",poss_sts='$poss_sts'";
	$set.=",bedroom='$bedroom'";
	$set.=",bathroom='$bathroom'";
	$set.=",hall='$hall'";
	$set.=",furnished='$furnished'";
	$set.=",floors='$floors'";
	$set.=",description='$description'";
	$set.=",con_person='$con_person'";
	$set.=",con_num='$con_num'";
	$set.=",call_time='$call_time'";
	$set.=",featured='$ftr'";
	
	if($upd == 1){
		$set .= ",randuniq='$randuniq'";
		$set  .= ",posted_at = '$crcdt'";    
		$set  .= ",post_sts = 1";
		$idvalue = $db->insertid("insert into listings set $set");
		
		if($_FILES['UsrImg']['tmp_name'] != "" && $_FILES['UsrImg']['tmp_name'] != "null") {
			if(count($_FILES['UsrImg']['name'])>5) {
				echo "<script>swal('Oops..', 'You cant upload more than 5 images!', 'error');</script>";
			}
			$files=array();
			$fdata=$_FILES['UsrImg'];
			if(is_array($fdata['name'])){
				for($i=0;$i<count($fdata['name']);++$i){
					$files[]=array(
					'name'    =>$fdata['name'][$i],
					'type'  => $fdata['type'][$i],
					'tmp_name'=>$fdata['tmp_name'][$i],
					'error' => $fdata['error'][$i], 
					'size'  => $fdata['size'][$i] );
				}
			}
			else $files[]=$fdata;
			foreach($files as $file) {
				$fl=$file['tmp_name'];
				$fln=$file['name'];
				$ext=end(explode(".", $fln));
				$ext=strtolower($ext);
				list($width,$height,$type,$attr)=getimagesize($fl);
				if(($ext != "jpg") && ($ext != "jpeg") && ($ext != "gif") && ($ext != "png")) {
					echo "<script>swal('Oops..', 'Unrecognised image type. Try uploading .jpg or .png types.', 'error');</script>";
				}
				else if($width<800 || $height<500) {
					echo "<script>swal('Oops..', 'Image must be atleast 800*500 pixels!', 'error');</script>";
				}
			}
			
			foreach($files as $file) {
				$fl=$file['tmp_name'];
				$fln=$file['name'];
				$ext=end(explode(".", $fln));
				$ext=strtolower($ext);
				$tmpname=basename($fl);
				$pat=array();
				$pat[0]='/php/';
				$pat[1]='/.tmp/';
				$repl=array();
				$repl[0]='';
				$repl[1]='';
				$oname=strtolower(preg_replace($pat, $repl, $tmpname));
				$newname='prop'.$_SESSION['pid'].'_'.$oname.'.'.$ext;
				$org='../images/prop/org/'.$newname;
				$prf='../images/prop/'.$newname;
				$sm='../images/prop/230_144/'.$newname;
				$tsm='../images/prop/74_46/'.$newname;
				move_uploaded_file($fl, $org);

				$resizeObj = new resize($org);
				
				$resizeObj -> resizeImage(800, 500, 'exact');
				$resizeObj -> saveImage($prf, 72);
				
				$resizeObj -> resizeImage(230, 144, 'exact');
				$resizeObj -> saveImage($sm, 100);
				
				$resizeObj -> resizeImage(74, 46, 'exact');
				$resizeObj -> saveImage($tsm, 100);
				
				$set="pid='$idvalue'";
				$set.=",image='$newname'";
				$db->insertrec("insert into listing_images set $set");
			}
		}
		$act = "add";
	}
	else if($upd == 2){
		$set  .= ",updated_at = '$crcdt'";
		$db->insertrec("update listings set $set where id='$idvalue'");
		$act = "upd";
	}
	header("location:prop?page=$pg&act=$act");
	exit;
}

$statelist = "<option value=''>- - Select- -</option>";
$citylist = "<option value=''>- - Select- -</option>";
$arealist = "<option value=''>- - Select- -</option>";
if($upd == 1){
	$hidimg = "style='display:none;'";
	$TextChange = "Add";
}
else if($upd == 2){
$hidimg = "";
$TextChange = "Edit";
	
$GetRecord = $db->singlerec("select * from listings where id='$id'");
@extract($GetRecord);	

$DropDownQry = "select state_id as stateid,state_name from mlm_state where state_id='$stateid'";
$statelist .= $drop->get_dropdown($db,$DropDownQry,$stateid);

$DropDownQry = "select city_id as locationid,city_name from mlm_city where city_id='$locationid'";
$citylist .= $drop->get_dropdown($db,$DropDownQry,$locationid);

$DropDownQry = "select area_id,area_name from mlm_area where area_id='$area_id'";
$arealist .= $drop->get_dropdown($db,$DropDownQry,$area_id);
}
$countrylist = "<option value=''>- - Select- -</option>";
$DropDownQry = "select country_id as countryid,country_name from mlm_country order by country_id asc";
$countrylist .= $drop->get_dropdown($db,$DropDownQry,$countryid);
?>
		<!--Page content-->
		<!--===================================================-->
		<div id="page-content">
			<div class="row">
			  <div class="eq-height">
				 <div class="col-sm-6 eq-box-sm">
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"><?php echo $TextChange;?> Property</h3>
						</div>
						<form class="form-horizontal" method="post" action="propupd" enctype="multipart/form-data">
							<input type="hidden" name="idvalue" value="<?echo $id;?>" />
							<input type="hidden" name="upd" value="<?echo $upd;?>" />							
							<div class="panel-body">
								<table style="padding:25px;">
								    <tr>
										<td>Email <font color="red">*</font></td>
										<td><input type="email" name="email" value="<?php echo $email; ?>" class="form-control" required>
										</td>
									</tr>
									<tr>
										<td>Title <font color="red">*</font></td>
										<td><input type="text" name="prop_title" value="<?php echo $prop_title; ?>" class="form-control" required>
										</td>
									</tr>
									<tr>
										<td>Category <font color="red">*</font></td>
										<td><select name="category" class="form-control">
										<?php $PS_Categ=$db->get_all("select filtername,cat_name from category where cat_status='1'");
										foreach($PS_Categ as $PSCateg):
										$catt=$PSCateg['cat_name'];
										$fcat=$PSCateg['filtername'];
										if($category==$fcat) $ch="selected";
										else $ch="";
										echo '<option value="'.$fcat.'" '.$ch.'>'.$catt.'</option>';
										endforeach;
										?>
										</select>
										</td>
									</tr>
									<tr>
										<td>Property For <font color="red">*</font></td>
										<td><select name="prop_for" class="form-control">
											<option value="rent" <?php if(@$prop_for=='rent') echo "selected"; ?>>Rent</option>
											<option value="sale" <?php if(@$prop_for=='sale') echo "selected"; ?>>Sale</option>
											<option value="lease" <?php if(@$prop_for=='lease') echo "selected"; ?>>Lease</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>Country <font color="red">*</font></td>
										<td><select onchange="countryfn();" name="countryid" id="countryid" class="form-control" required><?php echo $countrylist;?></select>
										</td>
									</tr>
									<tr>
										<td>State <font color="red">*</font></td>
										<td><select onchange="statefn();" name="stateid" id="stateid" class="form-control" required><?php echo $statelist;?></select>
										</td>
									</tr>
									<tr>
										<td>City <font color="red">*</font></td>
										<td><select onchange="cityfn();" name="locationid" id="locationid" class="form-control" required><?php echo $citylist;?></select>
										</td>
									</tr>
									<tr>
										<td>Area <font color="red">*</font></td>
										<td><select name="area_id" id="area_id" class="form-control" required><?php echo $arealist;?></select>
										</td>
									</tr>
									<!--<tr>
										<td>City <font color="red">*</font></td>
										<td><input id="autocomplete" type="text" onFocus="geolocate();" name="location" value="<?php echo $location; ?>" class="form-control" required>
										</td>
									</tr>-->
									<tr>
										<td>Address <font color="red">*</font></td>
										<td><input type="text" name="address" value="<?php echo $address; ?>" class="form-control" required>
										</td>
									</tr>
									<tr>
										<td>Price <font color="red">*</font></td>
										<td><input type="number" name="exp_price" value="<?php echo $exp_price; ?>" class="form-control" required>
										</td>
									</tr>
									<tr>
										<td>Booking/Token <font color="red">*</font></td>
										<td><input type="number" name="booking_price" value="<?php echo $booking_price; ?>" class="form-control" required>
										</td>
									</tr>
									<tr>
										<td>Covered Area <font color="red">*</font></td>
										<td><input type="number" name="covered_area" value="<?php echo $covered_area; ?>" class="form-control" required>
										</td>
									</tr>
									<tr>
										<td>Plot Area <font color="red">*</font></td>
										<td><input type="number" name="plot_area" value="<?php echo $plot_area; ?>" class="form-control" required>
										</td>
									</tr>
									<tr>
										<td>Possession Status <font color="red">*</font></td>
										<td><select name="poss_sts" class="form-control">
											<option value="1" <?php if(@$poss_sts==1) echo "selected"; ?>>Under Construction</option>
											<option value="2" <?php if(@$poss_sts==2) echo "selected"; ?>>Ready to Move</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>Featured </td>
										<td><input type="checkbox" name="featured" <?php if(@$featured==1) echo "checked"; ?>>
										</td>
									</tr>
									<tr>
										<td>Bedroom <font color="red">*</font></td>
										<td><select name="bedroom" class="form-control">
											<option value="1" <?php if(@$bedroom==1) echo "selected"; ?>>1</option>
											<option value="2" <?php if(@$bedroom==2) echo "selected"; ?>>2</option>
											<option value="3" <?php if(@$bedroom==3) echo "selected"; ?>>3</option>
											<option value="4" <?php if(@$bedroom==4) echo "selected"; ?>>4</option>
											<option value="5" <?php if(@$bedroom==5) echo "selected"; ?>>5</option>
											<option value="6" <?php if(@$bedroom==6) echo "selected"; ?>>6</option>
											<option value="7" <?php if(@$bedroom==7) echo "selected"; ?>>7</option>
											<option value="8" <?php if(@$bedroom==8) echo "selected"; ?>>8</option>
											<option value="9" <?php if(@$bedroom==9) echo "selected"; ?>>9</option>
											<option value="10" <?php if(@$bedroom==10) echo "selected"; ?>>10</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>Bathroom <font color="red">*</font></td>
										<td><select name="bathroom" class="form-control">
											<option value="1" <?php if(@$bathroom==1) echo "selected"; ?>>1</option>
											<option value="2" <?php if(@$bathroom==2) echo "selected"; ?>>2</option>
											<option value="3" <?php if(@$bathroom==3) echo "selected"; ?>>3</option>
											<option value="4" <?php if(@$bathroom==4) echo "selected"; ?>>4</option>
											<option value="5" <?php if(@$bathroom==5) echo "selected"; ?>>5</option>
											<option value="6" <?php if(@$bathroom==6) echo "selected"; ?>>6</option>
											<option value="7" <?php if(@$bathroom==7) echo "selected"; ?>>7</option>
											<option value="8" <?php if(@$bathroom==8) echo "selected"; ?>>8</option>
											<option value="9" <?php if(@$bathroom==9) echo "selected"; ?>>9</option>
											<option value="10" <?php if(@$bathroom==10) echo "selected"; ?>>10</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>Hall <font color="red">*</font></td>
										<td><select name="hall" class="form-control">
											<option value="1" <?php if(@hall==1) echo "selected"; ?>>1</option>
											<option value="2" <?php if(@hall==2) echo "selected"; ?>>2</option>
											<option value="3" <?php if(@hall==3) echo "selected"; ?>>3</option>
											<option value="4" <?php if(@hall==4) echo "selected"; ?>>4</option>
											<option value="5" <?php if(@hall==5) echo "selected"; ?>>5</option>
											<option value="6" <?php if(@hall==6) echo "selected"; ?>>6</option>
											<option value="7" <?php if(@hall==7) echo "selected"; ?>>7</option>
											<option value="8" <?php if(@hall==8) echo "selected"; ?>>8</option>
											<option value="9" <?php if(@hall==9) echo "selected"; ?>>9</option>
											<option value="10" <?php if(@hall==10) echo "selected"; ?>>10</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>Furnished Status <font color="red">*</font></td>
										<td><select name="furnished" class="form-control">
											<option value="1" <?php if(@$furnished==1) echo "selected"; ?>>Furnished</option>
											<option value="2" <?php if(@$furnished==2) echo "selected"; ?>>Unfurnished</option>
											<option value="3" <?php if(@$furnished==3) echo "selected"; ?>>Partially Furnished</option></select>
										</td>
									</tr>
									<tr>
										<td>Floors <font color="red">*</font></td>
										<td><select name="floors" class="form-control">
											<option value="1" <?php if(@$floors==1) echo "selected"; ?>>1</option>
											<option value="2" <?php if(@$floors==2) echo "selected"; ?>>2</option>
											<option value="3" <?php if(@$floors==3) echo "selected"; ?>>3</option>
											<option value="4" <?php if(@$floors==4) echo "selected"; ?>>4</option>
											<option value="5" <?php if(@$floors==5) echo "selected"; ?>>5</option>
											<option value="6" <?php if(@$floors==6) echo "selected"; ?>>6</option>
											<option value="7" <?php if(@$floors==7) echo "selected"; ?>>7</option>
											<option value="8" <?php if(@$floors==8) echo "selected"; ?>>8</option>
											<option value="9" <?php if(@$floors==9) echo "selected"; ?>>9</option>
											<option value="10" <?php if(@$floors==10) echo "selected"; ?>>10</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>Ad Summary <font color="red">*</font></td>
										<td><input type="text" name="description" value="<?php echo $description; ?>" class="form-control" required>
										</td>
									</tr>
									<tr>
										<td>Contact Person <font color="red">*</font></td>
										<td><input type="text" name="con_person" value="<?php echo $con_person; ?>" class="form-control" required>
										</td>
									</tr>
									<tr>
										<td>Contact No <font color="red">*</font></td>
										<td><input type="number" name="con_num" value="<?php echo $con_num; ?>" class="form-control" required>
										</td>
									</tr>
									<tr>
										<td>Best time to call <font color="red">*</font></td>
										<td><select name="call_time" class="form-control">
											<option value="1" <?php if(@$call_time==1) echo "selected"; ?>>Anytime</option>
											<option value="2" <?php if(@$call_time==2) echo "selected"; ?>>Morning</option>
											<option value="3" <?php if(@$call_time==3) echo "selected"; ?>>Evening</option></select>
										</td>
									</tr>
									<?php if($upd==1) { ?>
									<tr>
										<td>Image</td>
										<td><input name="UsrImg" type="file" multiple="multiple" required="required"></td>
									</tr>
									<?php } ?>
								</table>
							</div>
							<div class="panel-footer text-left">
								<div class="col-md-4  text-right"><input class="btn btn-info" type="submit" name="submit" value="Submit"></div>
								<a class="btn btn-info" href="prop">Cancel</a>
							</div>
						</form>
						<!--===================================================-->
						<!--End Horizontal Form-->
					</div>
				</div>
			  </div>
			</div>
		</div>
		<!--===================================================-->
		<!--End page content-->
	</div>
	<!--===================================================-->
	<!--END CONTENT CONTAINER-->
	<?php include "leftmenu.php"; ?>
</div>
<?php include "footer.php"; ?>
<script>
function countryfn(){
	var getval = document.getElementById("countryid").value;
	if (window.XMLHttpRequest)
	  {
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
		document.getElementById("stateid").innerHTML = xmlhttp.responseText;
		document.getElementById("locationid").innerHTML = "<option>--select--</option>";
		}
	  }
	var url = "ajaxstate.php?getval="+getval;
	xmlhttp.open("GET",url,true);
	xmlhttp.send();
} 
function statefn(){
	var getval = document.getElementById("stateid").value;
	if (window.XMLHttpRequest)
	  {
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
		document.getElementById("locationid").innerHTML = xmlhttp.responseText;
		}
	  }
	var url = "ajaxcity.php?getval="+getval;
	xmlhttp.open("GET",url,true);
	xmlhttp.send();
} 
function cityfn(){
	var getval = document.getElementById("locationid").value;
	if (window.XMLHttpRequest)
	  {
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
		document.getElementById("area_id").innerHTML = xmlhttp.responseText;
		}
	  }
	var url = "ajaxarea.php?getval="+getval;
	xmlhttp.open("GET",url,true);
	xmlhttp.send();
} 
</script>