<?php
class common extends database {
	function drop_down($array,$getval,$getname){
		$list = "";
		for($astrn=1; $astrn<count($array); $astrn++){
			if($astrn == $getval)
				$list .= "<input type='radio' id='$getname' name='$getname' value='$astrn' checked>$array[$astrn] &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			else	
				$list .= " <input type='radio' id='$getname' name='$getname' value='$astrn'>$array[$astrn]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		}
		return $list;
	}
	//========================================================================	
	function dropdown($array,$getval,$getname){
		$list = "<option name='' value='0'>--select--</option>";
		for($astrn=1; $astrn<count($array); $astrn++){
			if($astrn == $getval)
				$list .= "<option value='$astrn' selected>$array[$astrn]</option>";
			else	
				$list .= "<option value='$astrn'>$array[$astrn]</option>";
		}
		return $list;
	}
	//========================================================================	
	function dropdowninitial($array,$getval){
		$list = "<option name='' value='0'>--select--</option>";
		for($astrn=0; $astrn<count($array); $astrn++){
			if($astrn == $getval)
				$list .= "<option value='$astrn' selected>$array[$astrn]</option>";
			else	
				$list .= "<option value='$astrn'>$array[$astrn]</option>";
		}
		return $list;
	}
	function dropdown_array_view($array,$getval){
		$ret = $array[$getval];
		return $ret;
	}
	//========================================================================	
	function drop_down_view($array,$getval){
		$list = "";
		for($astrn=1; $astrn<count($array); $astrn++){
			if($astrn == $getval)
				$list .= $array[$astrn];
		}
		return $list;
	}
	function counting_days(){
		$start = '2015-01-01';
		$end = date("Y/m/d");
		$diff = (strtotime($end)- strtotime($start))/24/3600; 
		
		return $diff;
	}
	//========================================================================	
	function drop_down_mail($array,$getval){
		$list = $array[$getval];
		return $list;
	}
	//========================================================================	
	function first_letter($string){
		$expr = '/(?<=\s|^)[a-z]/i';
		preg_match_all($expr, $string, $matches);
		$result = implode('', $matches[0]);
		$result = strtoupper($result);
		return $result;
	}
	//========================================================================	
	function int_val($string){
		$ret = preg_replace("/[^0-9]/","",$string);
		return $ret;
	}
	//=======================================================================
	function character($string){
		$ret = preg_replace('/[^A-Za-z]/', '', $string);
		return $ret;
	}
	//=======================================================================
	function user_profile_id($getid){
		$array = array("","00000","0000","000","00","0");
		$charval = preg_replace('/[^A-Za-z]/', '', $getid);
		$getrec = database::singlerec("select user_profileid from register where user_profileid like '%$charval%' order by user_profileid desc");
		$userprofileid = $getrec['user_profileid'];
		if($userprofileid=="")
			$userprofileid=$getid;

		$intval = preg_replace("/[^0-9]/","",$userprofileid);
		$incval = bcadd($intval,1,0);
		$stlen = strlen($incval);
		$zero = $array[$stlen];
		$ret = $charval.$zero.$incval;
		return $ret;
	}
	//========================================================================	
	function hidecontrols($string){
		if($string == "Admin")
			$ret = "";
		else
			$ret = "<style>.btn-default{display:none;} .cntrhid{display:none;}</style>";
		
		return $ret;
	}
	
	// Encrypt Function
	function encrypt($encrypt, $key){
		$encrypt = serialize($encrypt);
		$iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
		$key = pack('H*', $key);
		$mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
		$passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt.$mac, MCRYPT_MODE_CBC, $iv);
		$encoded = base64_encode($passcrypt).'|'.base64_encode($iv);
		return $encoded;
	}

	// Decrypt Function
	function decrypt($decrypt, $key){
		$decrypt = explode('|', $decrypt.'|');
		$decoded = base64_decode($decrypt[0]);
		$iv = base64_decode($decrypt[1]);
		if(strlen($iv)!==mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)){ return false; }
		$key = pack('H*', $key);
		$decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
		$mac = substr($decrypted, -64);
		$decrypted = substr($decrypted, 0, -64);
		$calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
		if($calcmac!==$mac){ return false; }
		$decrypted = unserialize($decrypted);
		return $decrypted;
	}
	
	function datetimestamp($getdate){
		$DateArr = @split("/",$getdate);
		@list($bkDate,$bkMonth,$bkYear) = $DateArr;
		$ret = @mktime(0,0,0,$bkMonth,$bkDate,$bkYear);
		return $ret;
	}
	function expired_dt($call_dt,$tot_month){
		if($call_dt !=""){
			$DateArr = @split("/",$call_dt);
			@list($bkDate,$bkMonth,$bkYear) = $DateArr;
			$ret = @mktime(0,0,0,$bkMonth+$tot_month,$bkDate,$bkYear);
			$ret = date("d/m/Y",$ret);
		}
		else{
			$ret = "";
		}
		return $ret;
	}
	function opt_num(){
		$ret = mt_rand(100000, 999999);
		return $ret;
	}
	function email($from,$to,$subject,$message){
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: <'.$from.'>' . "\r\n";
		@mail($to, $subject, $message, $headers);
	}
	function signup_mail($from,$to,$url){
		$subject = "Confirm your account for property listing";
		$message ="<br>"."Click below link to activate your account. If you believe this is an error, ignore this message and we'll never bother you again.<br/><br/><a target='_blank' href='$url'>Click Here</a>";
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: <'.$from.'>' . "\r\n";
		@mail($to, $subject, $message, $headers);
	}
	function month_days($fromdate){
		/* $DateArr = @explode("-",$fromdate);
		list($dtYear,$dtMonth,$dtDay) = $DateArr;
		$frmdate = $dtYear."-".$dtMonth."-".$dtDay; */
		$todate = date("Y-m-d");
		
		$date1 = strtotime($fromdate);
		$date2 = strtotime($todate);
		$months = 0;
		while (strtotime('+1 MONTH', $date1) < $date2) {
			$months++;
			$date1 = strtotime('+1 MONTH', $date1);
		}
		$ret = $months.' month,'. ($date2 - $date1) / (60*60*24) .' days'; 
		return $ret;
	}
	function timeconvert($from_time,$to_time){
	   $hours=intval($DepartureTime / 3600);
	   $mins=intval($DepartureTime / 60) % 60;
	   $secs=$DepartureTime % 60;
	   $trvaltime=sprintf("%d:%02d:%02d",$hours, $mins, $secs);
	}
	function randuniq(){
		$str='';
		$str.=substr(str_shuffle("01234123456789123489abcdeefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 4);
		$str.=substr(rand(0,time()),0,4);
		return $str;
	}
}
?>