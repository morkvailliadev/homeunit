<?php include "header.php"; ?>
            <!--===================================================-->
            <!--END NAVBAR-->
            <div class="boxed">
                <!--CONTENT CONTAINER-->
                <!--===================================================-->
                <div id="content-container">
                    
                    <div class="pageheader">
                        <h3><i class="fa fa-home"></i> Bank Details</h3>
                        <div class="breadcrumb-wrapper">
                            <span class="label">You are here:</span>
                            <ol class="breadcrumb">
                                <li> <a href="welcome.php"> Home </a> </li>
                                <li class="active"> Bank Details </li>
                            </ol>
                        </div>
                    </div>
<?php
$upd = isset($upd)?$upd:'';
$id = isSet($id) ? $id : '' ;
$act = isSet($act) ? $act : '' ;
$page = isSet($page) ? $page : '' ;
$Message = isSet($Message) ? $Message : '' ;
$acc_name=isset($acc_name)?$acc_name:'';
$acc_no=isset($acc_no)?$acc_no:'';
$bank_name=isset($bank_name)?$bank_name:'';
$ifsc_code=isset($ifsc_code)?$ifsc_code:'';
$branch_name=isset($branch_name)?$branch_name:'';

if($submit){
    $crcdt = time();
	$acc_name = trim(addslashes($acc_name));
	$acc_no = trim(addslashes($acc_no));
	$bank_name = trim(addslashes($bank_name));
	$ifsc_code = trim(addslashes($ifsc_code));
	$branch_name = trim(addslashes($branch_name));
	
	$set  = "acc_name = '$acc_name'";
	$set  .= ",acc_no = '$acc_no'";
	$set  .= ",bank_name = '$bank_name'";
	$set  .= ",ifsc_code = '$ifsc_code'";
	$set  .= ",branch_name = '$branch_name'";
	$db->insertrec("update bank_detail set $set where id='1'");
	$act="upd";
	header("location:bank_detail.php?page=$pg&act=$act");
	exit;
}
if($upd == 1)
	$hidimg = "style='display:none;'";
else if($upd == 2)
	$hidimg = "";

$GetRecord=$db->singlerec("select * from bank_detail where id='1'");
@extract($GetRecord);
?>

<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
<script type="text/javascript" src="js/tinymce.js" ></script>
<!--Page content-->
<!--===================================================-->
<div id="page-content">
<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
<div class="panel-heading">
<?php
if($act=="upd") echo "<br><h4><font color='green'>&nbsp; Bank details updated successfully!</font></h4>";
?>
	</div>
		<div class="panel-body">
			<table>
				<thead>
					<form method="post" action="bank_detail.php" enctype="multipart/form-data" class="form-horizontal" >
					<input type="hidden" name="idvalue" value="<?php echo $id; ?>" />
					<input type="hidden" name="pg" value="<?php echo $page; ?>" />
					<input type="hidden" name="upd" value="<?php echo $upd; ?>" />
					<tr>
						<td valign="top"><label>Account Name  <font color="red">*</font></label></td>
						<td><input class="form-control" type="text" name="acc_name" value="<?php echo $acc_name; ?>" required></td>
					</tr>
					<tr>
						<td valign="top"><label>Account Number  <font color="red">*</font></label></td>
						<td><input class="form-control" type="text" name="acc_no" value="<?php echo $acc_no; ?>" required></td>
					</tr>
					<tr>
						<td valign="top"><label>Bank Name  <font color="red">*</font></label></td>
						<td><input class="form-control" type="text" name="bank_name" value="<?php echo $bank_name; ?>" required></td>
					</tr>
					<tr>
						<td valign="top"><label>IFSC Code  <font color="red">*</font></label></td>
						<td><input class="form-control" type="text" name="ifsc_code" value="<?php echo $ifsc_code; ?>" required></td>
					</tr>
					<tr>
						<td valign="top"><label>Branch Name  <font color="red">*</font></label></td>
						<td><input class="form-control" type="text" name="branch_name" value="<?php echo $branch_name; ?>" required></td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<br><input type="submit" name="submit" class="btn btn-primary" value="Save">
							<input type="reset" name="reset" class="btn btn-primary" value="Reset">
						</td>
					</tr>
				  </form>
				</thead>
			</table>
		</div>
	</div>
</div>
<!--===================================================-->
<!--End page content-->
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<?php include "leftmenu.php"; ?>
</div>
<?

include "footer.php";
?>
