<?php include"header.php"; ?>
<div class="boxed">
	<!--CONTENT CONTAINER-->
	<!--===================================================-->
	<div id="content-container">
		<?php //include "header_nav.php"; ?>
		<div class="pageheader">
			<h3><i class="fa fa-home"></i>Area </h3>
			<div class="breadcrumb-wrapper">
				<span class="label">You are here:</span>
				<ol class="breadcrumb">
					<li> <a href="welcome.php"> Home </a> </li>
					<li class="active"> Area </li>
				</ol>
			</div>
		</div>
		
<?
$upd=isset($upd)?$upd:'';
$sid=isSet($sid)?$sid:'';
$cid=isSet($cid)?$cid:'';
$ctid = isSet($ctid) ? $ctid : '' ;
$area_id=isSet($area_id)?$area_id:'';
$act=isSet($act)?$act:'';
$page=isSet($page)?$page:'';
$Message=isSet($Message)?$Message :'';
$area_name=isSet($area_name)?$area_name:'';

if($submit){
    $crcdt = time();
    $area_name  = trim(addslashes($area_name));
	$checkStatus = $db->check1column("mlm_area","area_name",$area_name);
	if($area_name != ''){
		if($upd == 2)
			$checkStatus = 0;
			
		if($checkStatus == 0){
			$set  = "area_name = '$area_name'";
			if($upd == 1){  
				$set  .= ",city_id = '$ctid'";
				$set  .= ",state_id = '$sid'";
				$set  .= ",country_id = '$cid'";
				$db->insertrec("insert into mlm_area set $set");
				$act = "add";
			}
			else if($upd == 2){
				$db->insertrec("update mlm_area set $set where area_id='$area_id'");
				$act = "upd";
			}
			header("location:area.php?&cid=$cid&sid=$sid&ctid=$ctid");
			exit;
		}
		else{
			$upd = $upd ;
			$Message = "<font color='red'>$area_name Already Exit</font>";
		}
    } 
	else{
		$upd = $upd ;
		$Message = "<font color='red'>Input Fields Marked With * are compulsory</font>";
	}
}
if($upd==1){
	$TextChange = "Add";
}
else if($upd==2){
	$TextChange = "Update";
	$Getmaincat=$db->singlerec("select * from mlm_area where area_id='$area_id'");
    $area_name = stripslashes($Getmaincat['area_name']);
}

?>
		
			<!--Page content-->
		<!--===================================================-->
		<div id="page-content">
			<div class="row">
			  <div class="eq-height">
				 <div class="col-sm-6 eq-box-sm">
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"><?php echo $TextChange;?> Area</h3>
						</div>
						<form class="form-horizontal" method="post" action="">
							<input type="hidden" name="idvalue" value="<?echo $sid;?>" />
							<input type="hidden" name="upd" value="<?echo $upd;?>" />							
							<div class="panel-body">
								<table style="padding:25px;">
									<tr>
										<td>Name <font color="red">*</font></td>
										<td><input type="text" name="area_name" id="area_name" value="<?php echo $area_name; ?>" class="form-control">
										</td>
									</tr>
							    </table>
							</div>
							<div class="panel-footer text-left">
								<div class="col-md-4  text-right"><input class="btn btn-info" type="submit" name="submit" value="Submit"></div>
								<a class="btn btn-info" href="area.php?cid=<?echo $cid;?>&stid=<?echo $stid;?>&ctid=<?echo $ctid;?>">Cancel</a>
							</div>
						</form>
						<!--===================================================-->
						<!--End Horizontal Form-->
					</div>
				</div>
			  </div>
			</div>
		</div>
		<!--===================================================-->
		<!--End page content-->
	</div>
	<!--===================================================-->
	<!--END CONTENT CONTAINER-->
	<?php include "leftmenu.php"; ?>
</div>
<?php include "footer.php"; ?>