<?php include"header.php"; ?>
<div class="boxed">
	<!--CONTENT CONTAINER-->
	<!--===================================================-->
	<div id="content-container">
		<?php //include "header_nav.php"; ?>
		<div class="pageheader">
			<h3><i class="fa fa-home"></i> Category </h3>
			<div class="breadcrumb-wrapper">
				<span class="label">You are here:</span>
				<ol class="breadcrumb">
					<li><a href="welcome.php"> Home </a></li>
					<li class="active"> Category </li>
				</ol>
			</div>
		</div>
<?php
$id = isSet($id) ? $id : '' ;
$upd = isSet($upd) ? $upd : '' ;
$status = isSet($status) ? $status : '' ;
$Message = isSet($Message) ? $Message : '' ;
$act = isSet($act) ? $act : '' ;

if($act == "del") {
	$db->insertrec("delete from category where id='$id'");
	echo "<script>window.location='category.php?act=del1&page=$page';</script>";
    @header("location:category.php?act=del1&page=$page");
    exit ;
}
if($status == "1") {
	$crcdt = time();
    $db->insertrec("update category set cat_status='0' where id='$id'");
	echo "<script>window.location='category.php?act=sts';</script>";
    @header("location:category.php?act=sts");
    exit ;
}
else if($status == "0") {
    $db->insertrec("update category set cat_status='1' where id='$id'");
	echo "<script>window.location='category.php?act=sts';</script>";
    @header("location:category.php?act=sts");
    exit ;
}

$GetRecord=$db->get_all("select * from category order by id desc");
if(count($GetRecord)==0)
    $Message="No Record found";
$disp = "";
for($i = 0 ; $i < count($GetRecord) ; $i++) {
    $id = $GetRecord[$i]['id'] ;
	$cat_name=$GetRecord[$i]['cat_name'];
	$active_status = $GetRecord[$i]['cat_status'] ;
	$slno = $i + 1 ;
	if($active_status == '0'){
		$DisplayStatus = $GT_InActive;
		$Title = "Deactive";
		$status_active = "Active";
		$EditLink = "<a class='btn btn-default' ><i class='fa ><font color='red'>--</font></i></a>";
	}	
    else if($active_status == '1'){
        $DisplayStatus = $GT_Active;
		$Title = "Active";
		$status_active = "Deactive";
		$EditLink = "<a href='categoryupd.php?upd=2&id=$id' data-toggle='tooltip' title='Edit' class='btn btn-default' ><i class='fa fa-edit'></i></a>";
	}
    $disp .="<tr>
				<td>$slno</td>
				<td align='left'>$cat_name</td>
				<td width='20%'>
				<div class='btn-group btn-group-xs'>
					<a href='category.php?id=$id&status=$active_status' title='$Title' class='btn btn-default' data-toggle='tooltip'>$DisplayStatus</a>
					$EditLink
					<a href='category.php?id=$id&act=del' onclick='confirm();' class='btn btn-default' title='Delete' data-toggle='tooltip'>$GT_Delete</a>
				</div>
				</td>
			</tr>";
}

if($act == "del1")
    $Message = "<font color='green'><b>Deleted Successfully</b></font>" ;
else if($act == "upd")
    $Message = "<font color='green'><b>Updated Successfully</b></font>" ;
else if($act == "add")
    $Message = "<font color='green'><b>Added Successfully</b></font>" ;
else if($act == "sts")
    $Message = "<font color='green'><b>Status Changed Successfully</b></font>" ;
?>

<!--Page content-->
                    <!--===================================================-->
                    <div id="page-content">
                        <!-- Basic Data Tables -->
                        <!--===================================================-->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"><?echo $Message;?></h3>
                            </div>
                            <div class="panel-body">
								<div class="col-sm-12 text-right"><a class="btn btn-info" href="categoryupd.php?upd=1">Add New</a></div>
							    <table id="demo-dt-basic" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
											<th>S.No</th>
											<th>Category Name</th>
											<th class='cntrhid'>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody><?echo $disp; ?></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--===================================================-->
                    <!--End page content-->
                </div>
                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
			<?php include "leftmenu.php"; ?>
            </div>
<?php include "footer.php"; ?>

