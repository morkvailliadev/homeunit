<?php 
include 'header.php';
include 'profile_header.php';
?>

<div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20">
        <span class="blackhead pdl10">Dashboard </span>
		
    </div><!--col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20-->
         
		 <?php include "profile_left.php"; ?>
         <div class="col-md-9 col-sm-12 col-xs-12 mt20">
             <div class="col-md-12 col-sm-12 col-xs-12 profile-brdr-2">
                 <div class="pdt10">
                 <div class="row col-md-10 col-sm-6 col-xs-12 property-dash-head">My Account</div>
                 <div class="row col-md-2 col-sm-6 col-xs-12 mb10 pull-right"><a href="post-ad"><input type="button" class="btn btn-view-detail" value="Add Property" /></a></div><br><br><hr>
                 </div><!--class="pdt10"-->
                    
                    
                     <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Personal Info</a></li>
                          <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Change Password</a></li>
                          <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Social</a></li>
                          <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Privacy Settings</a></li>
                        </ul>
                      
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active change" id="home">						
                               <form id="maform" action="my-account" method="post">
                                  <div class="form-group">
                                    <label class="label-c">Full Name :</label>
                                    <input type="text" class="form-control" name="fullname" value="<?php echo @$row["fullname"]; ?>">
                                  </div>
                                  
                                   <div class="form-group">
                                    <label class="label-c">Email Address : </label>
                                    <input type="email" class="form-control" name="" value="<?php echo @$row["email"]; ?>" disabled>
                                  </div>
                                  
                                  <div class="form-group">
                                    <label class="label-c">Mobile Number : </label>
                                    <input type="text" class="form-control" name="mobile" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="<?php echo @$row["mobile"]; ?>">
                                  </div>
                                  
                                   <div class="form-group">
                                    <label class="label-c">Website : </label>
                                    <input type="text" class="form-control" name="website" value="<?php echo @$row["website"]; ?>">
                                  </div>
                                  
                                  <div class="form-group">
                                    <label class="label-c">Details : </label>
                                    <textarea class="form-control" name="details" rows="5"><?php echo @$row["details"]; ?></textarea>
                                  </div>
                                  
                                  <input type="submit" class="btn btn-view-detail" name="prof_upd" value="Update">
                                </form>
                               
                            </div><!--home-->
                            <div role="tabpanel" class="tab-pane change" id="profile">
                                 <form id="cpform" action="my-account" method="post">
                                  <div class="form-group ">
                                    <label class="label-c">Current Password</label>
                                    <input type="password" class="form-control" name="cpass" placeholder="Current Password">
                                  </div>
                                  
                                   <div class="form-group">
                                    <label class="label-c">New Password</label>
                                    <input type="password" class="form-control" name="npass" placeholder="New Password">
                                  </div>
                                  
                                   <div class="form-group">
                                    <label class="label-c">Retype Password</label>
                                    <input type="password" class="form-control" name="rpass" placeholder="Retype Password">
                                  </div>
                                  <input type="submit" class="btn btn-view-detail" name="chpass" value="Update">
                                </form>
                            </div><!--profile-->
                            <div role="tabpanel" class="tab-pane change" id="messages">
                                 <form id="updsoc" action="my-account" method="post">
                                  <div class="form-group ">
                                    <label class="label-c">Facebook</label>
                                    <input type="text" class="form-control" name="fb" placeholder="Facebook URL" value="<?php echo $row['facebook']; ?>">
                                  </div>
                                  
                                   <div class="form-group">
                                    <label class="label-c">Twitter</label>
                                    <input type="text" class="form-control" name="twt" placeholder="Twitter URL" value="<?php echo $row['twitter']; ?>">
                                  </div>
                                  
                                   <div class="form-group">
                                    <label class="label-c">Skype</label>
                                    <input type="text" class="form-control" name="skp" placeholder="Skype Username" value="<?php echo $row['skype']; ?>">
                                  </div>
                                  <input type="submit" class="btn btn-view-detail" name="updsoc" value="Update">
                                </form>
                            </div><!--messages-->
                            <div role="tabpanel" class="tab-pane change" id="settings">
                               
                               <form action="my-account" method="post">
                                   <div class="form-group">
                                       <p style="color:#333;">Don't miss out on our great offers. Receive  from all our top property via e-mail</p>
									   <?
									   $mail=$_SESSION['usr'];
									   $chk=$db->check1column("newsletter", "email", $mail);
									   ?>
                                       <label class="radio-inline" style="color:#333;">
                                        <input type="radio" name="rcv" id="inlineRadio1" value="1" <?php if($chk==1) echo "checked"; ?>> Yes
                                      </label>
                                       <label class="radio-inline" style="color:#333;">
                                        <input type="radio" name="rcv" id="inlineRadio1" value="0" <?php if($chk==0) echo "checked"; ?>> No
                                      </label>
                                      
                                      <div class="checkbox">
                                        <label style="color:#333;">
                                          <input type="checkbox" name="agreed" value="1" checked="checked">
                                          I have read and accepted the Terms and conditions and Privacy policy
                                        </label>
                                      </div>
                                   </div>
                                   
                                    <input type="submit" class="btn btn-view-detail" name="nletter" value="Submit">
                               </form>
                            
                            </div><!--settings-->
                        </div><!--tab-content-->
                        <!-- Nav tabs end-->
                    
                 
             </div><!--col-md-12 col-sm-12 col-xs-12 profile-brdr-->
         </div><!--col-md-9 col-sm-12 col-xs-12-->
    </div><!--row-->
</div> <!--container-->


<?php include 'footer.php'; ?>