<footer class="">
    <div class="footer-top">
        <div class="container-wrapper">
            <div class="links-container">
            <div class="links-wrapper">
                <h3>Property Category</h3>
                <ul>
                    <?php
                    $PS_Categ=$db->get_all("select filtername,cat_name from category where cat_status='1'");
                    foreach($PS_Categ as $PSCateg):
                        $catt=$PSCateg['cat_name'];
                        $fcat=$PSCateg['filtername'];
                        if(isset($cat)==$fcat) $ch="selected";
                        else $ch="";
                        echo "<p><a href='property-list.php?cat=$fcat' class='footerboxlink'>$catt</a></p>";
                    endforeach;
                    ?>
                </ul>
            </div>
            <div class="links-wrapper">
                <h3>Listing by Location</h3>
                <ul>
                    <?php $avcity=$db->get_all("select DISTINCT location from listings where post_sts='1' order by location asc");
                    for($ct=0;$ct<count($avcity);$ct++){
                        $ctnam=$avcity[$ct]['location'];
                        if($ct==0)
                            echo "<p class='mt20'><a href='property-list?srchloc=$ctnam' class='footerboxlink'>$ctnam</a></p>";
                        else
                            echo "<p><a href='property-list?srchloc=$ctnam' class='footerboxlink'>$ctnam</a></p>";
                    }
                    ?>
                </ul>
            </div>
            <div class="links-wrapper">
                <h3>Featured Properties</h3>
                <?php
                $prop=$db->get_all("select prop_title,slug,randuniq from listings where post_sts=1 and featured=1 order by mplan desc limit 0, 5");
                for($fr=0;$fr<count($prop);$fr++){
                    $slug=$prop[$fr]['slug'];
                    $randuniq=$prop[$fr]['randuniq'];
                    $frtrprop=ucwords($prop[$fr]['prop_title']);
                    if($fr==0)
                        echo "<p class='mt20'><a href='$siteurl/listing/$randuniq/$slug' class='footerboxlink'>$frtrprop</a></p>";
                    else
                        echo "<p><a href='$siteurl/listing/$randuniq/$slug' class='footerboxlink'>$frtrprop</a></p>";
                }
                ?>
            </div>
        </div>
        <div class="signup-block">
            <h3>Luxury Properties</h3>
            <div class="signup-input-block">
                <form action="">
                    <input type="text" placeholder="Please enter your email address">
                    <button type="submit">Signup</button>
                </form>
            </div>
            <div class="social-connect">
                <h3>Social Connect</h3>
                <ul>
                    <li><a href="<?php echo $facebook;?>" target="_blank"><i id="social-fb" class="facebook fa fa-facebook"></i></a></li>
                    <li><a href="<?php echo $twitter;?>" target="_blank"><i id="social-tw" class="twitter fa fa-twitter"></i></a></li>
                    <li><a href="<?php echo $gplus;?>" target="_blank"><i id="social-gp" class="google fa fa-google-plus"></i></a></li>
<!--                    <li><a href="--><?php //echo $skype;?><!--" target="_blank"><i id="social-em" class="social skype"></i><img src="--><?php //echo $siteurl;?><!--/assets/images/social icon 4.png"></a></li>-->
                </ul>
            </div>
        </div>
        </div>
        <!--Footer-->
    </div>
        <div class="footer-bottom">
            <div class="container-wrapper">
                <span class="copyright"><?php echo $copyrights;?></span>
                <div class="links">
                    <a href="">Terms and Conditions</a>
                    <a href="">Privacy Policy</a>
                </div>
            </div>
        </div>
</footer>
<!--End Footer-->

 <script>
	 $('[rel="popover"]').popover({
        container: 'body',
        html: true,
        content: function () {
            var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
            return clone;
        }
    }).click(function(e) {
        e.preventDefault();
    });
	</script>
	
	
	<script type="application/javascript">
		$(".pop").popover({ trigger: "manual" , html: true, animation:false})
    .on("mouseenter", function () {
        var _this = this;
        $(this).popover("show");
        $(".popover").on("mouseleave", function () {
            $(_this).popover('hide');
        });
    }).on("mouseleave", function () {
        var _this = this;
        setTimeout(function () {
            if (!$(".popover:hover").length) {
                $(_this).popover("hide");
            }
        }, 300);
});

    </script>

</body></html>

<?php include "jsfunction.php"; ?>