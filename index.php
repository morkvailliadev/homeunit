<?php
include_once "header.php";
include_once "mapapi.php";
if($cms_on != $cms_approve_st){echo "<script>location.href='$cms_approve';</script>";}
?>

<style>
   .carousel { margin-top:0px;}
   .carousel-control {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    width: 15%;
    font-size: 20px;
    color: #fff;
    text-align: center;
    text-shadow: 0 1px 2px rgba(0,0,0,.6);
    background-color: rgba(0,0,0,0);
    filter: alpha(opacity=50);
    opacity: .5;
    z-index: 4;
}
.carousel-inner > .item > a > img, .carousel-inner > .item > img
{width:100%;}


</style>

        <div class="container property-header">
            <div class="header-filter">
                <h3>Lets experts help you find your perfect property</h3>
                <form name="quicksrch1" action="property-list" method="post">
                    <div class="inputs-block">
                        <div class="input-wrapper">
                            <input type="text" placeholder="City / Location">
                            <span class="fa fa-search search-icon"></span>
                        </div>
                        <div class="input-wrapper">
                            <select name="bed" id="">
                                <option value="">Bedrooms</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </div>
                        <div class="input-wrapper">
                            <select name="tbud" id="">
                                <option value="">Budget</option>
                                <option value="1000-5000">1000-5000</option>
                                <option value="5001-10000">5001-10000</option>
                                <option value="10001-15000">10001-15000</option>
                                <option value="15001-20000">15001-20000</option>
                                <option value="20001-20000">20001-25000</option>
                                <option value="20001-25000">25001-30000</option>
                                <option value="25001-30000">30001-35000</option>
                                <option value="30001-35000">35001-40000</option>
                                <option value="35001-40000">40001-45000</option>
                                <option value="40001-45000">40001-45000</option>
                                <option value="45001-50000">45001-50000</option>
                            </select>
                        </div>
                        <div class="input-wrapper search">
                            <button type="submit" name="quicksrch1">Search</button>
                        </div>
                    </div>
                </form>
                <form action="">
                    <div class="inputs-block bottom-inputs">
                        <div class="input-wrapper">
                            <span class="quick-search-new">Quick Search <span></span><span></span></span>
                        </div>
                        <div class="input-wrapper">
                            <select name="categ">
                                <option value="">Property Category</option>
                                <?php
                                foreach($PS_Cat as $cat):
                                    echo '<option value="'.$cat['filtername'].'">'.$cat['cat_name'].'</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div class="input-wrapper">
                            <select name="pfor">
                                <option value="">Property For</option>
                                <option value="rent">Rent</option>
                                <option value="sale">Sale</option>
                                <option value="lease">Lease</option>
                            </select>
                        </div>
                        <div class="input-wrapper">
                            <button type="submit" name="quicksrch2"><span></span></button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!--container-->


<div class="select-block">
    <div class="container-wrapper">
        <div class="logos-select">
            <div class="input-wrapper">
                <select name="" id="">
                    <option value="" hidden>Select Your City</option>
                    <option value="">2</option>
                </select>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="col-md-9 col-sm-12 col-xs-12 mt20">
     <p><span class="blackhead"> TITANIUM </span><span class="greenhead"> AGENCIES</span></p>
   </div>  
  <ul id="flexiselDemo3">
    <?php $agentrec=$db->get_all("select prof_image from register where role='Agent' limit 0,10");
	foreach($agentrec as $agntrec){
		$profimage=$agntrec['prof_image'];
		echo "<li><img src='$siteurl/images/user/$profimage' /></li>";
	}
	?>
  </ul>    
</div><!--container-->

<!--------------------------------------Newest Property---------------------------------------->

<div class="container mt30">
     <div class="col-md-12 col-sm-12 col-xs-12 mt10">
          
           <div class="col-md-12 col-sm-12 col-xs-12 brdr-new-1">
              <!---------Slider List--------->
<!---------------------------------------------------------Featured Property-------------------------------------------------------->

          <div class="row brdr-new-1-bg">
             <div class="corner-ribbon top-left sticky red shadow">New</div>
                      <div class="col-md-6">
                      
                          <p><span class="blackhead"> Featured </span><span class="greenhead" style="color: #E43;"> Properties</span></p>
                      </div>
                      <div class="col-md-6">
                      
                          <!-- Controls -->
                          <div class="controls pull-right">
                              <a class="left fa fa-chevron-left btn carshop-btn control-btn arrow-brdr" href="#carousel-example-1"
                                  data-slide="prev"></a><a class="right fa fa-chevron-right control-btn btn carshop-btn arrow-brdr" href="#carousel-example-1"
                                      data-slide="next"></a>
                          </div>
                      </div>
                  </div>
                  
                  <div id="carousel-example-1" class="carousel slide " data-ride="carousel">
                      <!-- Wrapper for slides -->
                      <div class="carousel-inner mt10">
                        
						<?php $arrcitys=array();
						$count = 1;
						$result=$db->get_all("select * from listings where post_sts=1 and featured=1 order by mplan desc limit 0, 40");
						foreach($result as $prop) {
							$arrcitys[]=$prop['locationid'];
							$im=$db->singlerec("select * from listing_images where pid='".$prop['id']."' limit 1");
						if($count==1){
							$active="active";
						}
						else {
							$active="";
						}
						if ($count%8 == 1) {  
							echo "<div class='item ".$active."'>";
							echo "<div class='row'>";
						}
						?>                           
                                  <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top:5px;">
                                      <div class="carshop-item brdr-2">
                                          <div class="photo">
                                              <a href="<?php echo $siteurl;?>/listing/<?php echo $prop['randuniq']; ?>/<?php echo $prop['slug']; ?>"><img src="<?php echo $siteurl;?>/images/prop/<?php echo $im['image']; ?>" class="img-responsive" alt="" /></a>
                                          </div><!--photo-->
                                             <div class="info row">                                    
                                                  <div class="col-md-12">
                                                         <p class="new-properties-head pdl10"><?php echo $prop['prop_title']; ?></p>
                                                         <p class="new-properties-place pdl10 "><?php echo $prop['location']; ?></p>
                                                         <p class=" greenhead pdl10"><?php echo $PSCurncy . $prop['exp_price']; ?></p>
                                                         <div class=" col-md-12">
                                                             <div class="row">
                                                                 <div class=" col-xs-4 brdr-2 new-properties-details">
                                                                      <img src="<?php echo $siteurl;?>/assets/images/square.png" /><?php echo $prop['covered_area']; ?>
                                                                 </div><!--col-md-4-->
                                                                 <div class=" col-xs-4 brdr-2 new-properties-details">
                                                                      <img src="<?php echo $siteurl;?>/assets/images/bed.png" /><span class="pdl10"><?php echo $prop['bedroom']; ?></span>
                                                                 </div><!--col-md-4-->
                                                                 <div class=" col-xs-4 brdr-2 new-properties-details">
                                                                    <img src="<?php echo $siteurl;?>/assets/images/bath.png" /><span class="pdl10"><?php echo $prop['bathroom']; ?></span>
                                                                 </div><!--col-md-4-->
                                                             </div><!--row-->
                                                         </div><!--brdr-2-->
                                                   </div><!--price col-md-12-->                                      
                                                  </div><!--info row-->
                                              </div><!--carshop-item-->
                                         </div><!--col-md-3-->
								<?php                      
								if ($count%8 == 0) {
									echo '</div></div>';
								}
								$count++;
								}
								if ($count%8 != 1) echo '</div></div>';
								?>  										 
                      </div>
              </div> 

          </div><!--col-md-12 col-sm-12 col-xs-12 brdr-->
          
          
          
          <div class="col-md-12 col-sm-12 col-xs-12 brdr-new-1 mt20">

          <div class="row brdr-new-1-bg">
                      <div class="col-md-6 col-sm-6 col-xs-6">
                          <p><span class="blackhead"> Featured </span><span class="greenhead"> Agents</span></p>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-6">
                          <!-- Controls -->
                          <div class="controls pull-right">
                              <a class="left fa fa-chevron-left btn control-btn carshop-btn arrow-brdr" href="#carousel-example-5" data-slide="prev"></a>
                              <a class="right fa fa-chevron-right btn control-btn carshop-btn arrow-brdr" href="#carousel-example-5" data-slide="next"></a>
                          </div>
                      </div>
                </div><!--row-->
                  
                  <div id="carousel-example-5" class="carousel slide" data-ride="carousel">
                      <!-- Wrapper for slides -->
                      <div class="carousel-inner">
                              <?php /*$agentrec=$db->get_all("select id,prof_image,fullname,mobile from register where role='Agent'");
								foreach($agentrec as $agntrec){
									$regid=$agntrec['id'];
									$fullname=$agntrec['fullname'];
									$mobile=$agntrec['mobile'];
									$profimage=$agntrec['prof_image'];
									$imgurl="$siteurl/images/user/$profimage";
									if(getimagesize($imgurl) !== false){$imgeurl=$imgurl;}else{$imgeurl="$siteurl/images/user/noimage.jpg";}
									*/
						$count = 1;
						$result=$db->get_all("select id,prof_image,fullname,mobile from register where role='Agent'");
						foreach($result as $agntrec) {
							$regid=$agntrec['id'];
							$fullname=$agntrec['fullname'];
							$mobile=$agntrec['mobile'];
							$profimage=$agntrec['prof_image'];
							$imgurl="$siteurl/images/user/$profimage";
							if(getimagesize($imgurl) !== false){$imgeurl=$imgurl;}else{$imgeurl="$siteurl/images/user/noimage.jpg";}
						if($count==1){
							$active="active";
						}
						else {
							$active="";
						}
						if ($count%18 == 1) {  
							echo "<div class='item ".$active."'>";
							echo "<div class='row'>";
						}
								?>
                                
                                <div class="col-md-2 col-sm-3  col-xs-6 mt20">
                                     <a href="#" rel="popover" class="pop" data-placement="top"  data-trigger="focus" data-popover-content="#list-popover<?php echo $regid;?>" data-original-title="" title="">
                                        <img class="avatar2 img-responsive img-rounded" src="<?php echo $imgeurl;?>" alt="agent">
                                      </a>
                                      <div id="list-popover<?php echo $regid;?>" class="hide">
                                               <strong> <?php echo $fullname;?></strong><br />
                                               <!--<small><i class="fa fa-map-marker"></i> <?php echo $fullname;?></small> <br />-->
                                               <small><i class="fa fa-mobile"></i> <?php echo $mobile;?></small> <br />
                                       </div><!--list-popover-->
                                </div><!--col-2-->
                               <?
							   if ($count%18 == 0) {
									echo '</div></div>';
								}
								$count++;
								}
								if ($count%18 != 1) echo '</div></div>'; 
							    ?> 
                                 
                      </div>
              </div> 


<!-------End Slider List--------->
          </div><!--col-md-12 col-sm-12 col-xs-12 brdr-->
          
          
          
          
          
         
          
          
          
          
          
          <div class="col-md-12 col-sm-12 col-xs-12 brdr-new-1 mt20">
              <!---------Slider List--------->

          <div class="row brdr-new-1-bg">
                      <div class="col-md-6 col-sm-6 col-xs-6">
                          <p><span class="blackhead"> New </span><span class="greenhead"> Properties</span></p>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-6">
                          <!-- Controls -->
                          <div class="controls pull-right">
                              <a class="left fa fa-chevron-left btn control-btn carshop-btn arrow-brdr" href="#carousel-example"
                                  data-slide="prev"></a><a class="right fa fa-chevron-right btn control-btn carshop-btn arrow-brdr" href="#carousel-example"
                                      data-slide="next"></a>
                          </div>
                          
                      </div>
                </div><!--row-->
                  
                  <div id="carousel-example" class="carousel slide " data-ride="carousel">
                      <!-- Wrapper for slides -->
                      <div class="carousel-inner mt10">
						<?
						$count = 1;
						$res=mysql_query("select * from listings where post_sts=1 and featured=0 order by mplan desc limit 0, 8");
						while($prop=mysql_fetch_assoc($res)) {
							$arrcitys[]=$res['locationid'];
							$im=$db->singlerec("select * from listing_images where pid='".$prop['id']."' limit 1");
						if($count==1){
							$active="active";
						}
						else {
							$active="";
						}
						if ($count%4 == 1) {  
							echo "<div class='item ".$active."'>";
							echo "<div class='row'>";
						}
						?>             
			  <div class="col-md-3 col-sm-6 col-xs-12 ">
				  <div class="carshop-item brdr-2">
					  <div class="photo">
						  <a href="<?echo $siteurl;?>/listing/<?php echo $prop['randuniq']; ?>/<?php echo $prop['slug']; ?>"><img src="<?echo $siteurl;?>/images/prop/<?php echo $im['image']; ?>"  alt="*" /></a>
					  </div><!--photo-->
						 <div class="info row">                                    
							  <div class="col-md-12">
									 <p class="new-properties-head pdl10"><?php echo $prop['prop_title']; ?></p>
									 <p class="new-properties-place pdl10 "><?php echo $prop['location']; ?></p>
									 <p class=" greenhead pdl10"><?php echo $PSCurncy . $prop['exp_price']; ?></p>
									 <div class=" col-md-12">
										 <div class="row">
											 <div class=" col-xs-4 brdr-2 new-properties-details">
												  <img src="<?echo $siteurl;?>/assets/images/square.png" /><?php echo $prop['covered_area']; ?>
											 </div><!--col-md-4-->
											 <div class=" col-xs-4 brdr-2 new-properties-details">
												  <img src="<?echo $siteurl;?>/assets/images/bed.png" /><span class="pdl10"><?php echo $prop['bedroom']; ?></span>
											 </div><!--col-md-4-->
											 <div class=" col-xs-4 brdr-2 new-properties-details">
												<img src="<?echo $siteurl;?>/assets/images/bath.png" /><span class="pdl10"><?php echo $prop['bathroom']; ?></span>
											 </div><!--col-md-4-->
										 </div><!--row-->
									 </div><!--brdr-2-->
							   </div><!--price col-md-12-->                                      
							  </div><!--info row-->
						  </div><!--carshop-item-->
					 </div><!--col-md-3-->
						<?php                      
						if ($count%4 == 0) {
							echo '</div></div>';
						}
						$count++;
						}
						if ($count%4 != 1) echo '</div></div>';
						?>  
			</div>
	  </div> 


<!-------End Slider List--------->
          </div><!--col-md-12 col-sm-12 col-xs-12 brdr-->
          
          <!---------------------------------------------------------All Property------------------------------------------------------->
          
          <div class="col-md-12 col-sm-12 col-xs-12 brdr-new-1 mt20">
              <!---------Slider List--------->

          <div class="row brdr-new-1-bg">
                      <div class="col-md-12">
                          <p><span class="blackhead"> All </span><span class="greenhead"> Properties</span><a href="property-list"><span class="btn btn-view pull-right">View All</span></a></p>
                      </div>     <!--col-md-12-->                 
                  </div>
                  
                  <div id="carousel-example-1" class="carousel slide " data-ride="carousel">
                      <!-- Wrapper for slides -->
                      <div class="carousel-inner mt10">
					  <?php
						$count = 1;
						$res=mysql_query("select * from listings where post_sts=1 order by id desc limit 0, 8");
						while($row=mysql_fetch_assoc($res)) {
							$arrcitys[]=$res['locationid'];
							$im=$db->singlerec("select * from listing_images where pid='".$row['id']."' limit 1");
						if($count==1){
							$active="active";
						}
						else {
							$active="";
						}
						if ($count%4 == 1) {  
							echo "<div class='item ".$active."'>";
							echo "<div class='row'>";
						}
						?>                             
                                  <div class="col-md-3 col-sm-6 col-xs-12 mt10">
                                      <div class="carshop-item brdr-2">
                                          <div class="photo">
                                              <a href="<?echo $siteurl;?>/listing/<?php echo $row['randuniq']; ?>/<?php echo $row['slug']; ?>"><img src="<?echo $siteurl;?>/images/prop/<?php echo $im['image']; ?>" class="img-responsive" alt="a" /></a>
                                          </div><!--photo-->
                                             <div class="info row">                                    
                                                  <div class="col-md-12">
                                                         <p class="new-properties-head pdl10"><?php echo $row['prop_title']; ?></p>
                                                         <p class="new-properties-place pdl10 "><?php echo $row['location']; ?></p>
                                                         <p class=" greenhead pdl10"><?php echo $PSCurncy . $row['exp_price']; ?></p>
                                                         <div class=" col-md-12">
                                                             <div class="row">
                                                                 <div class=" col-xs-4 brdr-2 new-properties-details">
                                                                      <img src="<?echo $siteurl;?>/assets/images/square.png" /><?php echo $row['covered_area']; ?>
                                                                 </div><!--col-md-4-->
                                                                 <div class=" col-xs-4 brdr-2 new-properties-details">
                                                                      <img src="<?echo $siteurl;?>/assets/images/bed.png" /><span class="pdl10"><?php echo $row['bedroom']; ?></span>
                                                                 </div><!--col-md-4-->
                                                                 <div class=" col-xs-4 brdr-2 new-properties-details">
                                                                    <img src="<?echo $siteurl;?>/assets/images/bath.png" /><span class="pdl10"><?php echo $row['bathroom']; ?></span>
                                                                 </div><!--col-md-4-->
                                                             </div><!--row-->
                                                         </div><!--brdr-2-->
                                                   </div><!--price col-md-12-->                                      
                                                  </div><!--info row-->
                                              </div><!--carshop-item-->
                                         </div><!--col-md-3-->
										<?php                      
											if ($count%4 == 0) {
												echo '</div></div>';
											}
											$count++;
										}
										if ($count%4 != 1) echo '</div></div>';
										?>  
                      </div>
              </div> 

          </div><!--col-md-12 col-sm-12 col-xs-12 brdr-->

           <div class="col-md-12 col-xs-12 col-sm-12 brdr-new-1 mt20 mb20 ">
               <div class="row brdr-new-1-bg">
                    <p class="pdl20"><span class="blackhead"> Property </span><span class="greenhead"> Category</span></p>
                </div>
				<div class="col-md-4 col-sm-12 col-xs-12 pdt10">
				<?php
				$PS_Categ=$db->get_all("select filtername,cat_name from category where cat_status='1'");
				foreach($PS_Categ as $PSCateg):
				$catt=$PSCateg['cat_name'];
				$fcat=$PSCateg['filtername'];
				if(isset($cat)==$fcat) $ch="selected";
				else $ch="";
				echo "<p><a href='property-list.php?cat=$fcat' class='house-hold-ink'>$catt</a></p>";
				endforeach;
				?>
				</div>
          </div><!--col-md-12 col-xs-12 col-sm-12 brdr-->
        <?php include "browsby_location.php";?>
		<!--col-md-12 col-sm-12 col-xs-12 brdr-->
          
            
          
  
         <div class="col-md-12 col-sm-12 col-xs-12 brdr-new-1 mt20">
              <!---------Slider List--------->

          <div class="row brdr-new-1-bg">
                      <div class="col-md-6 col-sm-6 col-xs-6">
                          <p><span class="blackhead"> Suggested </span><span class="greenhead"> Properties</span></p>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-6">
                          <!-- Controls -->
                          <div class="controls pull-right">
                              <a class="left fa fa-chevron-left btn carshop-btn control-btn arrow-brdr" href="#carousel-example-2"
                                  data-slide="prev"></a><a class="right fa fa-chevron-right control-btn btn carshop-btn arrow-brdr" href="#carousel-example-2"
                                      data-slide="next"></a>
                          </div>
                          
                      </div>
                  </div>
                  
                  <div id="carousel-example-2" class="carousel slide " data-ride="carousel">
                      <!-- Wrapper for slides -->
                      <div class="carousel-inner">
                        <?php
						$count = 1;
						$res=mysql_query("select * from listings where post_sts=1 order by mplan desc limit 0, 8");
						while($prop=mysql_fetch_assoc($res)) {
							$arrcitys[]=$res['locationid'];
							$im=$db->singlerec("select * from listing_images where pid='".$prop['id']."' limit 1");
						if($count==1){
							$active="active";
						}
						else {
							$active="";
						}
						if ($count%4 == 1) {  
							echo "<div class='item ".$active."'>";
							echo "<div class='row'>";
						}
						?>
						   <div class="col-md-3 col-sm-6 col-xs-12 mt10">
							  <div class="carshop-item brdr-2">
								  <div class="photo">
									  <a href="<?echo $siteurl;?>/listing/<?php echo $prop['randuniq']; ?>/<?php echo $prop['slug']; ?>"><img src="<?echo $siteurl;?>/images/prop/<?php echo $im['image']; ?>" class="img-responsive" alt="<?php echo $prop['prop_title']; ?>" /></a>
								  </div><!--photo-->
									 <div class="info row">                                    
										  <div class="col-md-12">
												 <p class="new-properties-head pdl10"><?php echo $prop['prop_title']; ?></p>
												 <p class="new-properties-place pdl10 "><?php echo $prop['location']; ?></p>
												 <p class=" greenhead pdl10"><?php echo $PSCurncy . $prop['exp_price']; ?></p>
												 <div class=" col-md-12">
													 <div class="row">
														 <div class=" col-xs-4 brdr-2 new-properties-details">
															  <img src="<?echo $siteurl;?>/assets/images/square.png" /><?php echo $prop['covered_area']; ?>
														 </div><!--col-md-4-->
														 <div class=" col-xs-4 brdr-2 new-properties-details">
															  <img src="<?echo $siteurl;?>/assets/images/bed.png" /><span class="pdl10"><?php echo $prop['bedroom']; ?></span>
														 </div><!--col-md-4-->
														 <div class=" col-xs-4 brdr-2 new-properties-details">
															<img src="<?echo $siteurl;?>/assets/images/bath.png" /><span class="pdl10"><?php echo $prop['bathroom']; ?></span>
														 </div><!--col-md-4-->
													 </div><!--row-->
												 </div><!--brdr-2-->
										   </div><!--price col-md-12-->                                      
										  </div><!--info row-->
									  </div><!--carshop-item-->
								 </div><!--col-md-3-->
                                <?php                      
								if ($count%4 == 0) {
									echo '</div></div>';
								}
								$count++;
								}
								if ($count%4 != 1) echo '</div></div>';
								?>
                      </div><div class="mb10"></div>
              </div> 

     </div>

          <div class="col-md-12 col-xs-12 col-sm-12 brdr-new-1 mt20">
               <div class=" row brdr-new-1-bg">
                    <p class="pdl20"><span class="blackhead"> Property </span><span class="greenhead"> News</span></p>
                </div><!--col-md-12--> 
				<?php
				$news=$db->singlerec("select * from news where active_status=1 order by id desc limit 1");
				?>	                 
                <div class="col-md-12 col-sm-12 col-xs-12 mt5"> 
                       <div class="col-md-2 col-sm-2 col-xs-2 paddng">
                             <img src="<?echo $siteurl;?>/images/news/<?php echo $news['image']; ?>" class="img-responsive" />
                       </div><!--col-md-2 col-sm-2 col-xs-2-->
                       <div class="col-md-10 col-sm-10 col-xs-10 paddng">
                             <div class="customer-review-head">
                                  <?php echo $news['subject']; ?>
                             </div><!--customer-review-head-->
                             <div class="property-news-font">
                             <?php echo $news['message']; ?>
                    </div><!--property-news-font-->
                    <div class="property-news-font-1">
                            <?php echo date("d-M-Y", $news['created_at']); ?>
                    </div><!--property-news-font-2-->
                       </div><!--col-md-10 col-sm-10 col-xs-10-->
                </div><!--col-md-12 col-sm-12 col-xs-12-->
          </div><!--col-md-12 col-xs-12 col-sm-12 brdr-->
          
           <div class="col-md-12 col-xs-12 col-sm-12 brdr-new-1 mt20">
               <div class=" row brdr-new-1-bg">
                    <p class="pdl20"><span class="blackhead"> Customer </span><span class="greenhead"> Reviews/Testimonial</span></p>
                </div><!--col-md-12-->  
                <?php
				$testim=$db->singlerec("select * from testimonial where active_status=1 order by id desc limit 1");
				?>				
                <div class="col-md-12 col-sm-12 col-xs-12 mt5"> 
                       <div class="col-md-2 col-sm-2 col-xs-2 paddng">
                             <img src="<?echo $siteurl;?>/images/testimonial/<?php echo $testim['image']; ?>" class="img-responsive">
                       </div><!--col-md-2 col-sm-2 col-xs-2-->
                       <div class="col-md-10 col-sm-10 col-xs-10 paddng">
                             <div class="customer-review-head">
                                  <?php echo $testim['subject']; ?>
                             </div><!--customer-review-head-->
                             <div class="property-news-font">
                                  <?php echo $testim['message']; ?>
                             </div><!--customer-review-font-->
							 <div class="property-news-font-1">
                                  <?php echo date("d-M-Y", $testim['posted_at']); ?>
                             </div><!--customer-review-font-->
                       </div><!--col-md-10 col-sm-10 col-xs-10-->
                </div><!--col-md-12 col-sm-12 col-xs-12-->
          </div><!--col-md-12 col-xs-12 col-sm-12 brdr-->

         <div class="col-md-12 col-xs-12 col-sm-12">
             <div class="select-block">
                 <div class="container-wrapper">
                     <div class="logos-select">
                         <div class="input-wrapper">
                             <select name="" id="" onchange="location=this.options[this.selectedIndex].value;">
                                 <option value="" hidden>Browse properties by Category</option>
                                 <?php
                                 $PS_Categ=$db->get_all("select filtername,cat_name from category where cat_status='1'");
                                 foreach($PS_Categ as $PSCateg):
                                     $catt=$PSCateg['cat_name'];
                                     $fcat=$PSCateg['filtername'];
                                     if(isset($cat)==$fcat) $ch="selected";
                                     else $ch="";
                                     echo "<option value='property-list.php?cat=$fcat' class='house-hold-ink'>$catt</option>";
                                     //echo "<p><a href='property-list.php?cat=$fcat' class='house-hold-ink'>$catt</a></p>";
                                 endforeach;
                                 ?>
                             </select>
                         </div>
                     </div>
                 </div>
             </div>
         </div>

     </div>



    <!--col-md-9 col-sm-12 col-xs-12-->
     <!-- Disabled Search -->
<!--     <div class="col-md-3 col-sm-12 col-xs-12 mt10 brdr">-->
<!--          -->
<!--              <p><span class="blackhead"> Property </span><span class="greenhead"> Search</span></p>-->
<!--              <div class="new-properties-brdr-btm-2"></div>-->
<!--                  <form name="advsrch" action="property-list" class="form-horizontal" method="post">-->
<!--                      <div class=" form-group">-->
<!--                          <div class="col-sm-12 pdt15">-->
<!--                              <input type="text" name="keyword" class="form-control form-ctrl-height" placeholder="Property Title or Address">-->
<!--                          </div>-->
<!--                      </div>-->
<!--                      <div class=" form-group">-->
<!--                          <div class="col-sm-6">-->
<!--                              <select name="categ" class="form-control form-ctrl-height">-->
<!--                                <option value="">Category</option>-->
<!--								--><?php
//								$PS_Categ=$db->get_all("select filtername,cat_name from category where cat_status='1'");
//								foreach($PS_Categ as $PSCateg):
//								$catt=$PSCateg['cat_name'];
//								$fcat=$PSCateg['filtername'];
//								if(isset($cat)==$fcat) $ch="selected";
//								else $ch="";
//								echo '<option value="'.$fcat.'">'.$catt.'</option>';
//								endforeach;
//								?>
<!--                               </select>-->
<!--                          </div>-->
<!--                          <div class="col-sm-6">-->
<!--                               <select name="pfor" class="form-control form-ctrl-height">-->
<!--                                  <option value="">Property For</option>-->
<!--                                  <option value="rent">Rent</option>-->
<!--                                  <option value="sale">Sale</option>-->
<!--                                  <option value="lease">Lease</option>-->
<!--                               </select>-->
<!--                          </div>-->
<!--                      </div>-->
<!--                      <div class=" form-group">-->
<!--                          <div class="col-sm-6">-->
<!--                              <select name="bed" class="form-control form-ctrl-height">-->
<!--                                  <option value="">Bedroom</option>-->
<!--                                  <option value="1">1</option>-->
<!--                                  <option value="2">2</option>-->
<!--                                  <option value="3">3</option>-->
<!--                                  <option value="4">4</option>-->
<!--                                  <option value="5">5</option>-->
<!--                                  <option value="6">6</option>-->
<!--                                  <option value="7">7</option>-->
<!--                                  <option value="8">8</option>-->
<!--                                  <option value="9">9</option>-->
<!--                                  <option value="10">10</option>-->
<!--                               </select>-->
<!--                          </div>-->
<!--                          <div class="col-sm-6">-->
<!--                               <select name="bath" class="form-control form-ctrl-height">-->
<!--                                  <option value="">Bathroom</option>-->
<!--                                  <option value="1">1</option>-->
<!--                                  <option value="2">2</option>-->
<!--                                  <option value="3">3</option>-->
<!--                                  <option value="4">4</option>-->
<!--                                  <option value="5">5</option>-->
<!--                                  <option value="6">6</option>-->
<!--                                  <option value="7">7</option>-->
<!--                                  <option value="8">8</option>-->
<!--                                  <option value="9">9</option>-->
<!--                                  <option value="10">10</option>-->
<!--                               </select>-->
<!--                          </div>-->
<!--                      </div>-->
<!--                      <div class=" form-group">-->
<!--                          <div class="col-sm-6">-->
<!--                              <select name="minp" class="form-control form-ctrl-height">-->
<!--                                  <option value="">Min-Price</option>-->
<!--                                  <option value="1000">1000</option>-->
<!--                                  <option value="5000">5000</option>-->
<!--                                  <option value="10000">10000</option>-->
<!--                                  <option value="15000">15000</option>-->
<!--                                  <option value="20000">20000</option>-->
<!--                                  <option value="25000">25000</option>-->
<!--                                  <option value="30000">30000</option>-->
<!--                                  <option value="35000">35000</option>-->
<!--                                  <option value="40000">40000</option>-->
<!--                                  <option value="45000">45000</option>-->
<!--                               </select>-->
<!--                          </div>-->
<!--                          <div class="col-sm-6">-->
<!--                               <select name="maxp" class="form-control form-ctrl-height">-->
<!--                                  <option value="">Max-Price</option>-->
<!--                                  <option value="5000">5000</option>-->
<!--                                  <option value="10000">10000</option>-->
<!--                                  <option value="15000">15000</option>-->
<!--                                  <option value="20000">20000</option>-->
<!--                                  <option value="25000">25000</option>-->
<!--                                  <option value="30000">30000</option>-->
<!--                                  <option value="35000">35000</option>-->
<!--                                  <option value="40000">40000</option>-->
<!--                                  <option value="45000">45000</option>-->
<!--                                  <option value="50000">50000</option>-->
<!--                               </select>-->
<!--                          </div>-->
<!--                      </div>-->
<!--                      <div class="form-group">-->
<!--                          <div class="col-sm-12 text-center">-->
<!--                              <button class="btn btn-search btn-block" type="submit" name="advsrch"> <i class="fa fa-search"></i> Search</button>-->
<!--                          </div>-->
<!--                      </div>-->
<!--                  </form>-->
<!--  -->
<!--        <div class="row mt20">-->
<!--           --><?php //$homelf=$db->get_all("select googleadsurl from ads where googleadsurl !='' and adstype='0' and active_status='1'");
//			for($lf=0;$lf<count($homelf);$lf++){
//				$googleadsurl=$homelf[$lf]['googleadsurl'];
//				echo "<div class='col-md-6 mb5' style='padding: 5px;'>$googleadsurl</div>";
//			}
//
//			$homelf=$db->get_all("select img,name from ads where adstype='0' and active_status='1'");
//			for($lf=0;$lf<count($homelf);$lf++){
//				$lfimg=$homelf[$lf]['img'];
//				$lfurl=$homelf[$lf]['name'];
//				echo "<div class='col-md-6 mb5' style='padding: 5px;'><a href='$lfurl'><img src='$siteurl/admin/uploads/icon/$lfimg' class='img-responsive' /></a></div>";
//			}
//			?>
<!--        </div>-->
<!--	</div><!--col-md-3 col-sm-12 col-xs-12-->
    <!-- Disabled Search END -->
</div><!--container-->


</div>

<?php include "footer.php"; ?>
<?php
if(isset($nslet)) {
	$name=trim(addslashes($name));
	$mail=trim(addslashes($email));
	if(empty($name)||empty($mail)) {
	    echo "<script>swal('Oops..', 'You need to enter the name and email id to subscribe to the newsletter!', 'error')</script>";
	}
	else {
		$check=$db->check1column("newsletter", "email", $mail);
		if($check==0) { 
			$set="name='$name',";
			$set.="email='$mail'";
			$db->insertrec("insert into newsletter set $set");
			echo "<script>swal('Great!', 'You subscribed to the newsletter!', 'success')</script>";
		}
		else {
			echo "<script>swal('Oops..', 'You already subscribed to the newsletter!', 'error')</script>";
		}
	}
}
else if(isset($_REQUEST['verified'])) {
	echo "<script>swal('Congrats!', 'Your account has been activated! Now you can login to our site!', 'success')</script>";
}
else if(isset($_REQUEST['rdone'])) {
		echo "<script>swal('Congrats!', 'You`re successfully registered into the site! Please check the mail to verify your account!', 'success');</script>";
}
?>
