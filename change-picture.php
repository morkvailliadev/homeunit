<?php
include 'header.php';
include 'profile_header.php';
?>
<style>
input[type="button" i], input[type="file" i] { background: linear-gradient(#2b8 5%, #4fb948 100%);color: #fff;padding: 8px; }
</style>
<div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20">
        <span class="blackhead pdl10">Dashboard</span>
    </div><!--col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20-->
    
         <?php include "profile_left.php"; ?>
         <div class="col-md-9 col-sm-12 col-xs-12 mt20">
             <div class="col-md-12 col-sm-12 col-xs-12 profile-brdr-2">
                 <div class="pdt10">
                 <div class="row col-md-10 col-sm-6 col-xs-12 property-dash-head">Upload Photo<hr></div>
                 </div><!--class="pdt10"-->
                 
                 <div class=" col-md-12 col-sm-12 col-xs-12 table-responsive">
						<form action="change-picture" method="post" enctype="multipart/form-data">
						<img id="previewing">
						<hr id="line">
						<div class="col-md-12 " id="selectImage"><span class="blackhead" style="font-size:16px;">Select an image</span><br><br>
						   <div class="row"><div class="col-md-6">
								<input type="file" name="file" id="file" required="required">
							</div></div>
							<div class="row"><div class="col-md-6">
								<input type="submit" name="prf_img" value="Upload" class="submit"><br><br>
							</div></div>
						</div>
						<div class="row"><div class="col-md-6">
								<span style="color:#4285f4;" class="mt10"><b>* Image must be atleast 150*150 Pixels!</b></span>
						</div></div>
						</form>
                 </div><!--table-responsive-->
             </div><!--col-md-12 col-sm-12 col-xs-12 profile-brdr-->
         </div><!--col-md-9 col-sm-12 col-xs-12-->
    </div><!--row-->
</div> <!--container-->

<?php include "footer.php"; ?>