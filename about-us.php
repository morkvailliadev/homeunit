<?
include "header.php";
$cms=$db->singlerec("select * from cms where active_status=1");
?>

<div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20">
        <span class="blackhead pdl10">About Us</span>
    </div><!--col-md-12 col-sm-12 col-xs-12  market-place-head-bg mt20-->
    
    <div class="row">
	
         <div class="col-md-12 col-sm-12 col-xs-12 mt20">
             <div class="col-md-12 col-sm-12 col-xs-12 profile-brdr-2">
                 <div class="pdt10">
                 
                 </div><!--class="pdt10"-->					
					
					<div class="text-center blackhead" style="font-size:20px; font-weight:bold;">ABOUT US</div>
					
					<div class="text-center customer-review-font mt10">
					    <?php echo  $cms['aboutus']; ?>
					</div>
					
					
					<div class="text-center mt15 blackhead" style="font-size:18px; font-weight:bold;">
					The Best Place For Searching Property, And Get Property Done.
					</div>
					
					
					<div class="col-sm-6 mt20 text-center">
					   <img src="assets/images/mission.png">
					   
					   <div class="text-center mt15 blackhead" style="font-size:18px; font-weight:bold; border-top:2px solid#4fb948">
					     OUR MISSION 
					   </div>
					   
					   <div class="text-center customer-review-font mt10">
					    <?php echo  $cms['what_wedo']; ?>
					</div>
					</div>
					
					
					<div class="col-sm-6 mt20 text-center">
					   <img src="assets/images/vission.png">
					   
					   <div class="text-center mt15 blackhead" style="font-size:18px; font-weight:bold; border-top:2px solid#4fb948">
					     OUR VISION
					   </div>
					   <div class="text-center customer-review-font mt10">
					    <?php echo  $cms['our_vision']; ?>
					</div>
					</div>
							
                 
             </div><!--col-md-12 col-sm-12 col-xs-12 profile-brdr-->
         </div><!--col-md-9 col-sm-12 col-xs-12-->
    </div><!--row-->
</div> <!--container-->

<?php include "footer.php"; ?>